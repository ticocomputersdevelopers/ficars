$(document).ready(function() {

	function fiscalAmountCalculation() {
		$('.JSfiscalProduct').each(function(el) {
			$(this).on('change', function() {
				var JSfiscalUnitQuant = $(this).find('.JSfiscalUnitQuant').val(),
					JSfiscalUnitPrice = $(this).find('.JSfiscalUnitPrice').val(),
					JSfiscalTotalAmount = $(this).find('.JSfiscalTotalAmount').val();
				
				$(this).find('.JSfiscalTotalAmount').val(parseFloat(JSfiscalUnitQuant * JSfiscalUnitPrice).toFixed(2));
			});
		});
		
		$('.JSfiscalContainer .JSfiscalProduct').each(function() {
			var fiscalProduct = $(this);
			$(this).find('.JSfiscalProductDisable').on('click', function() {
				var attr = fiscalProduct.attr('disabled');
				if (typeof attr !== 'undefined' && attr !== false) {
					fiscalProduct.removeAttr('disabled').removeClass('JSfiscalDisabled');
					fiscalProduct.find('input:not(.JSfiscalTotalAmount)').removeAttr('disabled');
					fiscalProduct.find('select:not(.JSfiscalTotalAmount)').removeAttr('disabled');
				} else {
					fiscalProduct.attr('disabled', 'disabled').addClass('JSfiscalDisabled');
					fiscalProduct.find('input:not(.JSfiscalProductDisable)').attr('disabled', 'disabled');
					fiscalProduct.find('select:not(.JSfiscalProductDisable)').attr('disabled', 'disabled');
				}
			});
		});

	}
	fiscalAmountCalculation();

	$('.JSpaymentDropdown-check-list').each(function() {
		var anchor = $(this).find('.paymentAnchor'),
			drop = $(this),
			list = $(this).find('.JSpaymentDrop');

		anchor.on('click', function(e) {
			if(drop.hasClass('visible')) {
				drop.removeClass('visible');
			} else {
				drop.addClass('visible');
				drop.parent().siblings().find('.JSpaymentDropdown-check-list').removeClass('visible');
			}
		});
	});

	function totalFiscalSum() {
			var totalFiscalSum = 0;
			$('.JSfiscalProduct:not(.JSfiscalDisabled)').each(function(){
				var product = parseFloat($(this).find('.JSfiscalTotalAmount').val());
				totalFiscalSum += product;
			});

			$('.JSfiscalSumTotal').val(totalFiscalSum.toFixed(2));
		}
		totalFiscalSum();

		$('.JSfiscalProduct').on('change', function() {
			totalFiscalSum();
		});

		$('.JSfiscalProductDisable').on('change', function() {
			totalFiscalSum();
		});
	});
	
	$('.JSpaymentDrop').on('change', function() {
		if($('#placanje0').is(':checked')) { 
			$('#paymentOther').removeClass('hidden');
			$('#paymentTypeOther').removeAttr('disabled');
			$('#paymentAmountOther').removeAttr('disabled');
		} else {
		 	$('#paymentOther').addClass('hidden'); 
		 	$('#paymentTypeOther').attr('disabled', 'disabled');
			$('#paymentAmountOther').attr('disabled', 'disabled');
		}

		if($('#placanje1').is(':checked')) {
			$('#paymentCash').removeClass('hidden'); 
			$('#paymentTypeCash').removeAttr('disabled');
			$('#paymentAmountCash').removeAttr('disabled');
		} else { 
			$('#paymentCash').addClass('hidden'); 
			$('#paymentTypeCash').attr('disabled', 'disabled');
			$('#paymentAmountCash').attr('disabled', 'disabled');
		}

		if($('#placanje2').is(':checked')) {
			$('#paymentCard').removeClass('hidden'); 
			$('#paymentTypeCard').removeAttr('disabled');
			$('#paymentAmountCard').removeAttr('disabled');
		} else {
			$('#paymentCard').addClass('hidden'); 
			$('#paymentTypeCard').attr('disabled', 'disabled');
			$('#paymentAmountCard').attr('disabled', 'disabled');
		}

		if($('#placanje3').is(':checked')) {
			$('#paymentCheck').removeClass('hidden');
			$('#paymentTypeCheck').removeAttr('disabled');
			$('#paymentAmountCheck').removeAttr('disabled'); 
		} else {
			$('#paymentCheck').addClass('hidden'); 
			$('#paymentTypeCheck').attr('disabled', 'disabled');
			$('#paymentAmountCheck').attr('disabled', 'disabled');
		}

		if($('#placanje4').is(':checked')) {
			$('#paymentWireTransfer').removeClass('hidden'); 
			$('#paymentTypeWireTransfer').removeAttr('disabled');
			$('#paymentAmountWireTransfer').removeAttr('disabled');
		} else {
			$('#paymentWireTransfer').addClass('hidden'); 
			$('#paymentTypeWireTransfer').attr('disabled', 'disabled');
			$('#paymentAmountWireTransfer').attr('disabled', 'disabled');
		}

		if($('#placanje5').is(':checked')) {
			$('#paymentVoucher').removeClass('hidden'); 
			$('#paymentTypeVoucher').removeAttr('disabled');
			$('#paymentAmountVoucher').removeAttr('disabled');
		} else {
			$('#paymentVoucher').addClass('hidden'); 
			$('#paymentTypeVoucher').attr('disabled', 'disabled');
			$('#paymentAmountVoucher').attr('disabled', 'disabled');
		}

		if($('#placanje6').is(':checked')) {
			$('#paymentMobileMoney').removeClass('hidden'); 
			$('#paymentTypeMobileMoney').removeAttr('disabled');
			$('#paymentAmountMobileMoney').removeAttr('disabled');
		} else {
			$('#paymentMobileMoney').addClass('hidden');
			$('#paymentTypeMobileMoney').attr('disabled', 'disabled');
			$('#paymentAmountMobileMoney').attr('disabled', 'disabled'); 
		}



	});

		$('#JSinvoiceType').click(function() {
			var invoiceType = $(this).val();
			var transactionType = $('#JStransactionType').val();
			if(invoiceType == 'Advance' && transactionType == 'Sale') {
				$('#JSReklama').removeClass('hidden'); 
				$('#JSReklamaTextarea').removeAttr('disabled');
			} else {
				$('#JSReklama').addClass('hidden'); 
				$('#JSReklamaTextarea').attr('disabled', 'disabled');
			}

		});

		$('#JStransactionType').click(function() {
			var invoiceType = $('#JSinvoiceType').val();
			var transactionType = $(this).val();
			if(invoiceType == 'Advance' && transactionType == 'Sale') {
				$('#JSReklama').removeClass('hidden'); 
				$('#JSReklamaTextarea').removeAttr('disabled');
			} else {
				$('#JSReklama').addClass('hidden'); 
				$('#JSReklamaTextarea').attr('disabled', 'disabled'); 
			}

		});


		//filtriranje po tipu
		$('.JSTipRacuna').click(function() {
			var tip = $(this).data('tip');
			var search = $('#search-btn').val();
			var vrsta = location.pathname.split('/')[4];
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!vrsta){
				vrsta = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}
				var tip_str= location.pathname.split('/')[3];
				var tipes = tip_str.split('-');
				var index = tipes.indexOf(tip);
				// alert(tip_str);
				if(index !== -1 ){
					tipes.splice(index,1);
				}else{
					tipes.push(tip);			
				}
			window.location.href = base_url + 'admin/racuni/' + tipes.join('-') + '/' + vrsta + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		//filtriranje po vrsti
		$('.JSVrstaRacuna').click(function() {
			var vrsta = $(this).data('vrsta');
			var search = $('#search-btn').val();
			var tip = location.pathname.split('/')[3];
			var datum_od = $('#datepicker-from').val();
			var datum_do = $('#datepicker-to').val();
				if(!datum_od){
				datum_od = '0';
				}
				if(!datum_do){
				datum_do = '0';
		 		}
		 		if(!vrsta){
				vrsta = '0';
		 		}
		 		if(!search){
				search = '0';
		 		}
				var vrsta_str= location.pathname.split('/')[4];
				var vrste = vrsta_str.split('-');
				var index = vrste.indexOf(vrsta);
				// alert(vrsta_str);
				if(index !== -1 ){
					vrste.splice(index,1);
				}else{
					vrste.push(vrsta);			
				}
			window.location.href = base_url + 'admin/racuni/' + tip + '/' + vrste.join('-') + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		//filtriranje po datumu
		$('#datepicker-from').change(function() {
		var search = $('#search-btn').val();	
		var tip = location.pathname.split('/')[3];
		var vrsta = location.pathname.split('/')[4];
		var datum_od = $(this).val();
		var datum_do = $(this).data('datumdo');

		if(!datum_od){
				datum_od = '0';
		}
		if(!search){
			search = '0';
		}
		window.location.href = base_url + 'admin/racuni/' + tip + '/' + vrsta + '/' + datum_od + '/' + datum_do + '/' + search;
		});

		$('#datepicker-to').change(function() {
		var search = $('#search-btn').val();	
		var tip = location.pathname.split('/')[3];
		var vrsta = location.pathname.split('/')[4];
		var datum_od = $(this).data('datumod');
		var datum_do = $(this).val();

		if(!datum_od){
				datum_od = '0';
		}
		if(!search){
			search = '0';
		}
		window.location.href = base_url + 'admin/racuni/' + tip + '/' + vrsta + '/' + datum_od + '/' + datum_do + '/' + search;
		});


		$('#search-btn').click(function(){

		var search = $('#search').val();
		var tip = location.pathname.split('/')[3];
		var vrsta = location.pathname.split('/')[4];
		var datum_od = $('#datepicker-from').val();
		var datum_do = $('#datepicker-to').val();
			if(!datum_od){
			datum_od = '0';
			}
			if(!datum_do){
			datum_do = '0';
		 	}
		 	if(!search){
			search = '0';
		 	}				

		window.location.href = base_url + 'admin/racuni/' + tip +'/'+ vrsta + '/' + datum_od + '/' + datum_do + '/' + search;
		
		});

		$('#search').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search').val() != ''){
        	$("#search-btn").trigger("click");
    	}
    	});
	
	// Fiskalizacija napravi kopiju
    $('.JSArticleDetails-CopySend-no').on('click',function() {
    	$('body').removeClass('JSFiscalCopy-modal');
    	$('.JSFiscalCopyAlert').fadeOut();
    });
    $('.JSFiscalDetails-CopySend').on('click',function(){
    	$('body').addClass('JSFiscalCopy-modal');
    	$('.JSFiscalCopyAlert').fadeIn();
    });

    // Fiskalizacija napravi kopiju
    $('.JSArticleDetails-RefundSend-no').on('click',function() {
    	$('body').removeClass('JSFiscalCopy-modal');
    	$('.JSFiscalRefundAlert').fadeOut();
    });
    $('.JSFiscalDetails-RefundSend').on('click',function(){
    	$('body').addClass('JSFiscalCopy-modal');
    	$('.JSFiscalRefundAlert').fadeIn();
    });

    //Fiskalizacija posalji mail
   	$('.JSFiscalMailSend-no').on('click',function() {
    	$('body').removeClass('JSFiscalCopy-modal');
    	$('.JSFiscalMailAlert').fadeOut();
    });
    $('.JSFiscalMailSend').on('click',function(){
    	$('body').addClass('JSFiscalCopy-modal');
    	$('.JSFiscalMailAlert').fadeIn();
    });

    // INSURE JUST ONE CLIK IN CART
	var count = 0;
    $('.send_order_fiskal').click(function(){
        count++;
        if (count > 1) {
            $(this).prop("disabled", true);
        } 
    });