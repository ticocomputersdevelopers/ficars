<?php

class AdminFiskalizacijaController extends Controller {

    public static function narudzbina_fiskalizacija($web_b2c_narudzbina_id) {
        
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();

        $data = array(
            'strana'=>'narudzbina_fiskalizacija',
            'title'=>"Fiskalizacija",
            "narudzbina" => $narudzbina,
            "fiskalni_naziv" => AdminNacinPlacanja::getFiscalName($narudzbina->web_nacin_placanja_id),
            "iznos_narudzbine" => number_format(array_map('current',DB::select("SELECT sum(kolicina*jm_cena) from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id = ".$web_b2c_narudzbina_id.""))[0], 2, '.', '')

            );      
                      
        return View::make('admin/page',$data);
    }

    public static function narudzbina_fiskalizacija_send($web_b2c_narudzbina_id) {

        $data = Input::get();


        $validator_arr = array(
                'items' => 'required',
                'payment' => 'required',
                'invoiceType' => 'required',
                'transactionType' => 'required',
                'referentDocumentDT' => 'date',
                'cashier' => 'max:50',
                'buyerCostCenterId' => 'max:50'
                );

        if(($data['invoiceType'] == 'Advance' && $data['transactionType'] == 'Sale' && DB::table('racun')->where(array('web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,'tip_racuna'=>'Advance'))->first())) {
            $validator_arr['referentDocumentNumber'] = 'required';
            // $validator_arr['referentDocumentDT'] = 'required|date';  
        } elseif ($data['invoiceType'] == 'Copy' || $data['transactionType'] == 'Refund') {
            $validator_arr['referentDocumentNumber'] = 'required';
            $validator_arr['referentDocumentDT'] = 'required|date';
            $validator_arr['buyerId'] = 'required';
        }


        $data['buyerId'] = trim($data['buyerId']);
        
        if(isset($data['items'])) {       
            foreach ($data['items'] as $key => $value) {

                $validator_arr['items.'.$key.'.gtin'] = 'string|min:8|max:14';
                $validator_arr['items.'.$key.'.name'] = 'required|max:2048';
                $validator_arr['items.'.$key.'.unit'] = 'required';
                $validator_arr['items.'.$key.'.quantity'] = 'required|numeric|min:0.001|max:999999';
                $validator_arr['items.'.$key.'.unitPrice'] = 'required|numeric|min:0.01|max:99999999999999.999';
                $validator_arr['items.'.$key.'.labels.0'] = 'required';
                
            }
        }

        $payment_array = array();

        if(isset($data['payment'])) {
            foreach ($data['payment'] as $key => $value) {
                $validator_arr['payment.'.$key.'.paymentType'] = 'required';
                if($data['invoiceType'] == 'ProForma') {
                    $validator_arr['payment.'.$key.'.amount'] = 'required|numeric|min:0.00|max:0.00';
                }  else {
                    $validator_arr['payment.'.$key.'.amount'] = 'required|numeric|min:0.01';
                }
                $payment_array[] = $value['paymentType'];
            }
        }

        $translate_mess =  Language::validator_messages();
        $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        } else {
            if(isset($data['referentDocumentNumber']) && !empty($data['referentDocumentNumber'])) {
                if(sizeof(explode("-",$data['referentDocumentNumber'])) != 3) {
                    return Redirect::back()->withInput()->with('error_message','Niste uneli odgovarajući format referentnog broja');
                } else {
                    if(strlen(explode("-",$data['referentDocumentNumber'])[0]) != 8 || strlen(explode("-",$data['referentDocumentNumber'])[1]) != 8 || strlen(explode("-",$data['referentDocumentNumber'])[2]) < 1) {
                        return Redirect::back()->withInput()->with('error_message','Niste uneli odgovarajući format referentnog broja');
                    }
                }
            } 

            if(sizeof($payment_array) != sizeof(array_unique($payment_array))) {
                return Redirect::back()->withInput()->with('error_message','Načini plaćanja koje ste uneli moraju biti međusobno različiti!');
            }

            if(isset($data['reklama']) && !empty($data['reklama'])) {
                $reklama_artikli = explode('*',$data['reklama']);
                $reklama_array = array();
                if(sizeof($reklama_artikli) < 2) {
                     return Redirect::back()->withInput()->with('error_message','Reklama mora biti u zahtevanom formatu!');
                }
                
                foreach (array_filter($reklama_artikli) as $reklama_artikal) {
                    if(sizeof(explode("--",$reklama_artikal)) != 2 ) {
                        return Redirect::back()->withInput()->with('error_message','Niste uneli reklamu u odgovarajućem formatu!');
                    } else {
                        $reklama_array[]['naziv'] = "*".trim(explode("--",$reklama_artikal)[0]);
                        $reklama_array[sizeof($reklama_array)-1]['cena'] = trim(explode("--",$reklama_artikal)[1]);
                    }
                }
                $reklama = json_encode($reklama_array);
                unset($data['reklama']);
            }
           
            $nazivi = array();
            if($data['invoiceType'] == 'Advance' && ($data['transactionType'] == 'Sale' || $data['transactionType'] == 'Refund')) {
                foreach ($data['items'] as $key => $value) {
                    if(AdminFiskalizacija::checkAdvanceInput($data['items'][$key]['name'],$data['items'][$key]['labels'][0]) || $data['items'][$key]['quantity'] != 1) {
                        return Redirect::back()->withInput()->with('error_message','Neispravno formiranje avansne prodaje!');
                    } else {
                        $nazivi[] = $data['items'][$key]['name'];
                    }
                }
                if(sizeof($nazivi) != sizeof(array_unique($nazivi))) {
                    return Redirect::back()->withInput()->with('error_message','Ne mogu se ponavljati iste poreske stope na više mesta.');
                }
            }
            

            $stavka_roba_id = array();
            $ukupan_racun = 0;
            foreach ($data['items'] as $key => $value) {
                $stavka_roba_id[] = $data['items'][$key]['roba_id'];
                unset($data['items'][$key]['roba_id']);
                if(isset($data['items'][$key]['gtin']) && empty($data['items'][$key]['gtin'])) {
                    unset($data['items'][$key]['gtin']);
                }
                if(!($data['invoiceType'] == 'Advance' && ($data['transactionType'] == 'Sale' || $data['transactionType'] == 'Refund'))) {   
                    $data['items'][$key]['name'] .= ' /'.trim($data['items'][$key]['unit']);
                }
                unset($data['items'][$key]['unit']);
                // $data['items'][$key]['name'] = AdminFiskalizacija::replace_special_characters($data['items'][$key]['name']);
                $data['items'][$key]['totalAmount'] =  number_format($data['items'][$key]['unitPrice'] *  $data['items'][$key]['quantity'], 2, '.', '');
                $data['items'][$key]['quantity'] = number_format($data['items'][$key]['quantity'], 3, '.', '');
                $data['items'][$key]['unitPrice'] = number_format($data['items'][$key]['unitPrice'], 2, '.', '');
                $ukupan_racun += $data['items'][$key]['totalAmount'];
            }

            $sumTotalPaymetAmount = 0;
            foreach ($data['payment'] as $key => $value) {
                $data['payment'][$key]['amount'] = number_format($data['payment'][$key]['amount'], 2, '.', '');
                 $sumTotalPaymetAmount += number_format($data['payment'][$key]['amount'], 2, '.', '');
            }

            if($data['invoiceType'] == 'Advance' && ($data['transactionType'] == 'Sale' || $data['transactionType'] == 'Refund') && $ukupan_racun != $sumTotalPaymetAmount) {
                 return Redirect::back()->withInput()->with('error_message','Iznos uplate na avansnom računu mora biti jednaka iznosu artikala!');
            } elseif (!(!empty($data['referentDocumentNumber']) && $data['invoiceType'] == 'Normal' && DB::table('racun')->where(array('broj_dokumenta' => $data['referentDocumentNumber'],'tip_racuna' => 'Advance','vrsta_transakcije' => 'Refund'))->first())) {
                if($ukupan_racun > $sumTotalPaymetAmount && $data['invoiceType'] != 'ProForma') {
                    return Redirect::back()->withInput()->with('error_message','Iznos uplate ne može biti manja od iznosa računa!');
                }
            }

            if(!File::exists('files/fiscalization/3_7QXA_Т.Р.ФИЋА_ОБЈЕКАТ_БР.1.nochain.CERiKEY.pem')){
                return Redirect::back()->withInput()->with('error_message','Nepostoji odgovarajući sertifikat!');
            }


            if(!empty($data['referentDocumentNumber']) && !empty($data['referentDocumentDT'])) {
                if(!DB::table('racun')->where(array('broj_dokumenta'=>$data['referentDocumentNumber'], 'datum_racuna' => $data['referentDocumentDT']))->first()) {
                    return Redirect::back()->withInput()->with('error_message','Ne slažu se referentni broj i vreme referentnog računa!');
                }
            } else if(!empty($data['referentDocumentDT'])) {
                return Redirect::back()->withInput()->with('error_message','Morate uneti i referentni broj!'); 
            }

            $data['items'] = array_values($data['items']);
            $data['payment'] = array_values($data['payment']);

            $dataRequest = array(
                                    'invoiceNumber' => '889/1.0.',
                                    'cashier' => Session::get('b2c_admin'.AdminOptions::server()),
                                    'referentDocumentNumber' => $data['referentDocumentNumber'],
                                    'referentDocumentDT' =>  !empty($data['referentDocumentDT']) ? date(DATE_ISO8601, strtotime($data['referentDocumentDT'])) : '',
                                    'invoiceType' => $data['invoiceType'],
                                    'transactionType' => $data['transactionType'],
                                    'items' => $data['items'],
                                    'payment' => $data['payment']
                                );

            
            if($data['invoiceType'] == 'Advance' && $data['transactionType'] == 'Sale') {
                $dataRequest['dateAndTimeOfIssue'] = date(DATE_ISO8601, strtotime(date('Y-m-d H:i:s')));
            }

            if(!empty($data['buyerId'])) {
                if (AdminFiskalizacija::checkBuyerId($data['buyerId'])) {
                    $dataRequest['buyerId'] = $data['buyerId'];
                } else {
                    return Redirect::back()->withInput()->with('error_message','Morate uneti ispravan format ID kupca. Pogledajte šifrarnik na portalu poreske uprave!'); 
                }
                
                if (!empty($data['buyerCostCenterId'])) {
                    if(AdminFiskalizacija::checkBuyerCenterId($data['buyerCostCenterId'])) {
                        if($data['invoiceType'] == 'Normal' && $data['transactionType'] == 'Sale') {   
                            $dataRequest['buyerCostCenterId'] = $data['buyerCostCenterId'];
                        } else {
                            return Redirect::back()->withInput()->with('error_message','Opciono polje kupca se unosi samo za tip računa Promet Prodaja!');
                        }
                    } else {
                         return Redirect::back()->withInput()->with('error_message','Morate uneti ispravan format Opciono polje kupca. Pogledajte šifrarnik na portalu poreske uprave!');
                    }
                }
            }

        $response = AdminFiskalizacija::vcsdServiceRequest(json_encode($dataRequest));

        $dataResponse = json_decode($response);

        if(!isset($dataResponse->requestedBy) || !isset($dataResponse->signedBy)) {
            
            AdminSupport::saveLog('Generisanje_racuna_bad_request');
            return Redirect::back()->with('error_message', 'Greška');
            // return Redirect::back()->with('error_message', $dataResponse->message);
        } else {                

                $dataRequest = (object) $dataRequest;

                $racun_id = DB::select("SELECT setval(pg_get_serial_sequence('racun', 'racun_id'), coalesce(max(racun_id)+1, 1), false) FROM racun")[0]->setval;

                DB::table('racun')->insert(array(
                                    'racun_id' => $racun_id,
                                    'broj_dokumenta' => 'Racun'.$racun_id,
                                    'request' => json_encode($dataRequest),
                                    'response' => $response
                                ));

                $racunUpdate = array(
                                        'racun_id' => $racun_id,
                                        'datum_prometa' => isset($dataRequest->dateAndTimeOfIssue) ? date('Y-m-d H:i:s',strtotime($dataRequest->dateAndTimeOfIssue)) : null,
                                        'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
                                        'kasir' => $dataRequest->cashier,
                                        'kupac_id' => isset($dataRequest->buyerId) ? $dataRequest->buyerId : null,
                                        'kupac_mesto_placanja_id' => isset($dataRequest->buyerCostCenterId) ? $dataRequest->buyerCostCenterId : null,
                                        'referentni_broj_dokumenta' => $dataRequest->referentDocumentNumber,
                                        'referentni_dokument_datum' => date('Y-m-d H:i:s',strtotime($dataRequest->referentDocumentDT)),
                                        'tip_racuna' => $dataRequest->invoiceType,
                                        'vrsta_transakcije' => $dataRequest->transactionType,

                                        'zatrazio' => $dataResponse->requestedBy,
                                        'potpisao' => $dataResponse->signedBy,
                                        'datum_racuna' => date('Y-m-d H:i:s',strtotime($dataResponse->sdcDateTime)),
                                        'brojac_racuna' => $dataResponse->invoiceCounter,
                                        'brojac_racuna_vrsta' => $dataResponse->invoiceCounterExtension,
                                        'broj_dokumenta' => $dataResponse->invoiceNumber,
                                        'verifikacija_url' => $dataResponse->verificationUrl,
                                        'qr_kod' => $dataResponse->verificationQRCode,
                                        'tekstualni_prikaz' => $dataResponse->journal,
                                        'poruka' => $dataResponse->messages,
                                        'interni_podaci' => $dataResponse->encryptedInternalData,
                                        'digitalni_potpis' => $dataResponse->signature,
                                        'ukupan_brojac' => $dataResponse->totalCounter,
                                        'ukupan_brojac_tip' => $dataResponse->transactionTypeCounter,
                                        'iznos' => $dataResponse->totalAmount,
                                        'revizija_poreza' => $dataResponse->taxGroupRevision,
                                        'poslovno_ime' => $dataResponse->businessName,
                                        'pib' => $dataResponse->tin,
                                        'naziv_lokacije' => $dataResponse->locationName,
                                        'adresa' => $dataResponse->address,
                                        'okrug' => $dataResponse->district,
                                        'mrc' => $dataResponse->mrc,
                                        'esir' => $dataRequest->invoiceNumber,
                                        'reklama' => isset($reklama) ? $reklama : '' 
                                    );

                DB::table('racun')->where('racun_id',$racun_id)->update($racunUpdate);



                foreach ($dataResponse->taxItems as $taxRow) {
                    $taxInsert = array(
                                            'racun_porez_id' => DB::select("SELECT setval(pg_get_serial_sequence('racun_porez', 'racun_porez_id'), coalesce(max(racun_porez_id)+1, 1), false) FROM racun_porez")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'tip_kategorije' => $taxRow->categoryType,
                                            'poreska_oznaka' => $taxRow->label,
                                            'iznos_poreza' => $taxRow->amount,
                                            'iznos_poreske_stope' => $taxRow->rate,
                                            'naziv_kategorije' => $taxRow->categoryName
                                        );
                    DB::table('racun_porez')->insert($taxInsert);
                }


                foreach ($dataRequest->payment as $paymentRow) {

                    $paymentInsert = array(
                                            'racun_vrsta_placanja_id' =>  DB::select("SELECT setval(pg_get_serial_sequence('racun_vrsta_placanja', 'racun_vrsta_placanja_id'), coalesce(max(racun_vrsta_placanja_id)+1, 1), false) FROM racun_vrsta_placanja")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'iznos' => $paymentRow['amount'],
                                            'racun_vrsta_placanja_naziv_id' => (int) AdminFiskalizacija::getMappedPlacanjeID($paymentRow['paymentType'],1)
                                        );

                    DB::table('racun_vrsta_placanja')->insert($paymentInsert);
                }

                foreach ($dataRequest->items as $key => $itemsRow) {

                    $itemsInsert = array(
                                            'racun_stavka_id' => DB::select("SELECT setval(pg_get_serial_sequence('racun_stavka', 'racun_stavka_id'), coalesce(max(racun_stavka_id)+1, 1), false) FROM racun_stavka")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'roba_id' => $stavka_roba_id[$key],
                                            'broj_stavke' => $key + 1,
                                            'naziv_stavke' => $itemsRow['name'],
                                            'kolicina' => $itemsRow['quantity'],
                                            'pcena' => $itemsRow['unitPrice'],
                                            'etikete' => implode(",",$itemsRow['labels']),
                                            'uk_cena' => $itemsRow['totalAmount']
                                        );
                    if(isset($itemsRow['gtin'])) {
                        $itemsInsert['gtin'] = $itemsRow['gtin'];
                    }

                    
                    $itemsInsert['nab_cena'] = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,'roba_id' => $stavka_roba_id[$key]))->pluck('racunska_cena_nc'); 
                
                    DB::table('racun_stavka')->insert($itemsInsert);
                }

                if(DB::table('narudzbina_status')->where('naziv','Fiskalizovano')->first()) {
                    $narudzbina_status_id = DB::table('narudzbina_status')->where('naziv','Fiskalizovano')->first()->narudzbina_status_id;
                    if(!DB::table('narudzbina_status_narudzbina')->where(array('narudzbina_status_id' => $narudzbina_status_id,'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id))->first()) {
                        DB::table('narudzbina_status_narudzbina')->insert(array('narudzbina_status_id' => $narudzbina_status_id,'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id));
                    }
                }

             DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update(array('realizovano' => 1));
             AdminSupport::saveLog('Generisanje_racuna_'.$racun_id);
             if(AdminOptions::gnrl_options(3066) == 1) {
                AdminFiskalizacija::mailFiscalNotificationToClient($racun_id,AdminFiskalizacija::getKupacMail($web_b2c_narudzbina_id));
             }
             return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$dataResponse->messages);

        }


        }
    }

    public static function narudzbina_fiskalizacija_detalji($web_b2c_narudzbina_id,$racun_id) {

            $racun = DB::table('racun')->where('racun_id',$racun_id)->first();
            $text = $racun->tekstualni_prikaz;
            $text_prikaz = explode("\r\n",$text);
            $text_prikaz_drugi_deo = array($text_prikaz[sizeof($text_prikaz)-2],$text_prikaz[sizeof($text_prikaz)-1]);
            unset($text_prikaz[sizeof($text_prikaz)-2]); unset($text_prikaz[sizeof($text_prikaz)-1]);
            $text_prikaz_prvi_deo = implode("\r\n",$text_prikaz);
            $text_prikaz_drugi_deo = implode("\r\n",$text_prikaz_drugi_deo);
 

            $data = array(
            'strana'=>'narudzbina_fiskalizacija_detalji',
            'title'=>"Fiskalizacija",
            'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
            'racun' => DB::table('racun')->where('racun_id',$racun_id)->first(),
            'stavke' => DB::table('racun_stavka')->where('racun_id',$racun_id)->get(),
            'porezi' => DB::table('racun_porez')->where('racun_id',$racun_id)->get(),
            'placanja' => DB::table('racun_vrsta_placanja')->where('racun_id',$racun_id)->get(),
            'porez_ukupno' => array_map('current',DB::select("SELECT sum(round(iznos_poreza,2)) from racun_porez where racun_id = ".$racun_id.""))[0],
            'text' => $text,
            'text_prikaz_prvi_deo' => $text_prikaz_prvi_deo,
            'text_prikaz_drugi_deo' => $text_prikaz_drugi_deo,
            'qr_kod_gif' => 'data:image/gif;base64,'.$racun->qr_kod,
            'change' => number_format(array_map('current',DB::select("SELECT sum(iznos) from racun_vrsta_placanja where racun_id = ".$racun_id." "))[0] - $racun->iznos,2, ',', '.')


            );      
                      
        return View::make('admin/page',$data);
    }

    public static function pdf_fiskalizacija_isecak($racun_id) {

                $customPaper = array(0,0,233, 1000); 

                $racun = DB::table('racun')->where('racun_id',$racun_id)->first();
                $data_isecak = array(   
                    'racun' => $racun,
                    'text' => $racun->tekstualni_prikaz,
                    'qr_kod_gif' => 'data:image/gif;base64,'.$racun->qr_kod,
                    'change' => number_format(array_map('current',DB::select("SELECT sum(iznos) from racun_vrsta_placanja where racun_id = ".$racun_id." "))[0] - $racun->iznos,2, ',', '.')
                );

                
                $pdf = App::make('dompdf');
                $pdf->loadView('admin.pdf_fiskal_isecak', $data_isecak)->setPaper($customPaper);
                return $pdf->stream();
    }

    public static function pdf_fiskalizacija_racun($racun_id) {
            $racun = DB::table('racun')->where('racun_id',$racun_id)->first();
            
            $data = array(
                            'racun' => $racun,
                            'stavke' => DB::table('racun_stavka')->where('racun_id',$racun->racun_id)->get(),
                            'placanja' => DB::table('racun_vrsta_placanja')->where('racun_id',$racun->racun_id)->get(),
                            'porezi' => DB::table('racun_porez')->where('racun_id',$racun->racun_id)->get(),
                            'porez_ukupno' => array_map('current',DB::select("SELECT sum(round(iznos_poreza,2)) from racun_porez where racun_id = ".$racun->racun_id.""))[0]
                        );

                $pdf = App::make('dompdf');
                $pdf->loadView('admin.pdf_fiskal_racun', $data);
                return $pdf->stream();
    }

    public static function fiskalizacija_detalji_send($web_b2c_narudzbina_id,$racun_id) {
            $data = Input::get();

            if(!isset($data['action'])) {
               return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Neispravna akcija!');
            }

            if($data['action'] == 'mail-send') {
                AdminFiskalizacija::mailFiscalNotificationToClient($racun_id,$data['email']);
                return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('message','Uspešno ste pokrenuli slanje maila!'); 
            } elseif($data['action'] == 'Copy' || $data['action'] == 'Refund') {

                    $racun = DB::table('racun')->where('racun_id',$racun_id)->first();

                    if($data['action'] == 'Refund') {

                        if(DB::table('racun')->where(array('vrsta_transakcije'=>'Refund', 'referentni_broj_dokumenta' => $racun->broj_dokumenta))->first()) {
                            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Ne možete više puta refundirati isti račun!'); 
                        }

                        if(isset($data['buyerId'])) {
                            if(!empty($data['buyerId']) && AdminFiskalizacija::checkBuyerId($data['buyerId'])) {
                                $buyerId = $data['buyerId'];
                            } else {
                                return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Morate uneti ispravan format ID kupca. Pogledajte šifrarnik na portalu poreske uprave!'); 
                            }
                        } else {
                            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Obrišite keš Ctrl+Shift+R'); 
                        }

                    }


                    $items = array();
                    $stavka_roba_id = array();
                    foreach (DB::table('racun_stavka')->select('gtin','naziv_stavke','kolicina','etikete','pcena','uk_cena','roba_id')->where('racun_id',$racun_id)->get() as $stavka) {
                        $stavka_roba_id[] = $stavka->roba_id;
                        if(!empty($stavka->gtin)) {
                            $items[] = array(
                                                'gtin' => $stavka->gtin,
                                                'name' => $stavka->naziv_stavke,
                                                'quantity' => $stavka->kolicina,
                                                'labels' => explode(",",$stavka->etikete),
                                                'unitPrice' => $stavka->pcena,
                                                'totalAmount' => $stavka->uk_cena
                            );
                        } else {
                             $items[] = array(
                                                'name' => $stavka->naziv_stavke,
                                                'quantity' => $stavka->kolicina,
                                                'labels' => explode(",",$stavka->etikete),
                                                'unitPrice' => $stavka->pcena,
                                                'totalAmount' => $stavka->uk_cena 
                                            );
                        }
                    }

                    $payment = array();
                    foreach (DB::table('racun_vrsta_placanja')->select('racun_vrsta_placanja_naziv_id','iznos')->where('racun_id',$racun_id)->get() as $placanje) {

                            $payment[] = array(
                                                'paymentType' => AdminFiskalizacija::getMappedPlacanjeID($placanje->racun_vrsta_placanja_naziv_id),
                                                'amount' => $placanje->iznos
                                            );
                    }

                    $dataRequest = array(
                                    'invoiceNumber' => '889/1.0.',
                                    'cashier' => Session::get('b2c_admin'.AdminOptions::server()),
                                    'buyerId' => isset($buyerId) ? $buyerId : (!empty($racun->kupac_id) ? $racun->kupac_id : null),
                                    'referentDocumentNumber' => $racun->broj_dokumenta,
                                    'referentDocumentDT' => date(DATE_ISO8601, strtotime($racun->datum_racuna)),
                                    'items' => $items,
                                    'payment' => $payment
                                );

                    if($data['action'] == 'Copy') {
                        $dataRequest['invoiceType'] = 'Copy';
                        $dataRequest['transactionType'] = $racun->vrsta_transakcije; 
                    } elseif($data['action'] == 'Refund') {
                        $dataRequest['invoiceType'] = $racun->tip_racuna;
                        $dataRequest['transactionType'] = 'Refund';
                    }
    
        $response = AdminFiskalizacija::vcsdServiceRequest(json_encode($dataRequest));
        $dataResponse = json_decode($response);

        if(!isset($dataResponse->requestedBy) || !isset($dataResponse->signedBy)) {
            if($data['action'] == 'Copy') {
                AdminSupport::saveLog('Generisanje_kopije_racuna_bad_request_'.$racun_id);
            } elseif ($data['action'] == 'Refund') {
                AdminSupport::saveLog('Generisanje_refundacije_racuna_bad_request_'.$racun_id);
            }
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Greška'); 
            // return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message',$dataResponse->message); 
        } else {

            $dataRequest = (object) $dataRequest;
            $racun_old = $racun_id;
                $racun_id = DB::select("SELECT setval(pg_get_serial_sequence('racun', 'racun_id'), coalesce(max(racun_id)+1, 1), false) FROM racun")[0]->setval;

                DB::table('racun')->insert(array(
                                    'racun_id' => $racun_id,
                                    'broj_dokumenta' => 'Racun'.$racun_id,
                                    'request' => json_encode($dataRequest),
                                    'response' => $response
                                ));

                $racunUpdate = array(
                                        'racun_id' => $racun_id,
                                        'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
                                        'kasir' => $dataRequest->cashier,
                                        'kupac_id' => $dataRequest->buyerId,
                                        'kupac_mesto_placanja_id' => isset($dataRequest->buyerCostCenterId) ? $dataRequest->buyerCostCenterId : null,
                                        'referentni_broj_dokumenta' => $dataRequest->referentDocumentNumber,
                                        'referentni_dokument_datum' => date('Y-m-d H:i:s',strtotime($dataRequest->referentDocumentDT)),
                                        'tip_racuna' => $dataRequest->invoiceType,
                                        'vrsta_transakcije' => $dataRequest->transactionType,

                                        'zatrazio' => $dataResponse->requestedBy,
                                        'potpisao' => $dataResponse->signedBy,
                                        'datum_racuna' => date('Y-m-d H:i:s',strtotime($dataResponse->sdcDateTime)),
                                        'brojac_racuna' => $dataResponse->invoiceCounter,
                                        'brojac_racuna_vrsta' => $dataResponse->invoiceCounterExtension,
                                        'broj_dokumenta' => $dataResponse->invoiceNumber,
                                        'verifikacija_url' => $dataResponse->verificationUrl,
                                        'qr_kod' => $dataResponse->verificationQRCode,
                                        'tekstualni_prikaz' => $dataResponse->journal,
                                        'poruka' => $dataResponse->messages,
                                        'interni_podaci' => $dataResponse->encryptedInternalData,
                                        'digitalni_potpis' => $dataResponse->signature,
                                        'ukupan_brojac' => $dataResponse->totalCounter,
                                        'ukupan_brojac_tip' => $dataResponse->transactionTypeCounter,
                                        'iznos' => $dataResponse->totalAmount,
                                        'revizija_poreza' => $dataResponse->taxGroupRevision,
                                        'poslovno_ime' => $dataResponse->businessName,
                                        'pib' => $dataResponse->tin,
                                        'naziv_lokacije' => $dataResponse->locationName,
                                        'adresa' => $dataResponse->address,
                                        'okrug' => $dataResponse->district,
                                        'mrc' => $dataResponse->mrc,
                                        'esir' => $dataRequest->invoiceNumber
                                       
                                    );

                DB::table('racun')->where('racun_id',$racun_id)->update($racunUpdate);

                foreach ($dataResponse->taxItems as $taxRow) {
                    $taxInsert = array(
                                            'racun_porez_id' => DB::select("SELECT setval(pg_get_serial_sequence('racun_porez', 'racun_porez_id'), coalesce(max(racun_porez_id)+1, 1), false) FROM racun_porez")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'tip_kategorije' => $taxRow->categoryType,
                                            'poreska_oznaka' => $taxRow->label,
                                            'iznos_poreza' => $taxRow->amount,
                                            'iznos_poreske_stope' => $taxRow->rate,
                                            'naziv_kategorije' => $taxRow->categoryName
                                        );
                    DB::table('racun_porez')->insert($taxInsert);
                }


                foreach ($dataRequest->payment as $paymentRow) {

                    $paymentInsert = array(
                                            'racun_vrsta_placanja_id' =>  DB::select("SELECT setval(pg_get_serial_sequence('racun_vrsta_placanja', 'racun_vrsta_placanja_id'), coalesce(max(racun_vrsta_placanja_id)+1, 1), false) FROM racun_vrsta_placanja")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'iznos' => $paymentRow['amount'],
                                            'racun_vrsta_placanja_naziv_id' => (int) AdminFiskalizacija::getMappedPlacanjeID($paymentRow['paymentType'],1)
                                        );

                    DB::table('racun_vrsta_placanja')->insert($paymentInsert);
                }

                foreach ($dataRequest->items as $key => $itemsRow) {
                    $itemsInsert = array(
                                            'racun_stavka_id' => DB::select("SELECT setval(pg_get_serial_sequence('racun_stavka', 'racun_stavka_id'), coalesce(max(racun_stavka_id)+1, 1), false) FROM racun_stavka")[0]->setval,
                                            'racun_id' => $racun_id,
                                            'roba_id' => $stavka_roba_id[$key],
                                            'broj_stavke' => $key + 1,
                                            'naziv_stavke' => $itemsRow['name'],
                                            'kolicina' => $itemsRow['quantity'],
                                            'pcena' => $itemsRow['unitPrice'],
                                            'etikete' => implode(",",$itemsRow['labels']),
                                            'uk_cena' => $itemsRow['totalAmount']
                                        );
                    if(isset($itemsRow['gtin'])) {
                        $itemsInsert['gtin'] = $itemsRow['gtin'];
                    }

            
                        $itemsInsert['nab_cena'] = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,'roba_id' => $stavka_roba_id[$key]))->pluck('racunska_cena_nc');

                
                    DB::table('racun_stavka')->insert($itemsInsert);
                }

                if($data['action'] == 'Copy') {
                    AdminSupport::saveLog('Generisanje_kopije_racuna_'.$racun_old.'-'.$racun_id);
                } elseif($data['action'] == 'Refund') {
                    AdminSupport::saveLog('Generisanje_refundacije_racuna_'.$racun_old.'-'.$racun_id);
                }

            if(AdminOptions::gnrl_options(3066) == 1) {
                AdminFiskalizacija::mailFiscalNotificationToClient($racun_id,AdminFiskalizacija::getKupacMail($web_b2c_narudzbina_id));
             }

             return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('message',$dataResponse->messages);
            }
        } else {
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$web_b2c_narudzbina_id.'/'.$racun_id)->with('error_message','Data akcija nije izvršena');
        }      
    }

    public static function racuni($tip, $vrsta, $datum_od, $datum_do, $search) {

        $select = "SELECT * from racun where potpisao is not null and potpisao <> '' ";
        $where = "";

        $tipovi = array();
        if(!empty($tip)){
        $tipovi = explode('-',$tip);
            $where .= " and tip_racuna in ('".implode("','",$tipovi)."') ";
        }

        $vrste = array();
        if(!empty($vrsta)){
        $vrste = explode('-',$vrsta);
            $where .= " and vrsta_transakcije in ('".implode("','",$vrste)."') ";
        }

        if(!empty($datum_od)){
            $where .= " and CAST(datum_racuna as DATE) >= '".$datum_od."' ";
        }

        if(!empty($datum_do)) {
            $where .= " and CAST(datum_racuna as DATE) <= '".$datum_do."' ";
        }

        if($datum_od==0 and $datum_do==0){
            $dat1='';
            $dat2='';
        }else 
        if($datum_od!=0 and $datum_do==0){
            $dat1=$datum_od;
            $dat2='';
        }else 
        if($datum_od==0 and $datum_do!=0){
            $dat1='';
            $dat2=$datum_do;
        }else 
        {
            $dat1=$datum_od;
            $dat2=$datum_do;
        }

        if(!empty($search)) {
            $where .= " and broj_dokumenta ilike '%".$search."%' or kupac_id ilike '%".$search."%' or kupac_mesto_placanja_id ilike '%".$search."%' or zatrazio ilike '%".$search."%' or potpisao ilike '%".$search."%' or brojac_racuna ilike '%".$search."%'";
        }

        if(Input::get('page')){
            $pageNo = Input::get('page');
        }else{
            $pageNo = 1;
        }

        $limit = 20;
        $offset = ($pageNo-1)*$limit;

        $pagination = " LIMIT ".$limit." OFFSET ".$offset."";

        $query = DB::select($select.$where.' order by racun_id desc '.$pagination);

        
         $data=array(
        "strana"=>'racuni',
        "title"=>"Administratorski panel",
        "tipovi" => $tipovi,
        "vrste" => $vrste,
        "count" => sizeof(DB::select($select.$where)),
        "datum_do" => $datum_do,
        "datum_od" => $datum_od,
        "dat1" => $dat1,
        "dat2" => $dat2,
        "query"=>$query,
        "limit"=>$limit,
        "search"=>$search
        );

        return View::make('admin/page',$data);
    }

    public static function fiskalizacija_analitika() {

        $data = Input::get();
        $query = "SELECT racun_id,datum_racuna from racun r where potpisao is not null and potpisao <> '' ";

        $queryWhere = "";
        if(!empty($data['invoiceType'])) {
            $queryWhere .= " and tip_racuna = '".$data['invoiceType']."' ";
        }

        if(!empty($data['transactionType'])) {
            $queryWhere .= " and vrsta_transakcije = '".$data['transactionType']."' ";
        }

        if(!empty($data['buyerId'])) {
            $queryWhere .= " and r.web_b2c_narudzbina_id in (select web_b2c_narudzbina_id from web_b2c_narudzbina where web_kupac_id = ".$data['buyerId'].") ";
            $web_kupac_id = $data['buyerId'];
        }

        if(!empty($data['date_from'])) {
            $queryWhere .= " and CAST(datum_racuna as DATE) >= '".$data['date_from']."' ";
        }

        if(!empty($data['date_to'])) {
            $queryWhere .= " and CAST(datum_racuna as DATE) <= '".$data['date_to']."' ";
        }
        $rows = DB::select($query.$queryWhere.' order by CAST(datum_racuna as DATE) asc');
        $rowsMappedNew = AdminFiskalizacija::array_map_assoc(function($key,$row){
            return [$row->racun_id,date('Y-m-d',strtotime($row->datum_racuna))];
        },$rows);
        $rows= array();
        foreach ($rowsMappedNew as $key => $value) {
            $rows[$value][] = $key;
        }
        $dataPdf = array(
                        'date_from' => !empty($data['date_from']) ? date('d.m.Y.',strtotime($data['date_from'])) : '',
                        'date_to' => !empty($data['date_to']) ? date('d.m.Y.',strtotime($data['date_to'])) : '',
                        'rows' => $rows,
                        'racuni_ids' => array_keys($rowsMappedNew),
                        'web_kupac' => isset($web_kupac_id) ? AdminKupci::kupacPodaci($web_kupac_id) : null,
                        'vrsta_racuna' => AdminFiskalizacija::mappedVrstaRacuna($data['invoiceType'],$data['transactionType'])
                    );
         $pdf = App::make('dompdf');
        
        $pdf->loadView('admin.pdf_fiskal_izvestaj', $dataPdf)->setPaper('a4', 'landscape');

        return $pdf->stream();

    }
     
}