<?php
use Service\DExpress;
use Service\Mailer;
Use Service\CityExpress;
use Service\Drip;

class AdminNarudzbineController extends Controller {

    
    function porudzbine($status, $narudzbina_status_id, $datum_od, $datum_do, $search, $pickup_id)
    {   
        $sortColumn = Input::get('sort_column') ? Input::get('sort_column') : 'broj_dokumenta';
        $sortDirection = Input::get('sort_direction') ? Input::get('sort_direction') : 'desc';

        $statusi = explode('-',$status);
        $query_osnovni=0;
        
        $select = "SELECT distinct web_b2c_narudzbina.*, web_kupac.* FROM web_b2c_narudzbina LEFT JOIN web_kupac ON web_kupac.web_kupac_id = web_b2c_narudzbina.web_kupac_id ";
        $where="";
        $where_registracija= "";
        $where_vrsta= "";
        $where_status= "";
        $export = Input::get('export') && Input::get('export') == '1' ? true : false;

        if (count($statusi)>1 && in_array('sve', $statusi)) {
            unset($statusi[array_search('sve', $statusi)]);
        }

        if (!in_array('sve', $statusi)) {
         
            if (in_array('registrovani', $statusi)) {
            $where_registracija.= "OR status_registracije=1 ";
            }
            if (in_array('neregistrovani', $statusi)) {
            $where_registracija.= "OR status_registracije=0 ";
            }
            if (in_array('nove', $statusi)) {
            $where_status.= "OR (prihvaceno=0 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('prihvacene', $statusi)) {
            $where_status.= "OR (prihvaceno=1 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('realizovane', $statusi)) {
            $where_status.= "OR (realizovano=1 AND stornirano=0) ";
            }
            if (in_array('stornirane', $statusi)) {
            $where_status.= "OR stornirano=1 ";
            }
            if (in_array('privatna', $statusi)) {
            $where_vrsta.= "OR flag_vrsta_kupca=0 ";
            }
            if (in_array('pravna', $statusi)) {
            $where_vrsta.= "OR flag_vrsta_kupca=1 ";
            }
           
        }

        if ($where_registracija != '') {
            $where.="AND (".substr($where_registracija, 3).") ";
        }
        if ($where_status != '') {
            $where.="AND (".substr($where_status, 3).") ";
        }
        if ($where_vrsta != '') {
            $where.="AND (".substr($where_vrsta, 3).") ";
        }

        if($datum_od==0 and $datum_do==0){
        }
        else if($datum_od!=0 and $datum_do==0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' ";
        }
        else if($datum_od==0 and $datum_do!=0){
            $where.= "AND datum_dokumenta <= '".$datum_do."' ";
        }
        else if($datum_od!=0 and $datum_do!=0){
            $where.= "AND datum_dokumenta >= '".$datum_od."' AND datum_dokumenta <= '".$datum_do."' ";
        }

        // filter dodatnih statusa
        if($narudzbina_status_id !=0){
            $dodatniStatusi = str_replace("-", ", ", $narudzbina_status_id);
            $select .= "LEFT JOIN narudzbina_status_narudzbina nsn ON nsn.web_b2c_narudzbina_id = web_b2c_narudzbina.web_b2c_narudzbina_id ";
            $where .= "AND nsn.narudzbina_status_id IN (". $dodatniStatusi .") ";
        }

        // filter city statusa
        if($pickup_id>1 AND $pickup_id !=''){
            $where.="AND (SELECT pickup_id FROM posta_zahtev_api WHERE web_b2c_narudzbina_id = web_b2c_narudzbina.web_b2c_narudzbina_id LIMIT 1 ) > '1' ";
        }
        else if($pickup_id == 1){
            $where.="AND (SELECT pickup_id FROM posta_zahtev_api WHERE web_b2c_narudzbina_id = web_b2c_narudzbina.web_b2c_narudzbina_id LIMIT 1 ) ='".$pickup_id."' AND (SELECT pickup_id FROM posta_zahtev_api WHERE web_b2c_narudzbina_id = web_b2c_narudzbina.web_b2c_narudzbina_id LIMIT 1 ) !='' ";
        }

        //search
        if(empty($search)){
            $where.="AND web_b2c_narudzbina.web_b2c_narudzbina_id != -1 ";
        }
        else{
            $where_search="";
            foreach (explode(' ',$search) as $word) {                   
                      $where_search.= "broj_dokumenta ILIKE '%" . strtoupper($word) . "%' OR adresa ILIKE '%" . $word . "%' OR ime ILIKE '%" . $word . "%' OR prezime ILIKE '%" . $word . "%' OR telefon ILIKE '%" . $word . "%' OR ";
                       }           
            $where.="AND (". substr($where_search, 0,-3) .") ";
        }
 
        if(Input::get('page')){
            $pageNo = Input::get('page');
        }else{
            $pageNo = 1;
        }

        $limit = 20;
        $offset = ($pageNo-1)*$limit;

        if($sortColumn != "status"){
            $sortQuery = " ORDER BY ".$sortColumn." ".$sortDirection;
        }else{
            if($sortDirection == "asc"){
                $sortQuery = " ORDER BY stornirano DESC, realizovano DESC, prihvaceno DESC";
            }else{
                $sortQuery = " ORDER BY prihvaceno DESC, realizovano DESC, stornirano DESC";
            }
        }
        $pagination = $sortQuery." LIMIT ".$limit." OFFSET ".$offset."";

        $query_basic = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3));
        $query = DB::select($select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3).$pagination);
        $allIds = array_map(function($row){ return $row->web_b2c_narudzbina_id; },$query_basic);
       
        if($export){
           
            $data = array(
                array('RBroj','Naziv', 'Adresa', 'PttBroj', 'Grad','Kontakt', 'Telefon', 'Sadrzaj_posiljke', 'Otkup', 'Vrednost', 'Napomena', 'RefID','PckNum')
            );
            $apo='\'';
            $i=0;
            foreach($query_basic as $item){
               $ukupna_cena = 0;
               $roba_ids=[];
               $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$item->web_b2c_narudzbina_id)->get();
                foreach ($stavke as $stavka) {
                    $ukupna_cena += $stavka->jm_cena*$stavka->kolicina;
                    $roba_ids[] =  DB::table('roba')->where('roba_id',$stavka->roba_id)->pluck('sku');
                }
                    
                
                $data[] = array(
                    $i++,$item->ime.' '.$item->prezime,$item->adresa,$item->posta_br,$item->mesto,'',$item->telefon,implode(' + ',$roba_ids),'', $ukupna_cena,'','',''
                    
                );
            } 
            
            $doc = new PHPExcel();
            $doc->setActiveSheetIndex(0);
            $doc->getActiveSheet()->fromArray($data);
             
            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $store_path = 'files/export_narudzbina_'.date('Y-m-d').'.xls';
            $objWriter->save($store_path);
            return Response::download($store_path);
        }
        // echo $select.(strlen($where)>0 ? " WHERE ":"").substr($where, 3).$pagination; die;
        $data=array(
        "strana"=>'pocetna',
        "title"=>"Administratorski panel",
        "query"=>$query,
        "count"=>count($query_basic),
        "allIds" => $allIds,
        "limit"=>$limit,
        "narudzbina_status_id"=>explode('-',$narudzbina_status_id),
        "pickup_id"=>$pickup_id,
        "statusi"=>$statusi,
        "search"=>$search,
        "datum_od"=>$datum_od,
        "datum_do"=>$datum_do,
        "sort_column" => $sortColumn,
        "sort_direction" => $sortDirection
        );
    return View::make('admin/page',$data);

    }

    function narudzbina($web_b2c_narudzbina_id, $roba_id = null)
    {   
        
        if(isset($roba_id)){
            $check_roba_id = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,'roba_id'=>$roba_id))->count();
            if($check_roba_id == 0){            
                $stavka_data = DB::table('roba')->select('tarifna_grupa_id','racunska_cena_nc')->where('roba_id',$roba_id)->first();
                $insert_array = array(
                    'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
                    'broj_stavke' => 1,
                    'roba_id' => $roba_id,
                    'kolicina' => 1,
                    'jm_cena' => AdminCommon::get_price($roba_id),
                    'tarifna_grupa_id' => $stavka_data->tarifna_grupa_id,
                    'racunska_cena_nc' => $stavka_data->racunska_cena_nc
                    );
                
                AdminNarudzbine::rezervacija($roba_id,1,false,true);

                DB::table('web_b2c_narudzbina_stavka')->insert($insert_array);
                AdminSupport::saveLog('NARUDZBINA_STAVKA_DODAJ', array(DB::table('web_b2c_narudzbina_stavka')->max('web_b2c_narudzbina_stavka_id')));

                //drip
                if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
                    $dbOrder = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->first();
                    $drip = new Drip();
                    $drip->addOrUpdateOrder($dbOrder,"updated");   
                }
            }
        }
        $id_posta_slanje = DB::select("SELECT nextval('posta_zahtev_api_posta_zahtev_api_id_seq')")[0]->nextval;
                
        $web_b2c_narudzbina_id == 0 ? $check_old = false : $check_old = true;

        $data=array(
        "strana"=>'narudzbina',
        "title"=>"Narudzbina",
        "web_b2c_narudzbina_id" => $web_b2c_narudzbina_id,
        "id_posta_slanje" => $id_posta_slanje,
        "web_kupac_id"=> $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_kupac_id') : '',
        "broj_dokumenta" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta') : AdminNarudzbine::brojNarudzbine(),
        "datum_dokumenta" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta') : date('Y-m-d'),
        "web_nacin_placanja_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_nacin_placanja_id') : '',
        "web_nacin_isporuke_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'web_nacin_isporuke_id') : '',
        "ip_adresa" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'ip_adresa') : '',
        "napomena" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena') : '',
        "posta_slanje_broj_posiljke" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'posta_slanje_broj_posiljke') : '',
        "posta_slanje_poslato" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'posta_slanje_poslato') : 0,
        "narudzbina_stavke" => $check_old ? DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->orderBy('web_b2c_narudzbina_stavka_id', 'asc')->get() : array(),
        "ukupna_cena" => $check_old ? AdminNarudzbine::ukupnaCena($web_b2c_narudzbina_id) : '0.00',
        "troskovi_isporuke" => $check_old ? AdminCommon::troskovi_isporuke($web_b2c_narudzbina_id) : '0.00',
        "cena_dostave_custom" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id,'trosak_isporuke') : '',
        "cena_dostave" => $check_old ? DB::table('web_troskovi_isporuke')->pluck('cena') : '0.00',
        "cena_do" => $check_old ? DB::table('web_troskovi_isporuke')->pluck('cena_do') : '0.00',
        "popust" => $check_old ? AdminNarudzbine::bodovi_popust($web_b2c_narudzbina_id) : '0.00',
        "prihvaceno" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'prihvaceno') : '',
        "realizovano" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'realizovano') : '',
        "stornirano" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'stornirano') : '',
        "posta_slanje_id" => $check_old ? AdminNarudzbine::find($web_b2c_narudzbina_id, 'posta_slanje_id') : 1,
        "narudzbina_status_id" => $check_old ? array_map('current',DB::table('narudzbina_status_narudzbina')->select('narudzbina_status_id')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get()) : [],
        "narudzbina_poste_slanja" => $check_old ? AdminNarudzbine::narudzbina_poste_slanja($web_b2c_narudzbina_id) : ''
        );

    return View::make('admin/page',$data);

    }

    public function updateNarudzbinu()
    {
        $data = Input::get();
        
        if(AdminNarudzbine::find($data['web_b2c_narudzbina_id'], 'realizovano') || AdminNarudzbine::find($data['web_b2c_narudzbina_id'], 'stornirano')){
            $data_rel_stor = (array) DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$data['web_b2c_narudzbina_id'])->first();
            $data_rel_stor['napomena'] = $data['napomena'];
            if(AdminNarudzbine::find($data['web_b2c_narudzbina_id'], 'realizovano')){
                $data_rel_stor['narudzbina_status_id'] = isset($data['narudzbina_status_id']) && count($data['narudzbina_status_id']) > 0 ? $data['narudzbina_status_id'] : [];
            }
            $data = $data_rel_stor;
        }

        $rules = array(
            'web_kupac_id' => 'not_in:0',
            'broj_dokumenta' => 'required|between:2,10|unique:web_b2c_narudzbina,broj_dokumenta,'.$data['web_b2c_narudzbina_id'].',web_b2c_narudzbina_id',
            'posta_slanje_broj_posiljke' => 'max:15',
            'ip_adresa' => 'max:20',
            'napomena' => 'max:1000'
        );
        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$data['web_b2c_narudzbina_id'])->withInput()->withErrors($validator->messages());
        }else{
            
            $narudzbina = AdminNarudzbine::save($data);

            $data['web_b2c_narudzbina_id'] == 0 ? $web_b2c_narudzbina_id = DB::select("SELECT currval('web_b2c_narudzbina_web_b2c_narudzbina_id_seq')")[0]->currval : $web_b2c_narudzbina_id = $data['web_b2c_narudzbina_id'];


            $narudzbina_status_id = isset($data['narudzbina_status_id']) && !empty($data['narudzbina_status_id']) ? (is_array($data['narudzbina_status_id']) ? $data['narudzbina_status_id'] : explode(',',$data['narudzbina_status_id'])) : [];
            AdminNarudzbine::saveMoreStatuses($web_b2c_narudzbina_id,$narudzbina_status_id);


            $message = 'Uspešno ste sačuvali podatke';
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message', $message);
        }
    }

    // RACUN, PONUDA... PDFs
    public function pdf($web_b2c_narudzbina_ids)
    {
        $web_b2c_narudzbina_ids = explode('-',$web_b2c_narudzbina_ids);

        $data = [
            'web_b2c_narudzbina_ids' => $web_b2c_narudzbina_ids,
        ];

        $pdf = App::make('dompdf');
        
        $pdf->loadView('admin.pdf_narudzbina', $data);

        return $pdf->stream();

        // AdminNarudzbine::createPdf($web_b2c_narudzbina_id);

    }

    public function pdf_racun($web_b2c_narudzbina_ids)
    {
        $web_b2c_narudzbina_ids = explode('-',$web_b2c_narudzbina_ids);

         $data = [
             'web_b2c_narudzbina_ids' => $web_b2c_narudzbina_ids,
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_racun', $data);

        return $pdf->stream();    

    }

    public function pdf_ponuda($web_b2c_narudzbina_ids)
    {
        $web_b2c_narudzbina_ids = explode('-',$web_b2c_narudzbina_ids);

         $data = [
             'web_b2c_narudzbina_ids' => $web_b2c_narudzbina_ids,
         ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_ponuda', $data);

        return $pdf->stream();
    }

    public function pdf_predracun($web_b2c_narudzbina_ids)
    {
        $web_b2c_narudzbina_ids = explode('-',$web_b2c_narudzbina_ids);
        
        $data = [
             'web_b2c_narudzbina_ids' => $web_b2c_narudzbina_ids,
        ];

        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_predracun', $data);

        return $pdf->stream();
    }



    // AJAX
    public function narudzbineSearch() {

        $rec = trim(Input::get('articles'));
        $narudzbina_id = Input::get('narudzbina_id');

        $not_ids = 'r.roba_id NOT IN (';
        $artikli = DB::table('web_b2c_narudzbina_stavka')->select('roba_id')->where('web_b2c_narudzbina_id',$narudzbina_id)->get();
        
        if(count($artikli) > 0){            
            foreach ($artikli as $row) {
                $not_ids .= $row->roba_id.',';

            }
            $not_ids = substr($not_ids, 0, -1).') AND ';
        }else{
             $not_ids = '';
        }

        // if(AdminOptions::vodjenje_lagera() == 1){
        //     $lager_join = 'INNER JOIN lager l ON r.roba_id = l.roba_id ';
        //     $lager_where = 'l.kolicina <> 0 AND ';
        // }else{
            $lager_join = '';
            $lager_where = '';
        // }

        is_numeric($rec) ? $robaIdFormatiran = "r.roba_id = '" . $rec . "' OR " : $robaIdFormatiran = "";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $skuFormatiran = "sku ILIKE '%" . $rec . "%' ";
        $sifraISFormatiran = "r.sifra_is ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = DB::select("SELECT r.roba_id, naziv_web, grupa, sku, r.sifra_is FROM roba r ".$lager_join."INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id INNER JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE ".$lager_where."flag_aktivan=1 AND flag_prikazi_u_cenovniku=1 AND ".$not_ids."(" . $robaIdFormatiran . "" . $nazivFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . " OR " . $skuFormatiran . " OR " .  $sifraISFormatiran .") ORDER BY grupa ASC");
        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list'>";
        foreach ($articles as $article) {
            $list .= "
                <li class='articles_list__item'>
                    <a class='articles_list__item__link' href='" . AdminOptions::base_url() . "admin/narudzbina/".$narudzbina_id."/" . $article->roba_id . "'>"
                        ."<span class='articles_list__item__link__small'>" . $article->sku . "</span>"
                        ."<span class='articles_list__item__link__small'>" . $article->roba_id . "</span>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </a>
                </li>";
        }

        $list .= "</ul>";
        echo $list; 
    }


    public function promena_statusa_narudzbine(){
        
        $status=Input::get('status_target');
        $web_b2c_narudzbina_ids=Input::get('web_b2c_narudzbina_ids');
        $confirm = Input::get('confirm') == 1;
        $response_ids = [];
        $exception = false;
        $checkLager = true;

        foreach(DB::table('web_b2c_narudzbina')->whereIn('web_b2c_narudzbina_id',$web_b2c_narudzbina_ids)->get() as $web_b2c_narudzbina){
            switch($status){
                case "1":

                    if($web_b2c_narudzbina->prihvaceno == 0 && $web_b2c_narudzbina->realizovano == 0 && $web_b2c_narudzbina->stornirano == 0){                    
                        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->update(array('prihvaceno'=>1,'poslednja_izmena'=>date('Y-m-d H:i:s')));

                        if(AdminOptions::gnrl_options(3035) == 1){
                            AdminNarudzbine::mailOrderNotificationToClient($web_b2c_narudzbina->web_b2c_narudzbina_id, 'prihvacena');
                        }

                        $response_ids[] = $web_b2c_narudzbina->web_b2c_narudzbina_id;
                    }else{
                        $exception = true;
                    }

                break;
                
                 case "2":
                    if($web_b2c_narudzbina->prihvaceno == 1 && $web_b2c_narudzbina->realizovano == 0 && $web_b2c_narudzbina->stornirano == 0){
                        if(!$confirm){                        
                            foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->get() as $row){
                                $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
                                $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
                                $lager = DB::table('lager')->where(array('roba_id'=>$row->roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
                                $kolicina = 0;
                                if($lager){
                                    $kolicina = $lager->kolicina;
                                }
                                if($row->kolicina > $kolicina){
                                    $checkLager = false;
                                    break;
                                }
                            }
                        }

                        if($checkLager || $confirm){
                            foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->get() as $row){
                                AdminNarudzbine::rezervacija($row->roba_id,$row->kolicina);
                            }
                            
                            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->update(array('realizovano'=>1,'poslednja_izmena'=>date('Y-m-d H:i:s')));

                            $response_ids[] = $web_b2c_narudzbina->web_b2c_narudzbina_id;
                        }
                    }else{
                        $exception = true;
                    }
                break;
                case "3":
                    if($web_b2c_narudzbina->stornirano == 0){
                        $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->pluck('realizovano');
                        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->get() as $row){
                            if($realizovna==1){
                                AdminNarudzbine::rezervacija($row->roba_id,-1*$row->kolicina,true,false);
                            }else{
                                AdminNarudzbine::rezervacija($row->roba_id,$row->kolicina,false,true);
                            }
                            
                        }
                        

                        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina->web_b2c_narudzbina_id)->update(array('stornirano'=>1,'realizovano'=>0,'poslednja_izmena'=>date('Y-m-d H:i:s')));

                        $response_ids[] = $web_b2c_narudzbina->web_b2c_narudzbina_id;
                    }else{
                        $exception = true;
                    }
                break;
            }
        }

        if($status == '1'){
            AdminSupport::saveLog('NARUDZBINA_STATUS_PRIHVATI', $response_ids);
        }else if($status == '2'){
            AdminSupport::saveLog('NARUDZBINA_STATUS_REALIZUJ', $response_ids);
        }else if($status == '3'){
            AdminSupport::saveLog('NARUDZBINA_STATUS_STORNIRAJ', $response_ids);
        }

        return Response::json(['ids' => $response_ids, 'exception' => $exception, 'check_stock' => $checkLager]);

     
    }

    public function porudzbina_vise(){
        $web_b2c_narudzbina_id=Input::get('web_b2c_narudzbina_id');
        
        foreach(DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            echo '  <ul class="order-more-details-list">
                
                    <li class="row">
                        <span class="medium-6 columns">Broj porudžbine:</span>
                        <span class="medium-6 columns">'.$row->broj_dokumenta.'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Datum porudžbine:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Nacin isporuke:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::narudzbina_ni($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Način plaćanja:</span>
                        <span class="medium-6 columns">'.AdminNarudzbine::narudzbina_np($row->web_b2c_narudzbina_id).'</span>
                    </li>
                    
                    <li class="row">
                        <span class="medium-6 columns">Napomena:</span>
                        <span class="medium-6 columns">'.$row->napomena.'</span>
                    </li>
            
                </ul>';
        }
        echo '
        <hr>
        <ul class="order-more-product-list">
            <li class="row titles">
            <span class="medium-7 columns">Naziv proizvoda:</span>
            <span class="medium-2 columns">Cena:</span>
            <span class="medium-1 columns">Količina:</span>
            <span class="medium-2 columns">Ukupna cena:</span>
        </li>
        ';
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row2){
            echo '<li class="row">
                        <span class="medium-7 columns">'.AdminNarudzbine::artikal_narudzbina($row2->web_b2c_narudzbina_stavka_id).'</span>
                        <span class="medium-2 columns">'.AdminNarudzbine::artikal_cena_naruzbina($row2->web_b2c_narudzbina_stavka_id).'</span>
                        <span class="medium-1 columns">'.$row2->kolicina.'</span>
                        <span class="medium-2 columns">'.AdminNarudzbine::artikal_cena_stavka($row2->web_b2c_narudzbina_stavka_id).'</span>
                    </li>           
                ';
        }
        echo '
        <hr>
        <li class="row">
            <span class="medium-9 columns"><b>Ukupno:</b></span>
            <span class="medium-3 columns">'.AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($web_b2c_narudzbina_id)).'</span>
        </li>
        </ul>';
    }

    public function narudzbina_kupac(){
        $data = Input::get();
        $kupac = AdminKupci::kupacPodaci($data['web_kupac_id']);
        return View::make('admin/partials/ajax/narudzbina_kupac',array('kupac' => $kupac))->render();
    }

    public function deleteStavka() {

        $stavka_id = Input::get('stavka_id');
        $stavka = DB::table('web_b2c_narudzbina_stavka')->select('roba_id','web_b2c_narudzbina_id','kolicina')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->first();
        $web_b2c_narudzbina_id = $stavka->web_b2c_narudzbina_id;
        $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('realizovano');

        if($realizovna==1){
            AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina,true,false);
        }else{
            AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina,false,true);
        }


        AdminSupport::saveLog('NARUDZBINA_STAVKA_OBRISI', array($stavka_id));
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->delete();

        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->update(['poslednja_izmena'=>date('Y-m-d H:i:s')]);

        //drip
        if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
            $dbOrder = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->first();
            $drip = new Drip();
            $drip->addOrUpdateOrder($dbOrder,"updated");
        }        
    }

   
    public function prihvati(){
        $narudzbina_id = Input::get('narudzbina_id');
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['prihvaceno' => 1,'poslednja_izmena'=>date('Y-m-d H:i:s')]);

        AdminSupport::saveLog('NARUDZBINA_STATUS_PRIHVATI', array($narudzbina_id));

        if(AdminOptions::gnrl_options(3035) == 1){
            AdminNarudzbine::mailOrderNotificationToClient($narudzbina_id,'prihvacena');
        }

        if(AdminOptions::info_sys('infograf')){
            AdminOrderIS::createOrderInfografById($narudzbina_id);
        }
    }

    public function realizuj(){
        $narudzbina_id = Input::get('narudzbina_id');
        $confirm = Input::get('confirm') == 1;
        $checkLager = true;

        $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();

        if(!$confirm){ 
            foreach($stavke as $stavka){
                $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
                $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
                $lager = DB::table('lager')->where(array('roba_id'=>$stavka->roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
                $kolicina = 0;
                if($lager){
                    $kolicina = $lager->kolicina;
                }
                if($stavka->kolicina > $kolicina){
                    $checkLager = false;
                    break;
                }            
            }
        }

        if($checkLager || $confirm){        
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['realizovano' => 1,'poslednja_izmena'=>date('Y-m-d H:i:s')]);

            foreach($stavke as $stavka){
                AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina);
            }

            AdminSupport::saveLog('NARUDZBINA_STATUS_REALIZUJ', array($narudzbina_id));


            if(AdminOptions::gnrl_options(3035) == 1){
                AdminNarudzbine::mailOrderNotificationToClient($narudzbina_id,'realizovana');
            }
        }

        return Response::json(['check_stock' => $checkLager]);
    }

   public function storniraj(){
        $narudzbina_id = Input::get('narudzbina_id');
        $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('realizovano');
        $kupac = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('web_kupac_id');
        $kupac_bod = DB::table('web_kupac')->where('web_kupac_id',$kupac)->pluck('bodovi');
        $popust=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('popust');
        $bodovi = $popust/20;
        $novo_stanje = $kupac_bod+$bodovi;

        $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            if($realizovna==1){
                AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina,true,false);
            }else{
                AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina,false,true);
            }            
        }

        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['stornirano' => 1,'poslednja_izmena'=>date('Y-m-d H:i:s')]);
        DB::table('web_kupac')->where('web_kupac_id', $kupac)->update(['bodovi' => $novo_stanje]);

        AdminSupport::saveLog('NARUDZBINA_STATUS_STORNIRAJ', array($narudzbina_id));
    }

    public function nestorniraj(){
        $narudzbina_id = Input::get('narudzbina_id');
        $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('realizovano');
        $kupac = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('web_kupac_id');
        $kupac_bod = DB::table('web_kupac')->where('web_kupac_id',$kupac)->pluck('bodovi');
        $popust=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('popust');
        $bodovi = $popust/20;
        $novo_stanje = $kupac_bod-$bodovi;

        $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            if($realizovna==1){
                AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina,true,false);
            }else{
                AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina,false,true);
            }            
        }
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['stornirano' => 0,'poslednja_izmena'=>date('Y-m-d H:i:s')]);
        DB::table('web_kupac')->where('web_kupac_id', $kupac)->update(['bodovi' => $novo_stanje]);
        
        AdminSupport::saveLog('NARUDZBINA_ODSTORNIRAJ', array($narudzbina_id));

    }
    public function editCena(){
            $narudzbina_id = Input::get('narudzbina_id');  
            $stavka_id = Input::get('stavka_id');         
            $val = Input::get('val');   
                       
            DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_stavka_id'=> $stavka_id ))->update(['jm_cena' => $val,'poslednja_izmena'=>date('Y-m-d H:i:s')]);
            $id = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_stavka_id'=> $stavka_id ))->pluck('roba_id');
            AdminSupport::saveLog('NARUDZBINA_STAVKA_CENA_IZMENI', array($stavka_id)); 

        $cena_artikala = AdminNarudzbine::ukupnaCena($narudzbina_id);
        if(AdminOptions::web_options(133) == 1 AND AdminCommon::troskovi_isporuke($narudzbina_id)>0){
            $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['cena_artikla'] = $cena_artikala;
        }else{
            $results['ukupna_cena'] = $cena_artikala;
        }
        return json_encode($results);      
    }
    public function editTrosak(){
            $narudzbina_id = Input::get('narudzbina_id');  
            $stavka_id = Input::get('stavka_id');         
            $troskovi_isporuke = Input::get('val');      
            
            DB::table('web_b2c_narudzbina')->where(array('web_b2c_narudzbina_id'=> $narudzbina_id ))->update(['trosak_isporuke' => $troskovi_isporuke,'poslednja_izmena'=>date('Y-m-d H:i:s')]);

            AdminSupport::saveLog('NARUDZBINA_TROSKOVI_ISPORUKE_IZMENI', array($stavka_id)); 

            $cena_artikala = AdminNarudzbine::ukupnaCena($narudzbina_id);            

            $results['troskovi_isporuke'] = $troskovi_isporuke;
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
      
        return json_encode($results);      
    }
    public function updateKolicina() {

        $stavka_id = Input::get('stavka_id');
        $kolicina = Input::get('kolicina');
        $narudzbina_id = Input::get('narudzbina_id');
        $results = array(
            'cena_artikla' => 0,
            'troskovi_isporuke' => 0,
            'ukupna_cena' => 0,
            'lager' => 0
            );

        $roba_id = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->pluck('roba_id');

        $oldKolicina = AdminNarudzbine::kolicina($narudzbina_id,$roba_id);
        $razlika = floatval($kolicina) - $oldKolicina;
        if(AdminOptions::vodjenje_lagera() == 1){
            AdminNarudzbine::rezervacija($roba_id,$razlika,true,false);
        }
        if(AdminOptions::web_options(131) == 1){
            AdminNarudzbine::rezervacija($roba_id,$razlika,false,true);
        }
        $results['lager'] = AdminSupport::lager($roba_id);

        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $stavka_id)->update(['kolicina' => $kolicina]);
        AdminSupport::saveLog('NARUDZBINA_IZMENI_KOLICINA', array($stavka_id));

        $cena_artikala = AdminNarudzbine::ukupnaCena($narudzbina_id);
        $cena_do = DB::table('web_troskovi_isporuke')->pluck('cena_do');    
        
            if(AdminCommon::cena_dostave_custom($narudzbina_id) > 0){
                $troskovi_isporuke = AdminCommon::cena_dostave_custom($narudzbina_id);   
            }else{
                $troskovi_isporuke = AdminCommon::troskovi_isporuke($narudzbina_id);
            }
        
            $results['cena_artikla'] = number_format(floatval($cena_artikala),2,'.','');   
            $results['troskovi_isporuke'] = $troskovi_isporuke;
            if($cena_do < $cena_artikala) {        
            $results['ukupna_cena'] = number_format(floatval($cena_artikala+$troskovi_isporuke),2,'.','');
            }else{
            $results['ukupna_cena'] = number_format(floatval($cena_artikala),2,'.','');
            }

        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->update(['poslednja_izmena'=>date('Y-m-d H:i:s')]);

        return json_encode($results);

      
    }

    public function obrisi(){
        $narudzbina_id = Input::get('narudzbina_id');

        $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('realizovano');

        $stavke = DB::table('web_b2c_narudzbina_stavka')->select('roba_id','web_b2c_narudzbina_id','kolicina')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();
        foreach($stavke as $stavka){
            if($realizovna==1){
                AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina,true,false);
            }else{
                AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina,false,true);
            }
        }

        AdminSupport::saveLog('NARUDZBINA_OBRISI', array($narudzbina_id));
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->delete();
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->delete();
    }

    public function narudzbina_stavka($narudzbina_stavka_id){
        $narudzbina_stavka = DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_stavka_id'=> $narudzbina_stavka_id ))->first();

        if(is_null($narudzbina_stavka) || (!is_null($narudzbina_stavka) && $narudzbina_stavka->posta_zahtev == 1)){
            return Redirect::back();
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_stavka->web_b2c_narudzbina_id)->first();
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$narudzbina->web_kupac_id)->first();
        $narudzbina_stavka_upd = (array) $narudzbina_stavka;

        if(is_null($narudzbina_stavka->ulica_id)){
            $narudzbina_stavka_upd['ulica_id'] = $web_kupac->ulica_id;
        }
        if(is_null($narudzbina_stavka->broj)){
            $narudzbina_stavka_upd['broj'] = $web_kupac->broj;
        }
        if(is_null($narudzbina_stavka->posta_slanje_id)){
            $narudzbina_stavka_upd['posta_slanje_id'] = $narudzbina->posta_slanje_id;
        }
        if(is_null($narudzbina_stavka->web_nacin_placanja_id)){
            $narudzbina_stavka_upd['web_nacin_placanja_id'] = $narudzbina->web_nacin_placanja_id;
        }
        if(is_null($narudzbina_stavka->vrednost) || $narudzbina_stavka->vrednost == 0){
            $narudzbina_stavka_upd['vrednost'] = $narudzbina_stavka->kolicina * $narudzbina_stavka->jm_cena * 10;
        }
        if(is_null($narudzbina_stavka->masa) || $narudzbina_stavka->masa == 0){
            $narudzbina_stavka_upd['masa'] = DB::table('roba')->where('roba_id',$narudzbina_stavka->roba_id)->pluck('tezinski_faktor');
        }
        if(is_null($narudzbina_stavka->opis) || $narudzbina_stavka->opis == ''){
            $narudzbina_stavka_upd['opis'] = 'Tehnika';
        }

        $narudzbina_stavka = (object) $narudzbina_stavka_upd;


        $data = array(
            'strana'=>'narudzbina_stavka',
            'title'=>"Narudzbina stavka",
            'web_kupac'=>$web_kupac,
            'narudzbina_stavka' => $narudzbina_stavka,
            'ime_stavke' => DB::table('roba')->where('roba_id',$narudzbina_stavka->roba_id)->pluck('naziv_web')
            );
        return View::make('admin/page',$data);

    }

    public function narudzbina_stavka_update(){ 
        $data = Input::get();

        Validator::extend('bay_account', 'AdminSupport@bay_account');

        $rules = array(
            'broj_paketa' => 'numeric|digits_between:0,10',
            'broj' => 'required|max:50',
            'cena_otkupa' => 'required|numeric|digits_between:0,20',
            'vrednost' => 'numeric|digits_between:0,20',
            'masa' => 'numeric|digits_between:0,20',
            'napomena' => 'alpha_num|max:250',
            'opis' => 'required|alpha_num|max:50',
            'racun_otkupa' => 'regex:/(^[0-9\-]+$)+/|between:20,20|bay_account'
        );
        if($data['otkup_prima'] != -1){
            $rules['racun_otkupa'] = 'required|max:20';
        }

        $messages = array(
            'numeric' => 'Polje sme da sadrži samo brojeve.',
            'required' => 'Niste popunili polje.',
            'digits_between' => 'Dužina sadržaja je neodgovarajuća.',
            'between' => 'Dužina sadržaja je neodgovarajuća.',
            'regex' => 'Polje sadrži nedozvoljene karaktere.',
            'alpha_num' => 'Polje sadrži nedozvoljene karaktere.',
            'max' => 'Dužina sadržaja je neodgovarajuća.',
            'bay_account' => 'Polje sadrži nedozvoljene karaktere.'
        );


        $validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka/'.$data['web_b2c_narudzbina_stavka_id'])->withInput()->withErrors($validator->messages());
        }else{
            
            $web_b2c_narudzbina_stavka_id = $data['web_b2c_narudzbina_stavka_id'];
            unset($data['web_b2c_narudzbina_stavka_id']);
            unset($data['opstina']);
            unset($data['mesto']);


            DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->update($data);
            unset($data['broj_paketa']);
            unset($data['vrednost']);
            unset($data['masa']);


            $web_b2c_narudzbina_id = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->pluck('web_b2c_narudzbina_id');
            $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$data['posta_slanje_id']);

            $stavke = $grouped_by_posiljalac[$data['posiljalac_id']];
            if(count($stavke) > 1){
                DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id', array_map('current',$stavke))->update($data);
            }

            AdminSupport::saveLog('NARUDZBINA_STAVKA_IZMENI', array($web_b2c_narudzbina_stavka_id));
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka/'.$web_b2c_narudzbina_stavka_id);
        }
    }
    public function obrisi_city($web_b2c_narudzbina_id,$id){
        DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id', $id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }

    public function dodaj_novu_city($web_b2c_narudzbina_id,$id){
       
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->first();
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$narudzbina->web_kupac_id)->first();
        $narudzbina_stavka = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->first();
        $posta_zahtev = DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$id)->first();
        
        $partner_id=$narudzbina_stavka->posiljalac_id;
        $partner = DB::table('partner')->where('partner_id',$partner_id)->pluck('ptt_cityexpress');

            $data_insert = array(
            'web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,
            'posta_slanje_id'=> 2,
            'posta_zahtev_api_id'=> $id,
            'tip_obavestenja_id'=> '0',
            'vreme_dostave'=>'100',
            'dostava_subotom'=>'false',
            'posiljalac_ptt'=>$partner,
            'primalac_ptt'=>8690,
            'posiljalac_id'=> $partner_id,
            'primalac_id'=> $web_kupac->web_kupac_id,
            'masa'=>0,
            'otkupnina'=>4,
            'opis'=>'Bez opisa',
            'napomena'=>'',
            );
            unset($data_insert['posta_zahtev_api_id']);
            DB::table('posta_zahtev_api')->insert($data_insert);
        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }

       public function narudzbina_stavka_city($web_b2c_narudzbina_id,$id){
       
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->first();
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$narudzbina->web_kupac_id)->first();

        $stavke=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->get();
        $narudzbina_stavka = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->first();
        $posta_zahtev = DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$narudzbina->web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$id)->first();
        
        $partner_id=$posta_zahtev->posiljalac_id;
        $partner = DB::table('partner')->where('partner_id',$partner_id)->pluck('ptt_cityexpress');

        $cena_otkupa=0;
        foreach ($stavke as $stavka) {

            $cena_otkupa += $stavka->jm_cena * $stavka->kolicina;
        }
        
        $data = array(
            'strana'=>'narudzbina_stavka_city',
            'title'=>"Narudzbina stavka CityExpress",
            'web_kupac'=>$web_kupac,
            'narudzbina' => $narudzbina,
            'narudzbina_stavka'=> $narudzbina_stavka,            
            'posta_zahtev'=> $posta_zahtev,
            'stavke'=> $stavke,
            'cena_otkupa'=> (($posta_zahtev->cena_otkupa)>=0) ? $posta_zahtev->cena_otkupa : $cena_otkupa,
            'posta_zahtev_api_id'=> $id,
            'tip_obavestenja_id'=> (isset($posta_zahtev->tip_obavestenja_id)) ? $posta_zahtev->tip_obavestenja_id : '0',
            'vreme_dostave'=> (isset($posta_zahtev->vreme_dostave)) ? $posta_zahtev->vreme_dostave : '100',
            'dostava_subotom'=> (isset($posta_zahtev->dostava_subotom)) ? $posta_zahtev->dostava_subotom : 'false',
            'posiljalac_ptt'=> (isset($posta_zahtev->posiljalac_ptt)) ? $posta_zahtev->posiljalac_ptt : '',
            'partner_ptt_cityexpress_id'=> $partner,
            'primalac_ptt'=>  (isset($posta_zahtev->primalac_ptt)) ? $posta_zahtev->primalac_ptt : '',
            'kupac_ptt_cityexpress_id'=> (!empty($web_kupac->ptt_cityexpress)) ? $web_kupac->ptt_cityexpress : '',
            'posiljalac_id'=> (isset($posta_zahtev->posiljalac_id)) ? $posta_zahtev->posiljalac_id : 1,
            'masa'=>  (isset($posta_zahtev->masa)) ? $posta_zahtev->masa : 0,
            'opis'=>(isset($posta_zahtev->opis)) ? $posta_zahtev->opis : 'Bez opisa',
            'otkupnina'=>(isset($posta_zahtev->otkupnina)) ? $posta_zahtev->otkupnina : 4,
            'napomena'=>(isset($posta_zahtev->napomena)) ? $posta_zahtev->napomena : '',
            'ime_stavke' => DB::table('roba')->where('roba_id',$narudzbina_stavka->roba_id)->pluck('naziv_web')

            );      
                      
        return View::make('admin/page',$data);

    }
   
    public function narudzbina_stavka_city_update(){ 
        $data = Input::get();
        $rules = array(
            'adresa_kupca' => 'required',
            'posiljalac_ptt' => 'required|not_in:0',
            'primalac_ptt' => 'required|not_in:0',
            'masa' => 'numeric',
            'posiljalac_ulica' => 'required'
        );

        $messages = array(           
            'required' => 'Niste popunili polje.',
            'not_in' => 'Niste odabrali postanski broj i grad!!!',
            'numeric' => 'Polje mora biti broj.'           
        );

        $validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka-city/'.$data['web_b2c_narudzbina_id'].'/'.$data['posta_zahtev_api_id'])->withInput()->withErrors($validator->messages());
        }else{
              
            DB::table('web_kupac')->where('web_kupac_id', $data['primalac_id'])->update(array('ptt_cityexpress'=>$data['primalac_ptt'],'telefon'=>$data['kupac_telefon'],'adresa'=>$data['adresa_kupca'],'email'=>$data['kupac_mail']));
            DB::table('partner')->where('partner_id', $data['posiljalac_id'])->update(array('ptt_cityexpress'=>$data['posiljalac_ptt'],'adresa'=>$data['posiljalac_ulica']));            
           
            if($data['posiljalac_id'] != 1 && $data['kolicina']>0){
                if(!isset($data['kolicina'])){
                    $data['kolicina']=1;
                }
                if(DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id',$data['stavka_id'])->pluck('stavka_id') != $data['stavka_id']){
                    DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->insert(['stavka_id'=>$data['stavka_id'],'kolicina'=>$data['kolicina'],'posta_zahtev_api_id'=>$data['posta_zahtev_api_id']]);
                }else{
                    DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id',$data['stavka_id'])->update(array('kolicina'=>$data['kolicina']));
                }
            }          
            DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$data['stavka_id'])->update(array('posiljalac_id'=>$data['posiljalac_id']));

                unset($data['web_b2c_narudzbina_stavka_id']);
                unset($data['kupac_telefon']);
                unset($data['posiljalac_ulica']);
                unset($data['adresa_kupca']);
                unset($data['kupac_mail']);
                unset($data['stavka_id']);
                unset($data['kolicina']);
                
            DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id', $data['web_b2c_narudzbina_id'])->where('posta_zahtev_api_id', $data['posta_zahtev_api_id'])->update($data);

           
           
            $message = 'OK';
            AdminSupport::saveLog('NARUDZBINA_STAVKA_CITY_IZMENI', array($data['web_b2c_narudzbina_id']));
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka-city/'.$data['web_b2c_narudzbina_id'].'/'.$data['posta_zahtev_api_id'])->with('message',$message);
       }
    }

    public function posta_posalji($web_b2c_narudzbina_id,$posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna'=>1))->first();
        if(is_null($posta_slanje)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($narudzbina) || $narudzbina->posta_slanje_poslato == 1){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }

        $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$posta_slanje_id);

        $message = '';
        $data = array();
        $data['dexpress'] = array();
        foreach($grouped_by_posiljalac as $posiljalac_id => $stavke){
            if(count($stavke) > 0){
                if($posiljalac_id != 1){
                    $posiljalac = AdminPartneri::partner($posiljalac_id);
                    if(is_null($posiljalac)){
                        continue;
                    }
                    // else{
                    //     if(is_null($posiljalac->mail) || $posiljalac->mail == ''){
                    //         continue;
                    //     }
                    // }
                    //send to dobavljac
                }

                if($posta_slanje_id == 3){
                    $response = DExpress::generate_data($stavke,$posiljalac_id,$narudzbina->web_kupac_id);
                    if($response->success){
                        $data['dexpress'][] = $response->data;
                    }else{
                        $message = $response->message;
                        break;
                    }
                }
            }
        }

        if($posta_slanje_id == 3 && count($data['dexpress']) > 0 && $message == ''){
            // $response = DExpress::convertToCsv($data['dexpress']);
            $response = DExpress::convertToString($data['dexpress']);
           
            if($response->success){
                // //old, send on mail
                // Mailer::dexpress_send('sasa.zivkovic@tico.rs',$response->path);
                // if(!is_null($response->path)){
                //     File::delete($response->path);
                // }

                // $client = new SOAPClient('http://callc.dailyexpress.rs/DePreCalls.asmx?WSDL');
                // $soap_response = $client->AddPrecalls(array(
                //     'authCode' => $posta_slanje->auth_code,
                //     'PreCallData' => $response->result
                // ));
                // if($soap_response->AddPrecallsResult == "OK"){

                if(true){
                    $posta_paket_curr = DB::table('web_b2c_narudzbina_stavka')->max('posta_paket');
                    foreach($grouped_by_posiljalac as $posiljalac_id => $stavke){
                        $posta_paket = null;
                        if(count($stavke) > 0){
                            if($posiljalac_id == 1){
                                $broj_paketa = AdminNarudzbine::broj_paketa($stavke);
                                foreach(range(1,$broj_paketa) as $pack){
                                    $posta_paket = !is_null($posta_paket_curr) ? ($posta_paket_curr + $pack) : 1001;
                                }
                            }
                            DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',array_map('current',$stavke))->update(array('posta_zahtev'=>1,'posta_paket'=>$posta_paket));
                        }
                    }

                    $maticnaFirma = DB::table('preduzece')->where('preduzece_id',1)->first();
                    Mailer::send($maticnaFirma->email,$maticnaFirma->email,'DExpress zahtev ('.$maticnaFirma->naziv.')',$response->result);
                    $message = 'Zahtev je poslat kurirskoj službi.';
                }else{
                    $message = 'Greška DExpress servisa.';
                }

            }
        }
        if(!is_null($message) && $message != ''){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$message);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
    }

    public function posta_resetuj($web_b2c_narudzbina_id,$posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna'=>1))->first();
        if(is_null($posta_slanje)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
        $narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($narudzbina)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }

        $grouped_by_posiljalac = AdminNarudzbine::grouped_by_posiljalac($web_b2c_narudzbina_id,$posta_slanje_id,true);

        foreach($grouped_by_posiljalac as $stavke){
            if(count($stavke) > 0){
                DB::table('web_b2c_narudzbina_stavka')->whereIn('web_b2c_narudzbina_stavka_id',array_map('current',$stavke))->update(array('posta_zahtev'=>0));
            }
        }

        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }

    public function posta_odgovor(){
        $data = Request::all();
        $result = "ERROR";

        if(isset($data['ReferenceID'])){
            $reference_id = intval($data['ReferenceID']);
            $stavke_query = DB::table('web_b2c_narudzbina_stavka')->where('posta_reference_id',$reference_id);

            if($stavke_query->count() > 0){
                $status_code = isset($data['Status']) && is_numeric($data['Status']) ? intval($data['Status']) : -1;
                $status_id = DB::table('posta_status')->where(array('posta_slanje_id' => 3, 'code'=>$status_code))->pluck('posta_status_id');
                $json_data = json_encode($data);
                $upd_data = array(
                    'posta_status_id'=>$status_id,
                    'posta_response_data'=>$json_data,
                    'posta_zahtev'=>0
                    );
                $stavke_query->update($upd_data);

                $result = "OK";
            }

        }
        return $result;
    }
    public function posta_posalji_city($web_b2c_narudzbina_id,$posta_zahtev_api_id,$posta_slanje_id){

        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna'=>1))->first();
        if(is_null($posta_slanje)){
            return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
        }
        $stavke=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->first();

        if($posta_slanje_id == 2 && $stavke->posiljalac_id == 1){
            $results = CityExpress::createShipment($web_b2c_narudzbina_id,$posta_zahtev_api_id);

            if($results->success){

                $data= $results->results->CreatedShipmentId;
                $tracking_num= $results->results->TrackingCode;
                DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_slanje_id',$posta_slanje_id)->update(array('posta_odgovor_id'=>$data,'tracking_num'=>$tracking_num ));
                $posta_odgovor_id = DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_slanje_id',$posta_slanje_id)->pluck('posta_odgovor_id'); 

                // nalepnice
                $labela = CityExpress::shippingLabel($posta_odgovor_id);
                DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_slanje_id',$posta_slanje_id)->update(array('labela'=>ltrim($labela->results->PdfDocument)));
                // request
                // $request = CityExpress::requestPickup($posta_odgovor_id);
                // DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_slanje_id',$posta_slanje_id)->update(array('dokument'=>ltrim($request->results->PickupListDocument)));

                $poruka = 'Zahtev je poslat kurirskoj službi.';
                return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$poruka);
            }else{
                $message = 'Greška CityExpress servisa-'.$results->message;
                return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$message);                
            }
        }elseif ($posta_slanje_id == 2 && $stavke->posiljalac_id != 1) {
            $results = CityExpress::createShipmentPlain($web_b2c_narudzbina_id,$posta_zahtev_api_id);
            
            $stavka_id = DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('posta_zahtev_api_id',$posta_zahtev_api_id)->pluck('stavka_id');

            if($results->success){

                $data= $results->results->CreatedShipmentId;
                DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_slanje_id',$posta_slanje_id)->update(array('posta_odgovor_id'=>$data));
                DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('web_b2c_narudzbina_stavka_id',$stavka_id)->update(array('posta_zahtev'=>1));
                $poruka = 'Zahtev je poslat kurirskoj službi.';
                return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$poruka);
            }else{
                $message = 'Greška CityExpress servisa-'.$results->message;
                return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$message);                
            }
        }
    }
    public function posta_cancel_city($web_b2c_narudzbina_id,$posta_zahtev_api_id){

        $stavke=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->first();

        $results = CityExpress::CancelShipment($stavke->posta_odgovor_id);
        DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->delete();
        $poruka = 'Poslat je zahtev za otkazivnje pošiljke kurirskoj službi.';
        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$poruka);
    }

    public function posta_nalepnice($web_b2c_narudzbina_id,$posta_zahtev_api_id){

        $labela=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->pluck('labela');        
        
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="nalepnica.pdf"');
        echo base64_decode($labela);

        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }
    public function posta_dokument($web_b2c_narudzbina_id,$posta_zahtev_api_id){

        $dokument=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->pluck('dokument');        
        
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename="dokument.pdf"');
        echo base64_decode($dokument);

        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id);
    }

    public function priprema($web_b2c_narudzbina_id,$posta_zahtev_api_id){

        DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->update(array('pickup_id'=>1));        
        $poruka = 'Narudžbina je pripremljena za kurira';
        return Redirect::to(AdminOptions::base_url().'admin/narudzbina/'.$web_b2c_narudzbina_id)->with('message',$poruka);;
    }

    public function priprema_posalji(){

        $posta_odgovor_ids=array_map(function($items){return intval($items->posta_odgovor_id);},DB::table('posta_zahtev_api')->select('posta_odgovor_id')->where('pickup_id','=',1)->get());    

        $results = CityExpress::requestPickupForShipments($posta_odgovor_ids);
               
        if($results->success){
            
            DB::table('posta_zahtev_api')->where('pickup_id','=',1)->update(array('pickup_id'=>$results->results->PickupId,'dokument'=>ltrim($results->results->PickupListDocument)));

            // $dokument = DB::table('posta_zahtev_api')->where('pickup_id','=',$results->results->PickupId)->pluck('dokument'); 

            // header('Content-type: application/pdf');
            // header('Content-Disposition: attachment; filename="lista_za_preuzimanje-'.date("d/m/Y").'.pdf"');
            // echo base64_decode($dokument);
            
            $poruka = 'Poslat je zahtev kurirskoj službi.';
            return Redirect::to(AdminOptions::base_url().'admin/porudzbine/sve/0/0/0/0/0')->with('message',$poruka)->with('city_pdf_pickupid',$results->results->PickupId);
        }else{            
            $poruka = 'Greška CityExpress servisa-'.$results->message;
            return Redirect::to(AdminOptions::base_url().'admin/porudzbine/sve/0/0/0/0/0')->with('message',$poruka);
        }        
    }
    public function city_pdf($pickup_id){

            $dokument = DB::table('posta_zahtev_api')->where('pickup_id','=',$pickup_id)->pluck('dokument'); 

            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="lista_za_preuzimanje-'.date("d/m/Y").'.pdf"');
            echo base64_decode($dokument);     
    }
    
    public function porudzbinaDelete($web_b2c_narudzbina_id){
        AdminSupport::saveLog('NARUDZBINA_OBRISI', array($web_b2c_narudzbina_id));
        DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();
        DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();


        return Redirect::back();
       
    }    

    public function porudzbinaDeleteMore(){
        $web_b2c_narudzbina_ids = Input::get('web_b2c_narudzbina_ids');

        AdminSupport::saveLog('NARUDZBINA_OBRISI', $web_b2c_narudzbina_ids);

        foreach($web_b2c_narudzbina_ids as $web_b2c_narudzbina_id){

            $realizovna=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('realizovano');

            $stavke = DB::table('web_b2c_narudzbina_stavka')->select('roba_id','web_b2c_narudzbina_id','kolicina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->get();
            foreach($stavke as $stavka){
                if($realizovna==1){
                    AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina,true,false);
                }else{
                    AdminNarudzbine::rezervacija($stavka->roba_id,$stavka->kolicina,false,true);
                }
            }
            
        
            DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();
            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->delete();
        }
       
    }
    public function obrisi_stavku($stavka_id){
        $posta_zahtev_api= DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id',$stavka_id)->pluck('posta_zahtev_api_id');
        $web_b2c_narudzbina_id = DB::table('posta_zahtev_api')->where('posta_zahtev_api_id',$posta_zahtev_api)->pluck('web_b2c_narudzbina_id');
       
        DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id', $stavka_id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/narudzbina-stavka-city/'.$web_b2c_narudzbina_id.'/'.$posta_zahtev_api);
    }
    public function partner_data(){
        $partner_id = Input::get('partner_id');       
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $partnerData = array(            
            'adresa' => $partner->adresa,
            'ptt_cityexpress' => $partner->ptt_cityexpress
        );
        
        return json_encode($partnerData);
    }
    public function mail_send(){        

            $id = Input::get('narudzbina_id');
            $api_id=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$id)->first();
            $broj_dokumenta = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$id)->pluck('broj_dokumenta');

            $broj=$broj_dokumenta.'-'.$api_id->posta_zahtev_api_id;


            $partner_mail = DB::table('partner')->where('partner_id',$api_id->posiljalac_id)->pluck('mail');
            $stavke = DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->select('web_b2c_narudzbina_stavka.roba_id','web_b2c_narudzbina_stavka_posta_zahtev.kolicina',DB::raw('(select naziv_web from roba where roba_id=web_b2c_narudzbina_stavka.roba_id) as naziv'),DB::raw('(select sifra_d from roba where roba_id=web_b2c_narudzbina_stavka.roba_id) as sifra'))->leftJoin('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka.web_b2c_narudzbina_stavka_id','=','web_b2c_narudzbina_stavka_posta_zahtev.stavka_id')->where('posta_zahtev_api_id',$api_id->posta_zahtev_api_id)->orderBy('posta_zahtev_api_id', 'asc')->get(); 

            $body=" <p>Poštovani,<br /> Porudžbina broj ".$broj." za robu:</br> 
                        <table>
                        <tr>
                            <th>Artikal</th>
                            <th>Šifra Artikla</th>
                            <th>Količina</th>
                        </tr>";
            foreach ($stavke as $stavka) {
                
            $body.="<tr>
                        <td>".$stavka->naziv."</td>
                        <td>".$stavka->sifra."</td>
                        <td>".$stavka->kolicina."</td>
                    </tr>";
            }
            $body.='</table>                     
                        <b>je izvršena i City Express je dobio nalog za preuzimanje iste. Molimo Vas da pripremite robu za preuzimanje. <br /></p>';           
                 

           $subject="CityExpress zahtev za preuzimanje paketa ";
           Mailer::send($partner_mail,$partner_mail,$subject, $body);
               
            
    }  
    //slanje mejla kod promene statusa


    public function orders(){ 
        $action = Input::get('action');
        $realizedIds = [];

        if($action == 'dodela_statusa' || $action == 'uklanjanje_statusa') {
            $orders_ids = Input::get('orders_ids');
            $narudzbina_status_id = Input::get('narudzbina_status_id');

            $redovi = array_map(function($web_b2c_narudzbina_id) use ($narudzbina_status_id){
                return '('.$narudzbina_status_id.','.$web_b2c_narudzbina_id.')';
            },$orders_ids);

            if($action == 'dodela_statusa'){
                if(strtolower(DB::table('narudzbina_status')->where('narudzbina_status_id',$narudzbina_status_id)->pluck('naziv')) == 'povrat'){
                    $novi_narudzbina_statusi = DB::select("SELECT * FROM (VALUES ".implode(',',$redovi).") redovi(narudzbina_status_id,web_b2c_narudzbina_id) WHERE (narudzbina_status_id,web_b2c_narudzbina_id) NOT IN (SELECT narudzbina_status_id,web_b2c_narudzbina_id FROM narudzbina_status_narudzbina)");
                    foreach($novi_narudzbina_statusi as $row){
                        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->first();
                        if($web_b2c_narudzbina->prihvaceno == 1 && $web_b2c_narudzbina->stornirano == 0){
                            foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->get() as $stavka){
                                AdminNarudzbine::rezervacija($stavka->roba_id,-1*$stavka->kolicina);
                            }
                            DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->update(['realizovano'=>0]);
                            $realizedIds[] = $row->web_b2c_narudzbina_id;
                        }
                    }
                }

                DB::statement("INSERT INTO narudzbina_status_narudzbina (narudzbina_status_id,web_b2c_narudzbina_id) SELECT * FROM (VALUES ".implode(',',$redovi).") redovi(narudzbina_status_id,web_b2c_narudzbina_id) WHERE (narudzbina_status_id,web_b2c_narudzbina_id) NOT IN (SELECT narudzbina_status_id,web_b2c_narudzbina_id FROM narudzbina_status_narudzbina)");

                AdminSupport::saveLog('NARUDZBINA_DODATNI_STATUS_DODAJ',$orders_ids,[$narudzbina_status_id]);
            }else if($action == 'uklanjanje_statusa'){ 
                DB::statement("DELETE FROM narudzbina_status_narudzbina WHERE (narudzbina_status_id,web_b2c_narudzbina_id) IN (SELECT narudzbina_status_id,web_b2c_narudzbina_id FROM (VALUES ".implode(',',$redovi).") redovi(narudzbina_status_id,web_b2c_narudzbina_id))");
                
                AdminSupport::saveLog('NARUDZBINA_DODATNI_STATUS_UKLONI',$orders_ids,[$narudzbina_status_id]);
            }


            $statusResponse = [];
            foreach($orders_ids as $web_b2c_narudzbina_id){
                $statusResponse[intval($web_b2c_narudzbina_id)] = AdminNarudzbine::status_narudzbine($web_b2c_narudzbina_id);
            }

            return Response::json(['status_response' => $statusResponse, 'realized_ids' => $realizedIds]);        

        }
    }
}