<?php

class AdminNacinPlacanjaController extends Controller {

	public function index($id) {
		$data = array(
	                "strana"=>'nacin_placanja',
	                "title"=> 'Način plaćanja',	  
	                "nacini_placanja" => AdminNacinPlacanja::all(),
	                "web_nacin_placanja_id" => $id,
	                "nacin_placanja" => $id > 0 ? AdminNacinPlacanja::getSingle($id) : (object) array('naziv'=>'','selected'=>1,'b2c_default'=>0,'b2b_default'=>0)    
	            );
		return View::make('admin/page', $data);
	}

	public function edit($id) {
		$inputs = Input::get();
		
		if(isset($inputs['aktivno'])){
			$aktivno = 1;
		} else {
			$aktivno = 0;
		}
		if(isset($inputs['b2c_default'])){
			$b2c_default = 1;
		} else {
			$b2c_default = 0;
		}
		if(isset($inputs['b2b_default'])){
			$b2b_default = 1;
		} else {
			$b2b_default = 0;
		}

		$validator = Validator::make($inputs, array('naziv' => 'required'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
			if($aktivno == 1 && $b2c_default == 1){
				DB::table('web_nacin_placanja')->update(array('b2c_default' => 0));
			}
			if($aktivno == 1 && $b2b_default == 1){
				DB::table('web_nacin_placanja')->update(array('b2b_default' => 0));
			}
        	if($id == 0){
        		$current = DB::select("SELECT MAX(web_nacin_placanja_id) AS max FROM web_nacin_placanja")[0]->max;
        		$web_nacin_placanja_id = $current + 1;
        		$data = array('web_nacin_placanja_id' => $web_nacin_placanja_id, 'naziv' => $inputs['naziv'], 'selected' => $aktivno);
        		if(AdminOptions::gnrl_options(3065)) {
        			$data['racun_vrsta_placanja_naziv_id'] = $inputs['racun_vrsta_placanja_naziv_id'];
        			if(empty($inputs['racun_vrsta_placanja_naziv_id'])) {
        				$data['racun_vrsta_placanja_naziv_id'] = -1;
        			}
        		}
        		if($aktivno == 1){
        			$data['b2c_default'] = $b2c_default;
        			$data['b2b_default'] = $b2b_default;
        		}else{
					$data['b2c_default'] = 0;
        			$data['b2b_default'] = 0;        			
        		}
        		DB::table('web_nacin_placanja')->insert($data);

        		AdminSupport::saveLog('SIFARNIK_NACIN_PLACANJA_DODAJ', array(DB::table('web_nacin_placanja')->max('web_nacin_placanja_id')));

        		return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/0')->withMessage('Uspešno ste sačuvali podatke.');
			} else {
        		$data = array('naziv' => $inputs['naziv'], 'selected' => $aktivno);
        		if(AdminOptions::gnrl_options(3065)) {
        			$data['racun_vrsta_placanja_naziv_id'] = $inputs['racun_vrsta_placanja_naziv_id'];
        		}
        		if($aktivno == 1){
        			$data['b2c_default'] = $b2c_default;
        			$data['b2b_default'] = $b2b_default;
        		}else{
					$data['b2c_default'] = 0;
        			$data['b2b_default'] = 0;        			
        		}
				DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->update($data);

				AdminSupport::saveLog('SIFARNIK_NACIN_PLACANJA_IZMENI', array($id));

				return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/'.$id)->withMessage('Uspešno ste sačuvali podatke.');
			}
        }

	}

	public function delete($id) {
		if(!DB::table('web_b2c_narudzbina')->where('web_nacin_placanja_id',$id)->first()) {
			AdminSupport::saveLog('SIFARNIK_NACIN_PLACANJA_OBRISI', array($id));
			DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $id)->delete();
			return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/0')->withMessage('Uspešno ste obrisali sadržaj.');
		} else {
			return Redirect::to(AdminOptions::base_url().'admin/nacin_placanja/0')->with('error_message','Ova vrsta plaćanja postoji u narudžbinama.');
		}
	}





}