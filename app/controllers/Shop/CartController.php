<?php
use Service\Drip;

class CartController extends Controller {

	public function list_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		Cart::add_to_cart($kupac_id,$korpa_id,$roba_id,1,array());

		$articleDetails = Product::gtArticleDetails($roba_id,1);

		echo json_encode(array('check_available'=>Cart::check_avaliable($roba_id),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart(),'cart_ukupno' => Cart::cena(Cart::cart_ukupno()), 'article_details' => $articleDetails));
	}

	public function quick_view_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
		$kolicina=Input::get('kolicina');
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		$available = Cart::check_avaliable($roba_id);

		$added = false;
		if($available >= $kolicina){
			Cart::add_to_cart($kupac_id,$korpa_id,$roba_id,intval($kolicina),array());
			$added = true;
		}

		echo json_encode(array('added'=>$added,'check_available'=>Cart::check_avaliable($roba_id),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart(),'cart_ukupno' => Cart::cena(Cart::cart_ukupno())));
	}

	public function wish_list_add(){
		$lang = Language::multi() ? Request::segment(1) : null;  

		$roba_id=Input::get('roba_id');
		$kupac_id = Cart::kupac_id();

 
		if(Cart::add_to_wish($kupac_id,$roba_id)){
			$message = Language::trans('Artikal je dodat na listu želja').'.';
		}else{
			$message = Language::trans('Artikal je već dodat na listu želja').'.';
		}
		
		echo json_encode(array("broj_wish"=>Cart::broj_wish(),"message"=>$message));
	}

	public function wish_list_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');		
		

		Cart::delete_wish($roba_id);

	}

	public function product_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$inputs=Input::get();
		
		$available = Cart::check_avaliable($inputs['roba_id']);
		if(Options::web_options(320)==0){
       		$validator = Validator::make($inputs, array('kolicina' => 'numeric|min:1|max:'.$available.''), array('numeric' => Language::trans('Polje za količinu moze sadržati samo brojeve!'),'min' => Language::trans('Minimalna kolicina je 1!'),'max' => Language::trans('Maksimalna kolicina je').' '.$available.'!'));
    	}else{
    		$validator = Validator::make($inputs, array('kolicina' => 'numeric|min:0.1|max:'.$available.''), array('numeric' => Language::trans('Polje za količinu moze sadržati samo brojeve!'),'min' => Language::trans('Minimalna kolicina je 100g !'),'max' => Language::trans('Maksimalna kolicina je').' '.$available.'!'));
    	}
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
			$kupac_id = Cart::kupac_id();
			$korpa_id = Cart::korpa_id();


        	//osobine
        	$osobine_arr = array();
        	if(Product::check_osobine($inputs['roba_id'])){
	        	foreach($inputs as $key => $val){
	        		if(strpos($key, 'osobine') !== false){
	        			$osobine_arr[] = $val;
	        		}
	        	}
	        	if(Options::vodjenje_lagera()==1){
	        		if(Cart::osobina_kombinacija_check_available($inputs['roba_id'],$osobine_arr,$korpa_id) < $inputs['kolicina']){
			        	$validator->getMessageBag()->add('kolicina',Language::trans('Tražena količina za datu kombinaciju nije dostupna.'));
			        	return Redirect::back()->withInput()->withErrors($validator->messages());	        			
	        		}
			    }
		    }

			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$inputs['kolicina'],$osobine_arr,null,(Options::web_options(313)==1 ? $inputs['kamata'] : 0),$inputs['projectId']);

			return Redirect::back()->withInput()->with('success_add_to_cart',true);
        }
	}
	public function konfigurator_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$inputs=Input::get();
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		$available = Cart::check_avaliable($inputs['roba_id']);
		if($available >= $inputs['kolicina']){
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$inputs['kolicina'],array());
			echo 'available';
		}else{
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],$available,array());
			echo 'not-available';
		}

	}
	public function vezani_cart_add(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$inputs=Input::get();
		$kupac_id = Cart::kupac_id();
		$korpa_id = Cart::korpa_id();
		$available = Cart::check_avaliable($inputs['roba_id']);
		$vezani_available = Cart::check_avaliable($inputs['vezani_roba_id']);
		if($vezani_available >= $inputs['kolicina']){
			$kolicina = $inputs['kolicina'];
		}else{
			$kolicina = $vezani_available;
		}

		if($available == 0){
			echo json_encode(array('success'=>false,'exists'=>0)); exit;
		}

		if(DB::table('web_b2c_korpa_stavka')->where(array('roba_id'=>$inputs['roba_id'],'web_b2c_korpa_id'=>$korpa_id))->count() == 0){
			Cart::add_to_cart($kupac_id,$korpa_id,$inputs['roba_id'],1,array());
		}

		if(!Cart::check_avaliable_vezani($kolicina,$inputs['vezani_roba_id'],$inputs['roba_id'])){
			echo json_encode(array('success'=>false,'exists'=>1)); exit;
		}

		Cart::add_to_cart($kupac_id,$korpa_id,$inputs['vezani_roba_id'],$kolicina,array(),$inputs['roba_id']);
		$articleDetails = Product::gtArticleDetails($inputs['roba_id'],1);

		echo json_encode(array('success' => true,'check_available'=>Cart::check_avaliable($inputs['vezani_roba_id']),'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),'broj_cart'=>Cart::broj_cart(),'cart_ukupno' => Cart::cena(Cart::cart_ukupno()), 'article_details' => $articleDetails));

	}

	public function cart_add_sub(){
		$lang = Language::multi() ? Request::segment(1) : null;

		$stavka_id=Input::get('stavka_id');
		$kolicina = Input::get('kolicina');

		$web_b2c_stavka = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->first();
		$roba_id = $web_b2c_stavka->roba_id;
		$old_kolicina = $web_b2c_stavka->kolicina;
		$parent_vezani_roba_id = $web_b2c_stavka->parent_vezani_roba_id;
		$jm_cena = $web_b2c_stavka->jm_cena;
		$diff_kolicina = $kolicina - $old_kolicina;
		$available = !Product::check_osobine($roba_id) ? Cart::check_avaliable($roba_id) : Cart::osobina_kombinacija_check_available($roba_id,explode('-',$web_b2c_stavka->osobina_vrednost_ids),null,$stavka_id);
		$changed = false;

		if($available >= $diff_kolicina){
			$new_kolicina = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('kolicina') + $diff_kolicina;

			if(Cart::check_avaliable_vezani($diff_kolicina,$roba_id,$parent_vezani_roba_id)){
				DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kolicina));
				$changed = true;

				DB::table('web_b2c_korpa_stavka')->where(array('web_b2c_korpa_id'=>$web_b2c_stavka->web_b2c_korpa_id,'parent_vezani_roba_id'=>$roba_id))->where('kolicina','>',$new_kolicina)->update(array('kolicina'=>$new_kolicina));
			}
		}

		$vezane_stavke = DB::table('web_b2c_korpa_stavka')->select('web_b2c_korpa_stavka_id','kolicina','jm_cena')->where(array('web_b2c_korpa_id'=>$web_b2c_stavka->web_b2c_korpa_id,'parent_vezani_roba_id'=>$roba_id))->get();
		$cartClass = new Cart();
		$vezane_stavke = array_map(function($item) use ($cartClass) {
			return (object) array('web_b2c_korpa_stavka_id' => $item->web_b2c_korpa_stavka_id, 'kolicina' => $item->kolicina, 'vezani_item_cena' => $cartClass->cena($item->jm_cena*$item->kolicina));
		},$vezane_stavke);

        if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1 && Cart::kupac_id() > 0){
            $drip = new Drip();
            $dbcart = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',$web_b2c_stavka->web_b2c_korpa_id)->first();
            $drip->addOrUpdateCart($dbcart,"updated");
        }

        $articleDetails = Product::gtArticleDetails($roba_id,$diff_kolicina);

        $troskovi_isporuke =Cart::troskovi();
		echo json_encode(
			array(
			'changed'=>$changed,
			'mini_cart_list'=>View::make('shop/themes/'.Support::theme_path().'partials/mini_cart_list')->render(),
			'broj_cart'=>Cart::broj_cart(),
			'cart_ukupno' => Cart::cena(Cart::cart_ukupno()),
			'cart_item_ukupno_tezina' => Cart::cena(Cart::cart_ukupno()+$troskovi_isporuke-Cart::popust()->iznos),
			'cart_item_ukupno_dostava' => Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave()),
			'cart_item_ukupno' => Cart::cena($jm_cena*$kolicina),
			'cart_ukupno' => Cart::cena(Cart::cart_ukupno()),
			'vezane_stavke' => $vezane_stavke,
			'bodovi_ostvareni_bodovi_korpa' => Cart::bodoviOstvareniBodoviKorpa(),
			'bodovi_popust_bodovi_korpa' => Cart::bodoviPopustBodoviKorpa(),
			'vauceri_popust_cena_korpa' => cart::cena(Cart::vauceriPopustCenaKorpa()),
			'troskovi_isporuke_valuta'=>cart::cena($troskovi_isporuke),
			'troskovi_isporuke'=>$troskovi_isporuke,
			'article_details' => $articleDetails,
			'iznos_popusta' => Cart::popust()->iznos,
			'cena_sa_popustom' => Cart::cena(Cart::cart_ukupno() - Cart::popust()->iznos),
			'cart_discount' => Cart::cena(Cart::popust()->iznos), 
			'cart_discount_percent' => Cart::popust()->procenat
			)
		);
	}
	public function cart_stavka_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;
		
		$stavka_id=Input::get('stavka_id');
		$stavka = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->first();
		
		DB::table('web_b2c_korpa_stavka')->whereIn('web_b2c_korpa_stavka_id',array_map('current',DB::table('web_b2c_korpa_stavka')->select('web_b2c_korpa_stavka_id')->where('parent_vezani_roba_id',$stavka->roba_id)->get()))->delete();
		DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->delete();
		$articleDetails = Product::gtArticleDetails($stavka->roba_id,$stavka->kolicina);
        if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1 && Cart::kupac_id() > 0){
            $drip = new Drip();
            $dbcart = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',$stavka->web_b2c_korpa_id)->first();
            $drip->addOrUpdateCart($dbcart,"updated");
        }	
        echo json_encode(array('article_details' => $articleDetails));
	
	}
	public function cart_delete(){
		$lang = Language::multi() ? Request::segment(1) : null;
		$articlesDetails = [];
		foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->get() as $stavka){
			$articlesDetails[] = Product::gtArticleDetails($stavka->roba_id,$stavka->kolicina);
		}
		DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->delete();

        if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1 && Cart::kupac_id() > 0){
            $drip = new Drip();
            $dbcart = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',Cart::korpa_id())->first();
            $drip->addOrUpdateCart($dbcart,"updated");
        }	
        echo json_encode(array('articles_details' => $articlesDetails ));		
	}


}