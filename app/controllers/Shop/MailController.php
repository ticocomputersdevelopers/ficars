<?php 
use Service\Mailer;


class MailController extends Controller {

	public function meil_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

    	if(Options::gnrl_options(3003)){   
    	 		
	    	$data = array(
	    		'name' => Input::get('name'), 
	    		'email' => Input::get('email'),
	    		'msg' => Input::get('message'),
	    		'url' => Options::base_url()
	    	);

	    	Mail::send('shop.email.contact', $data, function($message){
	    	    $message->from(Input::get('email'), Input::get('name'));

	    	    $message->to(Options::company_email())->subject(EmailOption::subject());
	    	});
    	}else{
 		
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('name')." <br />
	        <b>Email:</b> ".Input::get('email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('message')."</p>";
	        $subject="Poruka sa ".parse_url(Options::domain(), PHP_URL_HOST);
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);
    	}

    } 

	public function contact_message_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'contact-name' => 'required|regex:'.Support::regex().'|max:200',
            'contact-email' => 'required|email',
            'contact-model-vozila' => 'required|regex:'.Support::regex().'|max:200',
            'contact-godiste-vozila' => 'required|digits_between:4,4',
            'contact-kubikaza-vozila' => 'required|digits_between:3,4',
            'contact-snaga-vozila' => 'required|digits_between:2,3',
            'contact-broj-sasije' => 'regex:'.Support::regex().'|max:200',
            'contact-message' => 'required|max:1000'
            );
        $translate_mess = Language::validator_messages();
        $translate_mess['digits_between'] = "Unesite od :min do :max cifre!";

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else {

	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />

	        <b>Pošiljalac:</b> ".Input::get('contact-name')." <br />
	        <b>Email:</b> ".Input::get('contact-email')."<br />
	        <b>Model vozila:</b> ".Input::get('contact-model-vozila')."<br />
	        <b>Godište vozila:</b> ".Input::get('contact-godiste-vozila')."<br />
	        <b>Kubikaža vozila:</b> ".Input::get('contact-kubikaza-vozila')." cm&sup3 <br />
	        <b>Snaga vozila:</b> ".Input::get('contact-snaga-vozila')." kW <br />
	        <b>Broj šasije vozila:</b> ".Input::get('contact-broj-sasije')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('contact-message')."</p>";
	        $subject="Poruka sa ".Options::server();
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);

	        return Redirect::back()->with('message',Language::trans('Vaša poruka je poslata').'!');
        }
	}

}