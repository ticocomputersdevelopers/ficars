<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Kupidobro {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			
			self::autoDownload(Support::autoLink($dobavljac_id),'files/kupidobro/kupidobro_xml/kupidobro.xml');
			$products_file = "files/kupidobro/kupidobro_xml/kupidobro.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->id;
				$naziv = (string) $product->title;
				$opis = (string) $product->description;
				$slika = (string) $product->image_link;
				$cena_nc = (float) $product->price;

				$kategorije = $product->categories->xpath('category');
				$grupa = (string) $kategorije[sizeof($kategorije) - 2];
				$podgrupa = (string) $kategorije[sizeof($kategorije) - 1];
				if (empty($grupa)) {
					$grupa = $podgrupa;
					unset($podgrupa);
				}

				if((string) $product->availability == "in stock") {
					$kolicina = 1;
				} else {
					$kolicina = 0;
				}
				$sku = (string) $product->squ;
				
				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
				if(isset($podgrupa) && !empty($podgrupa)) {
					$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
				}
				if(isset($opis) && !empty($opis)) {
					$sPolja .= " opis,";				$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($opis))) . "',";
					$sPolja .= " flag_opis_postoji,";	$sVrednosti .= "1,";
				}
				if(isset($sku) && !empty($sku) && strlen($sku) <= 10) {
				$sPolja .= " sku,";						$sVrednosti .= " '" . Support::encodeTo1250($sku) . "',";	
				}
				if(isset($slika) && !empty($slika)) {
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".Support::encodeTo1250($sifra)."','".Support::encodeTo1250($slika)."',1 )");
					$sPolja .= " flag_slika_postoji,";	$sVrednosti .= "1,";
				}
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($cena_nc,1,1,2),2, '.', '') . ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			self::autoDownload(Support::autoLink($dobavljac_id),'files/kupidobro/kupidobro_xml/kupidobro.xml');
			$products_file = "files/kupidobro/kupidobro_xml/kupidobro.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$sifra = (string) $product->id;
				$cena_nc = (float) $product->price;
				if((string) $product->availability == "in stock") {
					$kolicina = 1;
				} else {
					$kolicina = 0;
				}

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";	
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($cena_nc,1,1,2),2, '.', '') . ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function autoDownload($link,$file) {
		$arrContextOptions=array(
    		"ssl"=>array(
        	"verify_peer"=>false,
        	"verify_peer_name"=>false,
    		),
		); 
		file_put_contents($file, file_get_contents($link,false,stream_context_create($arrContextOptions)));
	}
	


}