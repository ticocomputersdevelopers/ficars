 <?php 
 use Service\Mailer;

 class AdminFiskalizacija {
    // public static function vcsdServiceRequest($requestPayload, $requestAction) {
    //         require_once('vendor/autoload.php');

    //         $client = new \GuzzleHttp\Client();

    //         $response = $client->request('POST', $requestAction, [
    //           'body' => $requestPayload,
    //           'headers' => [
    //             'Accept' => 'application/json',
    //             'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlLZXlOYW1lIjoiQXBpIEtsanVjIiwidWlkIjoiYnFORnlHa2ppcFJOYVFPRHcyY0xwcmh3b2MyMyIsInRpbWVzdGFtcENyZWF0ZWQiOjE2NTEyNDcwNjcwMzQsInRpbWVzdGFtcEV4cGlyZXMiOjE2NTEyNDcwNjcwMzQsImlhdCI6MTY1MTI0NzA2N30.nyNxubaX7zifaAI4MauzQLhSFBWcsO1zg8sDJ9uwiw0',
    //             'Content-Type' => 'application/json',
    //           ],
    //         ]);

    //         $return_json = $response->getBody();
    //         return $return_json;
    //     }


    public static function vcsdServiceRequest($requestPayload) {
                
                if(Config::get('app.livemode')) {
                    $requestAction = 'https://vsdc.suf.purs.gov.rs/api/v3/invoices';
                } else {
                    $requestAction = 'https://vsdc.sandbox.suf.purs.gov.rs/api/v3/invoices';
                }


                try {
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $requestAction);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $requestPayload);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'PAC: U9C98U']);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/files/fiscalization/3_7QXA_Т.Р.ФИЋА_ОБЈЕКАТ_БР.1.nochain.CERiKEY.pem');
                    curl_setopt($ch, CURLOPT_SSLCERTPASSWD, 'FICA6109');
                    curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');

                    $response = curl_exec($ch);


                    if (curl_errno($ch)) {
                        throw new \Exception(curl_error($ch));
                    }
                    if (is_null(json_decode($response))) {
                        throw new \Exception('Bad request');
                    }
                } catch (\Exception $e) {
                    throw $e;
                } finally {
                    curl_close($ch);
                }

                return $response;
    }

        public static function vcsdServiceGetTaxes() {

                if(Config::get('app.livemode')) {
                    $requestAction = 'https://vsdc.suf.purs.gov.rs/api/v3/status';
                } else {
                    $requestAction = 'https://vsdc.sandbox.suf.purs.gov.rs/api/v3/status';
                }


                try {
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $requestAction);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'PAC: U9C98U']);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/files/fiscalization/3_7QXA_Т.Р.ФИЋА_ОБЈЕКАТ_БР.1.nochain.CERiKEY.pem');
                    curl_setopt($ch, CURLOPT_SSLCERTPASSWD, 'FICA6109');
                    curl_setopt($ch, CURLOPT_SSLCERTTYPE, 'PEM');

                    $response = curl_exec($ch);

                    if (curl_errno($ch)) {
                        return array();
                        // throw new \Exception(curl_error($ch));
                    }
                    if (is_null(json_decode($response))) {
                        return array();
                        // throw new \Exception('Bad request');
                    }
                } catch (\Exception $e) {
                    throw $e;
                } finally {
                    curl_close($ch);
                }

                return json_decode($response)->currentTaxRates->taxCategories;
    }

    public static function cashier() {
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();
        return $user->ime.' '.$user->prezime;
    }

    public static function replace_special_characters($string) {
        $spec_string = array('\u010d','\u0161','\u0107','\u0111','\u017e','\u010c','\u0160','\u0106','\u0110','\u017d');
        $replace_string = array('č','š','ć','đ','ž','Č','Š','Ć','Đ','Ž');

        return str_replace($spec_string,$replace_string,$string);
    }

    public static function getKupacID($web_b2c_narudzbina_id) {
        $kupac_id =  DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
        return $kupac_id;
    }

    public static function mappedVrstaRacuna($faktura=null,$vrsta=null,$pismo='latinica') {

        if($pismo == 'latinica') {     
            $tip_fakture = array(
                                    'Normal' => 'Promet',
                                    'ProForma' => 'Predračun',
                                    'Copy' => 'Kopija',
                                    'Training' => 'Obuka',
                                    'Advance' => 'Avans'
                                );
            $vrsta_fakture = array(
                            'Sale' => 'Prodaja',
                            'Refund' => 'Refundacija'
                        );
        } else {
                    $tip_fakture = array(
                                    'Normal' => 'Промет',
                                    'ProForma' => 'Предрачун',
                                    'Copy' => 'Копија',
                                    'Training' => 'Обука',
                                    'Advance' => 'Аванс'
                                );
            $vrsta_fakture = array(
                            'Sale' => 'Продаја',
                            'Refund' => 'Рефундација'
                        );
        }
        if(!(is_null($faktura) && is_null($vrsta))) {
            return $tip_fakture[$faktura].' '.$vrsta_fakture[$vrsta];
        } elseif (!is_null($faktura)) {
            return $tip_fakture[$faktura];
        } elseif (!is_null($vrsta)) {
            return $vrsta_fakture[$vrsta];
        } else {
            return null;
        }
    }

    public static function mappedPlacanje($placanje,$pismo='latinica') {
        if($pismo == 'latinica') {     
            $placanje_mapped = array(
                                'Other' => 'Drugo',
                                'Cash' => 'Gotovina',
                                'Card' => 'Platna kartica',
                                'Check' => 'Ček',
                                'WireTransfer' => 'Prenos na račun',
                                'Voucher' => 'Vaučer',
                                'MobileMoney' => 'Instant plaćanje'
                            );
        } else {
                    $placanje_mapped = array(
                                'Other' => 'Друго',
                                'Cash' => 'Готовина',
                                'Card' => 'Платна картица',
                                'Check' => 'Чек',
                                'WireTransfer' => 'Пренос на рачун',
                                'Voucher' => 'Ваучер',
                                'MobileMoney' => 'Инстант плаћање'
                            );
        }

        return $placanje_mapped[$placanje];
    }

    public static function cirilicToLatin($string,$smer=0) {
        $cirilica = array('a','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш');

        $latinica = array('a','b','v','g','d','đ','e','ž','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','ć','u','f','h','c','č','dž','š','A','B','V','G','D','Đ','E','Ž','Z','I','J','K','L','Lj','M','N','Nj','O','P','R','S','T','Ć','U','F','H','C','Č','Dž','Š');
        if($smer == 0) {
            return str_replace($cirilica,$latinica,$string);
        } else {
            return str_replace($latinica,$cirilica,$string);
        }
    }

    public static function orderCheckFiskalization($web_b2c_narudzbina_id,$exception= array()) {
        if(is_array($exception) && !empty($exception)) {
            $check = !DB::table('racun')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->whereNotIn('tip_racuna',$exception)->whereNotNull('potpisao')->first();
        } else {
            $check = DB::table('racun')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->whereNotNull('potpisao')->first();
        }
        if($check) {
            return true;
        } else {
            return false;
        }
    }

    public static function getKupacMail($web_b2c_narudzbina_id) {
        $kupac_id = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
        return DB::table('web_kupac')->where('web_kupac_id',$kupac_id)->pluck('email');
    }

    public static function mailFiscalNotificationToClient($racun_id,$email) {
        
        $racun = DB::table('racun')->where('racun_id',$racun_id)->first();
        $subject = 'Informacije o računu: '.$racun->broj_dokumenta;
        $body = 'Poštovani, u prilogu je Vaš račun za naručenu robu.<br>
        Broj računa:'.$racun->broj_dokumenta.'<br><br>
        Veza za verifikaciju: <a href="'.$racun->verifikacija_url.'">'.$racun->verifikacija_url.'</a><br><br>';
        $filePath = 'files/'.$racun->broj_dokumenta.'.pdf';
        $customPaper = array(0,0,233, 1000); 
        $pdf = App::make('dompdf');
        $pdf->loadView('admin.pdf_fiskal_isecak',[
        'racun' => $racun,
        'text' => $racun->tekstualni_prikaz,
        'qr_kod_gif' => 'data:image/gif;base64,'.$racun->qr_kod,
        'change' => number_format(array_map('current',DB::select("SELECT sum(iznos) from racun_vrsta_placanja where racun_id = ".$racun_id." "))[0] - $racun->iznos,2, ',', '.')
        ])->setPaper($customPaper)->save($filePath);
        Mailer::send(Options::company_email(),$email,$subject,$body,$filePath);
        if (!is_null($filePath)) {
            if(File::exists($filePath)){
                File::delete($filePath);
            }   
        }
    }

    public static function checkStorned($web_b2c_narudzbina_id) {
        return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('stornirano');
    }

    public static function getPlacanjeNaziv($racun_vrsta_placanja_naziv_id) {
        return DB::table('racun_vrsta_placanja_naziv')->where('racun_vrsta_placanja_naziv_id',$racun_vrsta_placanja_naziv_id)->pluck('naziv');
    }

    public static function getMappedPlacanjeID($placanje,$smer = 0) {
        $placanja = array(
                            '0' => 'Other',
                            '1' => 'Cash',
                            '2' => 'Card',
                            '3' => 'Check',
                            '4' => 'WireTransfer',
                            '5' => 'Voucher',
                            '6' => 'MobileMoney'
                        );
        if($smer == 0) {
            return $placanja[$placanje];
        } else {
            return array_flip($placanja)[$placanje];
        }
    }

    public static function checkRefund($racun_id) {
        $refund = DB::table('racun')->where(array('racun_id' => $racun_id,'vrsta_transakcije' => 'Refund'))->first();

        if($refund) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUnit($roba_id) {
        $jedinica_mere_id = DB::table('roba')->where('roba_id',$roba_id)->pluck('jedinica_mere_id');
        if($jedinica_mere_id >= 0) {
            return DB::table('jedinica_mere')->where('jedinica_mere_id',$jedinica_mere_id)->pluck('naziv');
        } else {
            return '';
        }
    }

    public static function getTexValue($roba_id) {

        if(DB::table('roba')->where('roba_id',$roba_id)->first()) {    
            $tarifna_grupa_id = DB::table('roba')->where('roba_id',$roba_id)->pluck('tarifna_grupa_id');
            return DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->pluck('porez');
        } else {
            return 1;
        }
    }

    public static function getBuyerPib($web_b2c_narudzbina_id) {
        $web_kupac_id = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();
        if($web_kupac && $web_kupac->flag_vrsta_kupca == 1 && !empty($web_kupac->pib) && strlen($web_kupac->pib) == 9) {
            return '10:'.$web_kupac->pib;
        }
        
        return '';
    }

    public static function checkAdvanceInput($key,$value) {
        $array = array(
                        '10: Аванс' => 'Ђ',
                        '11: Аванс' => 'Е',
                        '12: Аванс' => 'Г',
                        '13: Аванс' => 'А'
                    );
        if(isset($array[$key]) && $array[$key] == $value) {
            return false;
        } elseif(!in_array($value, array('Ђ','Е','Г','А'))) {
            return false;
        } else {
            return true;
        }
    }

    public static function checkBuyerId($buyerId) {
        $buyerIdArray = explode(":",$buyerId);
        
        if(sizeof($buyerIdArray) == 1 || sizeof($buyerIdArray) > 3) {
            return false;
        } elseif( strlen($buyerIdArray[0]) != 2 || !in_array($buyerIdArray[0],array(10,11,12,20,21,22,23,30,31,32,33,34,35,40)) || !is_numeric($buyerIdArray[0])) {
            return false;
        } elseif($buyerIdArray[0] == 10 && (sizeof($buyerIdArray) != 2 || !is_numeric($buyerIdArray[1]) || strlen($buyerIdArray[1]) != 9 )) {
            return false;
        } elseif($buyerIdArray[0] == 11 &&  (sizeof($buyerIdArray) != 2 || !is_numeric($buyerIdArray[1]) || strlen($buyerIdArray[1]) != 13)) {
            return false;
        } elseif ($buyerIdArray[0] == 12 &&  (sizeof($buyerIdArray) != 3 || !is_numeric($buyerIdArray[1]) || strlen($buyerIdArray[1]) != 9 || !is_numeric($buyerIdArray[2]) || strlen($buyerIdArray[2]) != 5)) {
            return false;
        } elseif($buyerIdArray[0] == 20 && (sizeof($buyerIdArray) != 2 || !is_numeric($buyerIdArray[1]) || strlen($buyerIdArray[1]) != 9 )){
            return false;
        }
        else{
            return true;
        }
    }

    public static function checkBuyerCenterId($buyerCenterId) {
        $buyerCenterIdArray = explode(":",$buyerCenterId);

        if(sizeof($buyerCenterIdArray) != 2) {
            return false;
        } elseif(strlen($buyerCenterIdArray[0]) != 2 || !is_numeric($buyerCenterIdArray[0]) || !is_numeric($buyerCenterIdArray[1]) || !in_array($buyerCenterIdArray[0],array(10,11,20,21,30,31,32,33,40))) {
            return false;
        } else {
            return true;
        }
    }

    public static function allBuyersAnalitics() {
        $narudzbina_status_id = DB::table('narudzbina_status')->where('naziv','Fiskalizovano')->first()->narudzbina_status_id;

        $web_kupac_ids = array_map('current',DB::select("SELECT distinct web_kupac_id FROM web_b2c_narudzbina where web_b2c_narudzbina_id in (SELECT web_b2c_narudzbina_id from narudzbina_status_narudzbina where narudzbina_status_id = ".$narudzbina_status_id.") "));
        return DB::table('web_kupac')->select('web_kupac_id','ime','prezime','naziv','pib','flag_vrsta_kupca')->whereIn('web_kupac_id',$web_kupac_ids)->get();
    }

    public static function getAnaliticsPaymentAmount($racun_ids,$date) { 
      if(is_null($date)) {
        $rows =  DB::select("SELECT sum(iznos) as iznos, racun_vrsta_placanja_naziv_id from racun_vrsta_placanja rvp where racun_id in ('".implode("','",$racun_ids)."') group by rvp.racun_vrsta_placanja_naziv_id");
      } else {

      $rows =  DB::select("SELECT sum(iznos) as iznos,racun_vrsta_placanja_naziv_id from racun_vrsta_placanja rvp where racun_id in (select racun_id from racun where CAST(datum_racuna as DATE) = '".$date."') and racun_id in ('".implode("','",$racun_ids)."') group by rvp.racun_vrsta_placanja_naziv_id");
      }
      
      $array = array();
      $ukupno = 0;
      foreach ($rows as $row) {
          $array[DB::table('racun_vrsta_placanja_naziv')->where('racun_vrsta_placanja_naziv_id',$row->racun_vrsta_placanja_naziv_id)->pluck('racun_vrednost')] = $row->iznos;
          $ukupno = $ukupno + $row->iznos;
      }
      $array['total'] = $ukupno;
      return (object) $array;
    }

    public static function getAnaliticsTaxAmount($racun_ids,$date) {
        if(is_null($date)) {
            return (float) array_map('current',DB::select("SELECT sum(iznos_poreza) as iznos_poreza from racun_porez where  racun_id in ('".implode("','",$racun_ids)."')"))[0];
        } else {   
            return (float) array_map('current',DB::select("SELECT sum(iznos_poreza) as iznos_poreza from racun_porez where racun_id in (select racun_id from racun where CAST(datum_racuna as DATE) = '".$date."') and racun_id in ('".implode("','",$racun_ids)."')"))[0];
        }
    }

    public static function array_map_assoc(callable $f, array $a) {
        return array_column(array_map($f, array_keys($a), $a), 1, 0);
    }

 }