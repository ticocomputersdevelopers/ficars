<?php
namespace IsXls;
use DB;

class Osobine {

	public static function table_body($articles){

		$result_kombinacija_arr = array();
		$result_vrednost_arr = array();
		$vrednosti = array();
		$vrednost_rbr=1;
		foreach($articles as $article) {
			if(!empty($article->velicina) AND isset($article->velicina)){
				
	    		$roba = DB::table('roba')->where('sifra_is',$article->code)->first();

				$roba_id = $roba->roba_id;
				$cena = $article->web_price;	
				$kolicina = intval($article->quantity);
				if($kolicina < 0){
					$kolicina = 0;
				}
				
				$sifra = $article->code.'+'.$article->velicina;

				$result_kombinacija_arr[] = "(".strval($roba_id).",".strval($kolicina).",".strval($cena).",1,'".$sifra."' )";

				if(!in_array($article->velicina, $vrednosti)){
					$vrednosti[] = $article->velicina;
					$vrednost = strpos($article->velicina,'-') == (strlen($article->velicina)-1) ? str_replace('-','.5',$article->velicina) : $article->velicina;
					$result_vrednost_arr[] = "( 1, '".$vrednost."', 1, ".$vrednost_rbr.")";
					$vrednost_rbr++;
				}
			}
		}

		return (object) array("osobina_kombinacija"=>implode(",",$result_kombinacija_arr), "osobina_vrednost"=>implode(",",$result_vrednost_arr));
	}

	public static function query_insert_update($table_temp) {

		$osobina_kombinacija_temp = "(VALUES ".$table_temp->osobina_kombinacija.") osobina_kombinacija_temp(roba_id,kolicina,cena,aktivna,sifra)";
		
		DB::statement("UPDATE osobina_kombinacija ok SET kolicina = osobina_kombinacija_temp.kolicina FROM ".$osobina_kombinacija_temp." WHERE ok.sifra=osobina_kombinacija_temp.sifra ");
		
		//insert
		DB::statement("INSERT INTO osobina_kombinacija ( roba_id,kolicina,cena,aktivna,sifra ) SELECT * FROM ".$osobina_kombinacija_temp." WHERE sifra NOT IN ( SELECT sifra FROM osobina_kombinacija WHERE sifra IS NOT NULL )");
		DB::statement("DELETE FROM osobina_kombinacija WHERE sifra NOT IN (SELECT sifra FROM ".$osobina_kombinacija_temp." ) ");

		// lager-update
		DB::statement("UPDATE lager l SET kolicina = okt.kolicina FROM (SELECT SUM(kolicina) as kolicina, roba_id FROM ".$osobina_kombinacija_temp." GROUP BY roba_id) okt WHERE l.roba_id = okt.roba_id ");
		DB::statement("INSERT INTO lager (orgj_id, roba_id, kolicina,poslovna_godina_id) SELECT 1,roba_id,kolicina,2014 FROM (SELECT SUM(kolicina) as kolicina, roba_id FROM ".$osobina_kombinacija_temp." GROUP BY roba_id) okt WHERE okt.roba_id NOT IN ( SELECT roba_id FROM lager ) ");
		

		//insert vrednost
		$osobina_vrednost_temp = "(VALUES ".$table_temp->osobina_vrednost.") osobina_vrednost_temp(osobina_naziv_id, vrednost, aktivna, rbr)";

		DB::statement("INSERT INTO osobina_vrednost (osobina_naziv_id, vrednost, aktivna, rbr) SELECT * FROM ".$osobina_vrednost_temp." WHERE (osobina_naziv_id, vrednost) NOT IN ( SELECT osobina_naziv_id, vrednost FROM osobina_vrednost)");
		DB::statement("DELETE FROM osobina_vrednost WHERE (osobina_naziv_id, vrednost) NOT IN (SELECT osobina_naziv_id, vrednost FROM ".$osobina_vrednost_temp." ) ");



	}
	
	public static function update_osobina_kombinacija_vrednost($articles){

		$result_kombinacija_arr = array();
		
		foreach($articles as $article) {
			if(!empty($article->velicina) AND isset($article->velicina)){
				
	    		$osobina_kombinacija_id = DB::table('osobina_kombinacija')->where('sifra',$article->code.'+'.$article->velicina)->pluck('osobina_kombinacija_id');

	    		$vrednost = strpos($article->velicina,'-') == (strlen($article->velicina)-1) ? str_replace('-','.5',$article->velicina) : $article->velicina;
	    		$osobina_vrednost_id = DB::table('osobina_vrednost')->where(array('osobina_naziv_id'=>1, 'vrednost'=>$vrednost))->pluck('osobina_vrednost_id');

				$result_kombinacija_arr[] = "(".strval($osobina_kombinacija_id).",".strval($osobina_vrednost_id)." )";

			}
		}	
		$osobina_kombinacija_temp = "(VALUES ".implode(',',$result_kombinacija_arr).") osobina_kombinacija_vrednost_temp(osobina_kombinacija_id,osobina_vrednost_id)";

		//insert
		DB::statement("INSERT INTO osobina_kombinacija_vrednost ( osobina_kombinacija_id,osobina_vrednost_id) SELECT * FROM ".$osobina_kombinacija_temp." WHERE (osobina_kombinacija_id,osobina_vrednost_id) NOT IN ( SELECT osobina_kombinacija_id,osobina_vrednost_id FROM osobina_kombinacija_vrednost)");
		DB::statement("DELETE FROM osobina_kombinacija_vrednost WHERE (osobina_kombinacija_id,osobina_vrednost_id) NOT IN (SELECT osobina_kombinacija_id,osobina_vrednost_id FROM ".$osobina_kombinacija_temp." ) ");	
	}
}