<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8"> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title>PDF</title>
        <style>
          *  {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }
       </style>

       <?php 
        $text_prikaz = explode("\r\n",$text);
          $text_prikaz_drugi_deo = array($text_prikaz[sizeof($text_prikaz)-2],$text_prikaz[sizeof($text_prikaz)-1]);
          unset($text_prikaz[sizeof($text_prikaz)-2]); unset($text_prikaz[sizeof($text_prikaz)-1]);
          $text_prikaz_prvi_deo = implode("\r\n",$text_prikaz);
          if($change >= 0 || $racun->tip_racuna == 'ProForma') {
          if($change < 0) {
            $change = '0,00';
          } 
          $text_prikaz = explode("========================================",$text_prikaz_prvi_deo);
          $text_prikaz[1] = $text_prikaz[1]."Повраћај:".str_repeat(" ",48-strlen("Повраћај:")-strlen($change)).$change."\r\n";
          $text_prikaz_prvi_deo = implode("========================================",$text_prikaz); 
          }

          if($racun->tip_racuna == 'Normal' && $racun->vrsta_transakcije = 'Sale' && !empty($racun->referentni_broj_dokumenta) && DB::table('racun')->where(array('broj_dokumenta' => $racun->referentni_broj_dokumenta,'tip_racuna' => 'Advance','vrsta_transakcije' => 'Refund'))->first()) {
            $text_prikaz = explode("----------------------------------------",$text_prikaz_prvi_deo);
            $text_prikaz_parrent = explode("\r\n",$text_prikaz[1]);
            $placeno_avansom = number_format(DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('iznos'),2, ',', '.');
            $referentni_racun_id = DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('racun_id');
            $pdv_na_avans = number_format(array_map('current',DB::select("SELECT sum (ROUND(iznos_poreza, 2)) from racun_porez where racun_id = ".$referentni_racun_id.""))[0],2, ',', '.');
            

            $amount_avans_change = $racun->iznos - DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('iznos') - array_map('current',DB::select("SELECT sum(iznos) from racun_vrsta_placanja where racun_id = ".$racun->racun_id.""))[0];
            
            if($amount_avans_change  > 0) {
              $text_avans_change = 'Преостало за плаћање:';
            } else {
              $text_avans_change = 'Повраћај:';
            }

            $amount_avans_change = number_format(abs($amount_avans_change),2, ',', '.');

            $text_prikaz_parrent[1] = $text_prikaz_parrent[1]."\r\nПлаћено авансом:".str_repeat(" ",54-strlen('Плаћено авансом:')-strlen($placeno_avansom)).$placeno_avansom."\r\nПДВ на аванс:".str_repeat(" ",50-strlen('ПДВ на аванс:')-strlen($pdv_na_avans)).$pdv_na_avans;
            $text_prikaz[1] = implode("\r\n",$text_prikaz_parrent);

            $text_prikaz_parrent = explode("========================================",$text_prikaz[1]);
            if($text_avans_change != 'Повраћај:') {
              $text_prikaz_parrent[0] = $text_prikaz_parrent[0].$text_avans_change.str_repeat(" ",58-strlen($text_avans_change)-strlen($amount_avans_change)).$amount_avans_change."\r\n";
            } else {
              $text_prikaz_parrent[0] = $text_prikaz_parrent[0].$text_avans_change.str_repeat(" ",48-strlen($text_avans_change)-strlen($amount_avans_change)).$amount_avans_change."\r\n";
            }
            $text_prikaz[1] = implode("========================================",$text_prikaz_parrent);

            
            $text_prikaz_prvi_deo = implode("----------------------------------------",$text_prikaz);


            // var_dump($text_prikaz_parrent); die;
          }
            // die;
            if(!in_array($racun->tip_racuna,array('Advance','Normal')) ) {
              $text_prikaz_prvi_deo = explode("========================================",$text_prikaz_prvi_deo);
              $text_prikaz_prvi_deo[2] = "<div style='font-size:18px;'>ОВО НИЈЕ ФИСКАЛНИ РАЧУН</div>"; 
              $text_prikaz_prvi_deo = implode("========================================",$text_prikaz_prvi_deo);

            }

          $text_prikaz_drugi_deo = implode("\r\n",$text_prikaz_drugi_deo);
       ?>

   </head><body style="margin:0; padding: 0;line-height: 1; margin-top: 15px;"> 

        <div style="padding: 0; box-sizing: border-box; text-align: center;">  
            <pre style="  
            font-family: 'DejaVu Sans Mono', monospace; 
            display: block;
            padding: 9.5px;
            margin: 0 0 10px;
            font-size: 12px;
            line-height: 1;
            word-break: break-all; 
            word-wrap: break-word;">{{ $text_prikaz_prvi_deo }}</pre>

            <img style="max-width: 250px; " src="{{ $qr_kod_gif }}">

            @if($racun->tip_racuna == 'Copy' && $racun->vrsta_transakcije == 'Refund')
              <br><br>
              <pre style="  
            font-family: 'DejaVu Sans Mono', monospace; 
            display: block;
            padding: 9.5px;
            margin: 0 0 10px;
            font-size: 12px;
            line-height: 1;
            word-break: break-all; 
            word-wrap: break-word;height: margin-bottom 2px;">Потпис купца: _ _ _ _ _ _ _ _ _ _ </pre>
            @endif

            <pre style="  
            font-family: 'DejaVu Sans Mono', monospace; 
            display: block;
            padding: 9.5px;
            margin: 0 0 10px;
            font-size: 12px;
            line-height: 1;
            word-break: break-all; 
            word-wrap: break-word;">{{ $text_prikaz_drugi_deo }}</pre>

            @if($racun->tip_racuna == 'Normal' && $racun->vrsta_transakcije == 'Sale' && !empty($racun->referentni_broj_dokumenta) && DB::table('racun')->where(array('broj_dokumenta' => $racun->referentni_broj_dokumenta,'tip_racuna' => 'Advance','vrsta_transakcije' => 'Refund'))->first())

            <?php
              $number_advance= DB::table('racun')->where('broj_dokumenta',$racun->referentni_broj_dokumenta)->pluck('referentni_broj_dokumenta');
              
             $time_advance = DB::table('racun')->where(array('broj_dokumenta' => $number_advance))->pluck('datum_racuna'); ?>
             <pre style="  
            font-family: 'DejaVu Sans Mono', monospace; 
            display: block;
            /*padding: 9.5px;*/
            /*margin: 0 0 10px;*/
            font-size: 12px;
            line-height: 1;
            word-break: break-all; 
            word-wrap: break-word;
            white-space: wrap;
            width: 100%;
            ">*Последњи авансни рачун: <br> {{ $number_advance }} {{ date('d.m.Y.',strtotime($time_advance)) }}</pre>
            @endif



            @if($racun->tip_racuna == 'Advance' && $racun->vrsta_transakcije == 'Sale' && !empty(json_decode($racun->reklama)))
             <pre style="  
            font-family: 'DejaVu Sans Mono', monospace; 
            display: block;
            padding: 9.5px;
            margin: 0 0 10px;
            font-size: 12px;
            line-height: 1;
            word-break: break-all; 
            word-wrap: break-word;
            white-space: wrap;
            width: 100%;
            ">

           @foreach(json_decode($racun->reklama) as $reklama)
            <div style="margin-bottom: 5px"> 
              <div style="text-align: left; display: inline-block; width:60%;">{{ $reklama->naziv }}</div> 
              <div style="display: inline-block; width:36%; text-align: right;">{{ $reklama->cena }}</div>
            </div>
            @endforeach

            </pre>
            @endif

        </div>

   </body></html>