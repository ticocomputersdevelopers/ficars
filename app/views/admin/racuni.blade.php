



<section id="main-content">


		<div class="row flex"> 
			<div class="column medium-4"> 
				<h1 class="inline-block no-margin vert-align">{{ AdminLanguage::transAdmin('Računi') }}</h1>
			</div>

			<div class="orders-input column medium-4"> 
				<input type="text" class="search-field" id="search" autocomplete="on" placeholder="{{ AdminLanguage::transAdmin('Pretraži račune') }}...">
				<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</div>

			<div class="column medium-4 text-center">
				@if(AdminOptions::gnrl_options(3065))
				  <button class="text-center relative inline-block btn btn-primary" id="openOrderModal">Fiskalizacija analitika</button>
				@endif
			</div>
		</div>


		<div class="o-filters row"> 
			<div class="column medium-4">
				<label class="no-margin">{{ AdminLanguage::transAdmin('Tip računa') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSTipRacuna" data-tip="Normal" {{  in_array('Normal',$tipovi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Promet') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSTipRacuna" data-tip="ProForma" {{ in_array('ProForma',$tipovi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Predračun') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSTipRacuna" data-tip="Training" {{ in_array('Training',$tipovi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Obuka') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSTipRacuna" data-tip="Advance" {{ in_array('Advance',$tipovi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Avans') }}</span>
					</label>
				</div>
			&nbsp;
			<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSTipRacuna" data-tip="Copy" {{ in_array('Copy',$tipovi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Kopija') }}</span>
					</label>
				</div>
			</div>


			<div class="column medium-4"> 
				<label class="no-margin">{{ AdminLanguage::transAdmin('Vrsta računa') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSVrstaRacuna" data-vrsta="Sale" {{ in_array('Sale',$vrste) ? 'checked' : '' }}>
						<span class="label-text prodaja">{{ AdminLanguage::transAdmin('Prodaja') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSVrstaRacuna" data-vrsta="Refund" {{ in_array('Refund',$vrste) ? 'checked' : '' }}>
						<span class="label-text refundacija">{{ AdminLanguage::transAdmin('Refundacija') }}</span>
					</label>
				</div>
			</div>
			<a href="{{ AdminOptions::base_url() }}admin/racuni/0/0/0/0/0" class="m-input-and-button__btn btn btn-danger erase-btn no-margin tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi filtere') }}">
				<span class="fa fa-times" aria-hidden="true"></span>
			</a>
		</div>

		<div class="row o-filters-span-2">

			<div class="columns medium-5">
				<span>{{ AdminLanguage::transAdmin('Ukupno računa') }}: <b>{{$count}}</b></span>
			</div>

			<div class="columns medium-7">

				<span class="datepicker-col">
					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Od') }}:</label>
						<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
					</div>

					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Do') }}:</label>
						<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
					</div>
				</span>
			</div>
		</div>


		<!--====  End of Filters  ====-->
		
		<div class="table-scroll JSorders-listing" style="max-height: none;">
			<table class="order-table">
				<thead>
					<tr>
						<td>{{ AdminLanguage::transAdmin('ID') }}</td>
						<th>{{ AdminLanguage::transAdmin('Zatražio-potpisao-brojač') }}</th>
						<th>{{ AdminLanguage::transAdmin('Brojač računa') }}</th>
						<th>{{ AdminLanguage::transAdmin('Pfr vreme (Vremenska zona servera)') }}</th>
						<th>{{ AdminLanguage::transAdmin('Vrsta računa') }}</th>
						<th>{{ AdminLanguage::transAdmin('Ukupan iznos') }}</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					@foreach($query as $row)
					<tr class="JSMoreWiew {{ $row->vrsta_transakcije == 'Sale' ? 'prodaja' : 'refundacija' }}">
						<td>{{ $row->racun_id }}</td>
						<td>{{ $row->broj_dokumenta }}</td>
						<td>{{ $row->brojac_racuna }}</td>
						<td>{{ date('d.m.Y.  H:i:s' ,strtotime($row->datum_racuna)) }}</td>
						<td>{{ AdminFiskalizacija::mappedVrstaRacuna($row->tip_racuna,$row->vrsta_transakcije) }}</td>
						<td>{{ $row->iznos }}</td>
						<td><a class="btn btn-primary tooltipz" href="{{ AdminOptions::base_url().'admin/narudzbina-fiskalizacija-detalji/'.$row->web_b2c_narudzbina_id.'/'.$row->racun_id }}" target="_blank" aria-label="{{ AdminLanguage::transAdmin('Otvori račun u narudžbini') }}">{{ AdminLanguage::transAdmin('Detalji') }}</a></td>
						<td><a class="btn btn-primary" href="{{ AdminOptions::base_url().'admin/fiskalizacija_isecak/'.$row->racun_id }}" target="_blank">{{ AdminLanguage::transAdmin('Isečak') }}</a></td>
					</tr> 
					@endforeach
				</tbody>


			</table>
	
		



		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}

<!-- Modal za narudzbinu -->
<div class="modal-order">
	<form action="{{ AdminOptions::base_url() }}admin/fiskalizacija-analitika" method="post"  target="_blank"> 
		
	<div class="modal-main">
	<div class="modal-order-header">
		<span>Fiskalizacija analitika</span>
		<span id="closeOrderModal"><i class="fas fa-times"></i></span>						
	</div>
	<div class="modal-order-content">
		<div class="content-top">
			<!-- Box 1 -->
			<div class="box">
				<div class="content-top-title">
					Tip računa
				</div>				
				<div class="content-top-radio">
					<select name="invoiceType">		
						<option value="Normal" {{ Input::old('invoiceType') == 'Normal' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Promet') }}</option>
						<option value="ProForma" {{ Input::old('invoiceType') == 'ProForma' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Predračun') }}</option>
						<option value="Copy" {{ Input::old('invoiceType') == 'Copy' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Kopija') }}</option>
						<option value="Training" {{ Input::old('invoiceType') == 'Training' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Obuka') }}</option>
						<option value="Advance" {{ Input::old('invoiceType') == 'Advance' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Avans') }}</option>
					</select> 
				</div>	
			</div>

			<div class="box">
				<div class="content-top-title">
					Vrsta transakcije
				</div>				
				<div class="content-top-radio">
					<select name="transactionType" >
						<option value="Sale" {{ Input::old('transactionType') == 'Sale' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Prodaja') }}</option>
						<option value="Refund" {{ Input::old('transactionType') == 'Refund' ? 'selected' : ''}}>{{ AdminLanguage::transAdmin('Refundacija') }}</option>
					</select>
				</div>	
			</div>
		</div>

		<!-- Input Group -->
		<div class="input-groups">
			<div class="text-center">
			 Kupac - Ime i Prezime->Naziv->PIB
			<select name="buyerId" class="columns medium-12">
				<option value=""></option>
				@foreach(AdminFiskalizacija::allBuyersAnalitics() as $row)
				<option value="{{ $row->web_kupac_id }}">
					@if(!empty(trim($row->ime.' '.$row->prezime)))
						{{ $row->ime }} {{ $row->prezime }}
					@endif
					@if(!empty(trim($row->naziv))) 
						-> {{ $row->naziv }}
					@endif
					@if(!empty(trim($row->pib)))
						-> {{ $row->pib }}
					@endif
				</option>
				@endforeach
			</select>
			</div>

			<div class="input-bottom">
				<label for="date" class="text-center">Izaberite period:</label>

				<div class="date-groups">
					<div class="date">
						<span><span>Od:</span>  <input name="date_from" type="date" /> </span>
					</div>				
					<div class="date">
						<span><span>Do:</span>  <input name="date_to" type="date" /> </span>
					</div>				
				</div>
			</div>

		</div>
		<!-- End of Input Group -->

		<div class="modal-order-footer">
			<div class="gradient-border"></div>

			<div class="text-center">
				<a class="btn btn-secondary" id="closeOrderModalButton"><i class="fas fa-times"></i> Otkaži</a>
				<button class="btn btn-secondary"><i class="fas fa-print"></i> Štampaj</button>
			</div>
		</div>
	</div>

  </div>
</form>
</div>


<!-- MODAL END -->


</section>



