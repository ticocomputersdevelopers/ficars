<?php
if($datum_od==0 and $datum_do==0){
	$dat1='';
	$dat2='';
}else 
if($datum_od!=0 and $datum_do==0){
	$dat1=$datum_od;
	$dat2='';
}else 
if($datum_od==0 and $datum_do!=0){
	$dat1='';
	$dat2=$datum_do;
}else 
{
	$dat1=$datum_od;
	$dat2=$datum_do;
}
?>

<section id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@if(Session::has('city_pdf_pickupid'))
	<script>
		window.open("{{AdminOptions::base_url()}}admin/city-pdf/{{Session::get('city_pdf_pickupid')}}", '_blank');
	</script>
	@endif

	<script type="text/javascript">
		var all_narudzbina_ids = JSON.parse('{{ json_encode($allIds) }}');
	</script>
	<!-- ORDER LIST -->
	<section class="bw"> 

		<!--=============================
		=            Filters            = 
		==============================-->
		<div class="row flex"> 
			<div class="column medium-4"> 
				<h1 class="inline-block no-margin vert-align">{{ AdminLanguage::transAdmin('Narudžbine') }}</h1>
				<!-- ====================== -->
				<a href="#" class="video-manual" data-reveal-id="orders-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
				<div id="orders-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
					<div class="video-manual-container"> 
						<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Narudžbine') }}</span></p>
						<iframe src="https://player.vimeo.com/video/271250335" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<a class="close-reveal-modal" aria-label="Close">&#215;</a>
				</div>
				<!-- ====================== -->
			</div>

			<div class="orders-input column medium-4"> 
				<input type="text" class="search-field" id="search" autocomplete="on" placeholder="{{ AdminLanguage::transAdmin('Pretraži narudžbine') }}...">
				<button type="submit" id="search-btn" value="{{$search}}" class="m-input-and-button__btn btn btn-primary btn-radius">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</div>
			@if(Admin_model::check_admin(array('NARUDZBINE_AZURIRANJE')))
			<div class="columns medium-4 text-right"><a href="/admin/narudzbina/0" class="btn btn-create btn-small">{{ AdminLanguage::transAdmin('Kreiraj narudžbinu') }}</a>
			</div>
			@endif
			@if(Admin_model::city_pickup_exist()==1 AND (DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code') != ''))
			<div class="columns medium-12 text-right"><a href="{{AdminOptions::base_url()}}admin/priprema-za-slanje-posalji/" class="btn btn-create btn-small">{{ AdminLanguage::transAdmin('Pozovi CityExpress') }}</a></div>
			@endif
		</div>
		
		<div class="o-filters row"> 
			<div class="column medium-5">
				<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri kupca') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="registrovani" {{  in_array('registrovani',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Registrovani') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="neregistrovani" {{ in_array('neregistrovani',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Neregistrovani') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="privatna" {{ in_array('privatna',$statusi) ? 'checked' : '' }}>
						<span class="label-text private">{{ AdminLanguage::transAdmin('Fizička Lica') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="pravna" {{ in_array('pravna',$statusi) ? 'checked' : '' }}>
						<span class="label-text legal-person">{{ AdminLanguage::transAdmin('Pravna Lica') }}</span>
					</label>
				</div>
			</div>

			<div class="column medium-4"> 
				<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri narudžbina') }}:</label> 

				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="nove" {{ in_array('nove',$statusi) ? 'checked' : '' }}>
						<span class="label-text nova">{{ AdminLanguage::transAdmin('Nove') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="prihvacene" {{ in_array('prihvacene',$statusi) ? 'checked' : '' }}>
						<span class="label-text prihvacena">{{ AdminLanguage::transAdmin('Prihvaćene') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="realizovane" {{ in_array('realizovane',$statusi) ? 'checked' : '' }}>
						<span class="label-text">{{ AdminLanguage::transAdmin('Realizovane') }}</span>
					</label>
				</div>

				&nbsp;
				<div class="inline-block">
					<label class="checkbox-label">
						<input type="checkbox" class="JSStatusNarudzbine" data-status="stornirane" {{ in_array('stornirane',$statusi) ? 'checked' : '' }}>
						<span class="label-text stornirana">{{ AdminLanguage::transAdmin('Stornirane') }}</span>
					</label>
				</div>
			</div>

			<div class="column medium-3">

				<div class="row">
					<div class="column medium-9 small-11 no-padd"> 
						<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri dodatnih statusa') }}:</label>
						<div class="order-relative-div">
							<select class="m-input-and-button__input import-select " id="narudzbina_status_id"  multiple="multiple">
								<option value="0">{{ AdminLanguage::transAdmin('Svi dodatni statusi') }}</option>
								@foreach(AdminNarudzbine::dodatni_statusi(true) as $statusi)
								@if(in_array(strval($statusi->narudzbina_status_id),$narudzbina_status_id))	
								<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
								@else
								<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
								@endif				
								@endforeach
							</select>
							<a href="#" id="JSOrderStatusSearch" class="no-margin m-input-and-button__btn btn btn-danger tooltipz" aria-label="{{ AdminLanguage::transAdmin('Traži') }}">
								<i class="fa fa-search" aria-hidden="true"></i>
							</a>
						</div>
					</div>

					<div class="columns medium-12 no-padd">
						<form method="POST" action="{{AdminOptions::base_url()}}admin/order-import" enctype="multipart/form-data" id="JSOrdersImportForm">
							<div class="bg-image-file margin-h-10"> 
								<div class="row"> 
									<div class="columns large-9 medium-12 small-12 no-padd">
										<input type="file" name="import_file">
									</div>
								</div>
								<div class="error"> 
									{{ $errors->first('import_file') ? $errors->first('import_file') : '' }}
								</div>
							</div>	 
						</form>
					</div>
					@if(DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code') != '')
					<div class="column medium-9"> 
						<label class="no-margin">{{ AdminLanguage::transAdmin('Filteri CityExpress statusa') }}:</label>
						<select class="m-input-and-button__input import-select " id="pickup_id" >
							@if($pickup_id == 0)
							<option value="0" selected>{{ AdminLanguage::transAdmin('Svi dodatni statusi') }}</option>
							<option value="1">{{ AdminLanguage::transAdmin('Priprema za slanje') }}</option>
							<option value="2">{{ AdminLanguage::transAdmin('Poslato') }}</option>
							@endif
							@if($pickup_id == 1)
							<option value="0">{{ AdminLanguage::transAdmin('Svi dodatni statusi') }}</option>
							<option value="1" selected>{{ AdminLanguage::transAdmin('Priprema za slanje') }}</option>
							<option value="2">{{ AdminLanguage::transAdmin('Poslato') }}</option>
							@endif
							@if($pickup_id == 2)
							<option value="0">{{ AdminLanguage::transAdmin('Svi dodatni statusi') }}</option>
							<option value="1">{{ AdminLanguage::transAdmin('Priprema za slanje') }}</option>
							<option value="2" selected>{{ AdminLanguage::transAdmin('Poslato') }}</option>
							@endif

						</select>	
					</div>
					@endif
				</div>
				<a href="#" class="m-input-and-button__btn btn btn-danger erase-btn no-margin tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi filtere') }}">
					<span class="fa fa-times" id="JSFilterClear" aria-hidden="true"></span>
				</a>
			</div> 
		</div>
		
		<div class="row o-filters-span-2">

			<div class="columns medium-5">
				<span>{{ AdminLanguage::transAdmin('Ukupno narudžbina') }}: <b>{{count($query)}}</b></span>
				<span>{{ AdminLanguage::transAdmin('Ukupno selektovano') }}: <b id="JSselektovano">0</b></span>
			</div>

			<div class="columns medium-7">

				<span class="datepicker-col">
					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Od') }}:</label>
						<input id="datepicker-from" class="has-tooltip" data-datumdo="{{$datum_do}}" value="{{$dat1}}" type="text">
					</div>

					<div class="datepicker-col__box clearfix">
						<label>{{ AdminLanguage::transAdmin('Do') }}:</label>
						<input id="datepicker-to" class="has-tooltip" data-datumod="{{$datum_od}}" value="{{$dat2}}" type="text">
					</div>
				</span>
			</div>
		</div>


		<!--====  End of Filters  ====-->
		
		<div class="table-scroll JSorders-listing" style="max-height: none;">
			<table class="order-table">
				<thead>
					<tr>
						<td>{{ AdminLanguage::transAdmin('Izmeni') }}</td>
						<th class="JSSort" data-sort_column="ime" data-sort_direction="{{ $sort_column == 'ime' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Ime i prezime') }}</th>
						<th class="JSSort" data-sort_column="broj_dokumenta" data-sort_direction="{{ $sort_column == 'broj_dokumenta' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Broj narudžbine') }}</th>
						<th class="JSSort" data-sort_column="datum_dokumenta" data-sort_direction="{{ $sort_column == 'datum_dokumenta' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Datum') }}</th>
						<th>{{ AdminLanguage::transAdmin('Artikli') }}</th>
						<th class="text-right">{{ AdminLanguage::transAdmin('Iznos') }}</th>
						<th class="JSSort" data-sort_column="telefon" data-sort_direction="{{ $sort_column == 'telefon' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Telefon') }}</th>
						<th class="JSSort" data-sort_column="mesto" data-sort_direction="{{ $sort_column == 'mesto' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Mesto') }}</th>
						<th class="JSSort" data-sort_column="adresa" data-sort_direction="{{ $sort_column == 'adresa' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Adresa kupca') }}</th>
						<th>{{ AdminLanguage::transAdmin('Dodatni status') }}</th>
						<th class="JSSort" data-sort_column="status" data-sort_direction="{{ $sort_column == 'status' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Status') }}</th>
						@if(DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code') != '')
						<th>{{ AdminLanguage::transAdmin('City status') }}</th>
						@endif
					</tr>
				</thead>

				<tbody id="selectable">
					@foreach($query as $row) 
					<tr data-id-narudzbina="{{$row->web_b2c_narudzbina_id}}" class="JSMoreWiew {{AdminNarudzbine::narudzbina_status_css($row->web_b2c_narudzbina_id)}}">
						<td>
							<button class="btn btn-secondary btn-xm edit-article-btn btn-circle small JSOrderEdit tooltipz" aria-label="{{ AdminLanguage::transAdmin('Izmeni porudžbinu') }}">
								<i class="fa fa-pencil" aria-hidden="true"></i>
							</button>
							@if(AdminFiskalizacija::orderCheckFiskalization($row->web_b2c_narudzbina_id))
								<i class="fas fa-cash-register" style="color:#000;"></i>
							@endif
						</td>
						<td>{{Admin_model::narudzbina_kupac($row->web_b2c_narudzbina_id)}}</td>
						
						<td>{{$row->broj_dokumenta}}</td>
						
						<td>{{AdminNarudzbine::datum_narudzbine($row->web_b2c_narudzbina_id)}}</td>
						
						<td class="text-left"> 
							@foreach(AdminNarudzbine::narudzbina_stavke($row->web_b2c_narudzbina_id) as $stavka) 
							<div class="tooltipz counter" aria-label='{{ AdminArticles::find($stavka->roba_id, "naziv_web") }}'>
								<span class="ellipsis-text vert-align inline-block">{{ AdminArticles::find($stavka->roba_id, 'naziv_web') }}</span>
							</div>  
							@endforeach 
						</td>
						
						<td class="text-right">{{AdminCommon::cena(AdminNarudzbine::narudzbina_iznos_ukupno($row->web_b2c_narudzbina_id))}}</td>
						
						<td>{{AdminNarudzbine::kupacTelefoni($row->web_kupac_id)}}</td>
						
						<td>{{AdminNarudzbine::kupacMesto($row->web_kupac_id)}}</td>    
						
						<td>{{AdminNarudzbine::kupacAdresa($row->web_kupac_id)}}</td>
						
						<td>
							<span class="JSOrderAddedStatus ellipsis-text vert-align inline-block tooltipz" aria-label="{{AdminNarudzbine::status_narudzbine($row->web_b2c_narudzbina_id)}}">{{AdminNarudzbine::status_narudzbine($row->web_b2c_narudzbina_id)}}</span>
						</td>

						<td class="JSOrderStatus"> 
							{{AdminNarudzbine::narudzbina_status_active($row->web_b2c_narudzbina_id)}}
						</td>
						@if(DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code') != '')
						<td>{{Admin_model::city_pickup_id($row->web_b2c_narudzbina_id)}}</td>
						@endif
					</tr>
					@endforeach
				</tbody> 
			</table>
		</div> 
		
		<!-- PAGINATION -->
		{{ Paginator::make($query, $count, $limit)->links() }}

	</section>
	
</section> <!-- end of #main-content -->

<!-- MODAL ZA DETALJI PORUDJINE -->
<div id="ordersDitailsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<div class="content"></div>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
<!-- MODAL END -->

<ul class="custom-menu">
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaSelectAll">{{ AdminLanguage::transAdmin('Izaberi sve') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaPrihvati">{{ AdminLanguage::transAdmin('Prihvati') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaRealizuj">{{ AdminLanguage::transAdmin('Realizuj') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStorniraj">{{ AdminLanguage::transAdmin('Storniraj') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStampajPdf">{{ AdminLanguage::transAdmin('Štampaj porudžbine') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStampajPonudu">{{ AdminLanguage::transAdmin('Štampaj ponude') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStampajPredracun">{{ AdminLanguage::transAdmin('Štampaj predračune') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStampajPracun">{{ AdminLanguage::transAdmin('Štampaj račune') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaStatus">{{ AdminLanguage::transAdmin('Dodeli status') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSPorudzbinaObrisi">{{ AdminLanguage::transAdmin('Obriši') }}</button>
	</li>
</ul>


<div id="JSchooseStatusModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni status') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSAddStatusInput m-input-and-button__input">
				<option value="null">{{ AdminLanguage::transAdmin('Bez statusa') }}</option>
				@foreach(AdminNarudzbine::dodatni_statusi() as $row)
				<option value="{{ $row->narudzbina_status_id }}">{{ $row->naziv }}</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSAddStatus"><i class="fa fa-check" aria-hidden="true"></i></button>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSRemoveStatus"><i class="fa fa-close" aria-hidden="true"></i></button>
		</div>
	</div>
</div>
