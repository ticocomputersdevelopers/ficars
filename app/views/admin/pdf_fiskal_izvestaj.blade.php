<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Izveštaj Fiskalizacija</title>

    <style>
     *  {
        font-family: DejaVu Sans;
        box-sizing: border-box;
        font-size: 14px;
     }
     .text-left { text-align: left; }
     .text-right { text-align: right; }
     .text-center { text-align: center; }
     span.block { display: block; }
     .text-uppercase { text-transform: uppercase; }
     .col-6 { width: 49%; display: inline-block; } 
     .fiskal-date-time span { font-size: 13px; }
     .border-top { height: 2px; background: #000; width: auto; margin-top: .5rem; }
     .fiskal-table table, .fiskal-sum table { width: 100%; }
     table { 
         table-layout: fixed;
         width: 100%;
         margin: 5px auto;
         table-layout: auto;
         border-collapse: collapse;
     }
      th {
        /* padding-left: 3px; */
      }
     .fiskal-table table tr td:nth-child(6),
     .fiskal-table table tr td:nth-child(7),
     .fiskal-table table tr td:nth-child(8) {
         text-align: right; 
     }
     .fiskal-table table tr th:nth-child(6), 
     .fiskal-table table tr th:nth-child(7),
     .fiskal-table table tr th:nth-child(8) {
         text-align: right;
     }
     .fiskal-table table tr th { font-weight: normal; }
     .fiskal-table table * { font-size: 13px; }

     .fiskal-table table tr td:nth-child(5),
     .fiskal-table table tr th:nth-child(5) {
         text-align: left;
     }


    </style>
</head><body>

    <div class="pdf_fiskal_izvestaj">
        <div class="fiskal-top">
            <h3>{{ Options::company_name() }} <!-- <span class="block">MP magacin</span> --></h3>
        </div>    

        <div class="fiskal-center text-center">
            <h2 class="text-uppercase">Izvestaj Fiskalizacija</h2>
        </div>

        <div class="fiskal-date-time">
            <div class="col-6 text-left">
                @if(!empty($date_from))
                    <span class="block">Od: {{ $date_from }}</span>
                @endif
                @if(!empty($date_to))
                    <span class="block">Do: {{ $date_to }}</span> 
                @endif
                @if(!is_null($web_kupac))
                    <div class="row">    
                    <span>Kupac:</span>
                    @if(!empty(trim($web_kupac->ime.' '.$web_kupac->prezime)))
                            {{ $web_kupac->ime }} {{ $web_kupac->prezime }}
                        @endif
                        @if(!empty(trim($web_kupac->naziv))) 
                           {{ $web_kupac->naziv }}
                        @endif
                        @if(!empty(trim($web_kupac->pib)))
                           {{ $web_kupac->pib }}
                        @endif 
                    </div>
                @endif
                <span class="block">Tip računa: {{ $vrsta_racuna }}</span>
            <!-- <span class="block">Za sve kupce: Proknjizeni dokumenti</span> -->
            </div>

            <div class="col-6 text-right">
                <span class="block">Datum: {{ date('d.m.Y.') }}</span>
                <span class="block">Vreme: {{ date('H:i:s') }}</span>
            </div>
        </div>
        <div class="border-top"></div>

        <div class="fiskal-table"> 
            <table>  
                <tr>
                    <th scope="col" class="text-left">Rbr</th> 
                    <th scope="col" class="text-left">Datum</th>
                    <th scope="col" class="text-left">Ostalo</th>
                    <th scope="col" class="text-left">Gotovina</th>
                    <th scope="col">Platna Kartica</th>
                    <th scope="col">Ček</th>
                    <th scope="col" class="text-left">Prenos na račun</th>
                    <th scope="col">Vaučer</th>
                    <th scope="col" class="text-right">Instant plaćanje</th>
                    <th scope="col" class="text-right">Ukupno</th>
                    <th scope="col" class="text-right">PDV</th>
                </tr>
                
                    <?php $rbr = 0; ?>
                    @foreach($rows as $date => $row)
                    <tr>
                        <?php $payment = AdminFiskalizacija::getAnaliticsPaymentAmount($row,$date); 
                              $rbr = $rbr + 1;
                        ?>
                        <td>{{ $rbr }}</td>
                        <td>{{ date('d.m.Y.',strtotime($date)) }}</td>
                        <td class="text-left">{{ isset($payment->Other) ? number_format($payment->Other,2, '.', ',') : '0.00' }}</td>
                        <td>{{ isset($payment->Cash) ? number_format($payment->Cash,2, '.', ',') : '0.00' }}</td>
                        <td>{{ isset($payment->Card) ? number_format($payment->Card,2, '.', ',') : '0.00' }}</td>
                        <td>{{ isset($payment->Check) ? number_format($payment->Check,2, '.', ',') : '0.00' }}</td>
                        <td>{{ isset($payment->WireTransfer) ? number_format($payment->WireTransfer,2, '.', ',') : '0.00' }}</td>
                        <td>{{ isset($payment->Voucher) ? number_format($payment->Voucher,2, '.', ',') : '0.00' }}</td>
                        <td class="text-right">{{ isset($payment->MobileMoney) ? number_format($payment->MobileMoney,2, '.', ',') : '0.00' }}</td>
                        <td class="text-right">{{ isset($payment->total) ? number_format($payment->total,2, '.', ',') : '0.00' }}</td>
                        <td class="text-right">{{ number_format(AdminFiskalizacija::getAnaliticsTaxAmount($row,$date),2, '.', ',') }}</td>
                        
                    </tr>
                    @endforeach
                   
   
                <div class="border-top"></div> 

                    <?php 
                            if(!empty(array_map('current',$rows))) { 
                            $total = AdminFiskalizacija::getAnaliticsPaymentAmount($racuni_ids,null); 
                            } 
                        ?>
                    
                    <tr>
                        <th scope="col"></th>
                        <th class="text-left">Ukupno za ceo period:</th>
                        <th class="text-left">{{ isset($total->Other) ? number_format($total->Other,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-left">{{ isset($total->Cash) ? number_format($total->Cash,2, '.', ',') : '0.00' }}</th>
                        <th scope="col">{{ isset($total->Card) ? number_format($total->Card,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-left">{{ isset($total->Check) ? number_format($total->Check,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-left">{{ isset($total->WireTransfer) ? number_format($total->WireTransfer,2, '.', ',') : '0.00' }}</th>
                        <th scope="col">{{ isset($total->Voucher) ? number_format($total->Voucher,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-right">{{ isset($total->MobileMoney) ? number_format($total->MobileMoney,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-right">{{ isset($total->total) ? number_format($total->total,2, '.', ',') : '0.00' }}</th>
                        <th scope="col" class="text-right">{{ !empty(array_map('current',$rows)) ? number_format(AdminFiskalizacija::getAnaliticsTaxAmount($racuni_ids,null),2, '.', ',') : '0.00' }}</th>
                    </tr> 
            </table>
        </div>
    </div>
</body></html>