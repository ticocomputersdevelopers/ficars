<!DOCTYPE html>
<html><head>
<meta charset="UTF-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
    <style>
    *  {
        margin: 0;
        padding: 0;
        font-family: DejaVu Sans;
        box-sizing: border-box;
        font-size: 14px;
    }
    .col-6 {
        width: 48%;
        display: inline-block;
        margin-right: 1%;
    }
    .fiskalni-racun-pdf {
        text-align: center; 
        margin: 30px;
        padding: 25px;
    }
    ul { list-style: none; }

    .fiskalni-racun-main li { 
        display: inline-block;
        max-width: 100%; 
        padding-bottom: .5rem;
        margin: 0;
        text-align: left; 
    }
    span { display: block; }

    .text-left { text-align: left; }

    small { font-size: 11px; }

    hr {
        background-color: #222;
        border: 0 none;
        color: #000;
        height: 1px;
    }
    hr.light {
        background-color: #eee;
        border: 0 none;
        color: #eee;
        height: 1px;
    }
    hr.invisible { background: transparent; }

    .border-btm-list {
        border-bottom: 1px solid #333;
    }
    .relative { position: relative; }

    .right-ordered-list li { position: relative; padding: 0; }

    .right-ordered-list span { display: inline-block; text-align: right; }

    .right-ordered-list li { margin: 0px 0;}

   /* .bottom-w-100 { width: 100%; position: absolute; bottom: 10%; }*/
    .qr-code { height: 150px; }
    tr  th { width: 138px; text-align: center; }
    tr th:first-child { text-align: left; }
    tr th:last-child { text-align: right; }
    table { border-collapse: collapse; width: 100%; }

    .border-bottom-td td { border-bottom: 1px solid #000; }
    .bottom-w-100 tbody .border-bottom-td { text-align: center; }
    .bottom-w-100 tbody .border-bottom-td td:last-child { text-align: right; }
    .stope tbody tr td:first-child { text-align: left; }
    .stope tr th { text-align: left; }
    .col-3 { width: 45%; display: inline-block; text-align: left; padding: 0; margin: 0; line-height: 1 }
    .col-9 { width: 50%; text-align: right; display: inline-block; padding: 0; margin: 0; line-height: 1 }

    .li-d-block li {
        display:block;
    }
    .col-9-b { display: block; margin: 0; padding: 0; }
 
   </style>

   </head><body style="margin-top: 30px"> 

        <div class="fiskalni-racun-pdf">
            <div class="fiskalni-racun-main">
               
                <div class="col-6">
                    <ul>
                        <li><strong>PIB:</strong> {{ $racun->pib }}</li>
                        <hr>
                        <li><strong>Dobavljač:</strong> {{ $racun->poslovno_ime }}</li>
                        <hr>
                        <li><strong>Mesto prodaje:</strong> {{ $racun->naziv_lokacije }}</li>
                        <hr>
                        <li><strong>Adresa:</strong> {{ $racun->adresa }}</li>
                        <hr>
                        <li><strong>Opština:</strong> {{ AdminFiskalizacija::cirilicToLatin($racun->okrug) }}</li>
                        <hr>
                        <li><strong>Kasir:</strong> {{ $racun->kasir }}</li>
                        <hr>
                        <li><strong>ID kupca:</strong> {{ $racun->kupac_id }}</li>
                        <hr>
                        <li><strong>Opciono polje kupca:</strong> {{ $racun->kupac_mesto_placanja_id }}</li>
                        <hr>
                        <li><strong>Esir broj:</strong> {{ $racun->esir }}</li>
                        <hr>
                        <li><strong>Esir vreme:</strong> {{ !empty($racun->datum_prometa) ? date('d.m.Y. H:i:s',strtotime($racun->datum_prometa)) : '' }}</li>
                        @if(!empty($racun->referentni_broj_dokumenta))
                        <hr>
                        <li><strong>Ref. broj:</strong> {{ $racun->referentni_broj_dokumenta }}</li>
                        <hr>
                        <li><strong>Ref. vreme:</strong> {{ date('d.m.Y. H:i:s',strtotime($racun->referentni_dokument_datum)) }}</li>
                        @endif
                        <hr>
                    </ul>
                </div>


                <div class="col-6">
                    <ul class="relative">
                        <li class="col-6"><strong>Broj računa</strong> 
                            <span><strong>{{ $racun->broj_dokumenta }}</strong></span>
                            <br>
                            <span><strong>Račun je proveren</strong></span>
                            <br>
                            <span><img src="data:image/gif;base64,{{$racun->qr_kod}}" class="qr-code"></span>
                            <br>
                            <span><strong>{{ AdminFiskalizacija::cirilicToLatin(str_replace("=","",explode("\r\n",$racun->tekstualni_prikaz)[0])) }}</strong></span>
                        </li>
                        <hr>
                    </ul>
 
                    <ul class="relative li-d-block">
                        <li>
                            <strong class="col-3">Za uplatu:</strong> 
                            <span class="col-9">{{ str_replace(".",",",$racun->iznos) }}</span>
                        </li>
                        <li>
                            <strong class="col-3">Porez ukupno:</strong> 
                            <span class="col-9">{{ str_replace(".",",",$porez_ukupno) }}</span>
                        </li> 

                        <li>
                            <strong class="col-3">Način plaćanja:</strong> 
                            <div class="col-9"> 
                                @foreach($placanja as $placanje)
                                <span class="col-9-b">{{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }}: {{ str_replace(".",",",$placanje->iznos) }}</span>
                                @endforeach
                            </div>   
                        </li>
                        <li>
                            <strong class="col-3">Vrsta računa:</strong> 
                            <span class="col-9">{{ AdminFiskalizacija::mappedVrstaRacuna($racun->tip_racuna,$racun->vrsta_transakcije) }}</span>
                        </li> 
                        <li>
                            <strong class="col-3">Vreme na bezbednosnom elementu:</strong>
                            <span class="col-9">{{ date('d.m.Y H:i:s',strtotime($racun->datum_racuna)) }}</span>
                        </li> 
                        <li>
                            <strong class="col-3">Brojač računa:</strong>
                            <span class="col-9">{{ $racun->brojac_racuna }}</span>
                        </li> 
                    </ul>

                    <!-- <ul class="right-ordered-list"> 
                        <br>
                        <li style="width: 64%"><strong>Za uplatu:</strong> <span style="width: 110%;">{{ str_replace(".",",",$racun->iznos) }}</span></li>
                        <hr class="invisible"> 
                        <li style="width: 51%"><strong>Porez ukupno:</strong> <span style="width: 121%;">{{ str_replace(".",",",$porez_ukupno) }}</span></li>
                        <hr class="invisible">
                        <li style="width: 60%"><strong>Način plaćanja:</strong> 
                            @foreach($placanja as $placanje) 
                            <span style="width: 99%; text-align: right;">{{ AdminFiskalizacija::getPlacanjeNaziv($placanje->racun_vrsta_placanja_naziv_id) }}: {{ str_replace(".",",",$placanje->iznos) }}</span></li>
                            <br>
                            @endforeach 
                        <hr class="invisible">
                        <li style="width: 84%"><strong>Vrsta računa:</strong> <span style="width: 76%;">{{ AdminFiskalizacija::mappedVrstaRacuna($racun->tip_racuna,$racun->vrsta_transakcije) }}</span></li>
                        <hr class="invisible">
                        <li style="width: 55%"><strong>Vreme na bezbedonosnom elementu:</strong> <span style="width: 130%; text-align:right; margin-top: -25px;">{{ date('d.m.Y H:i:s',strtotime($racun->datum_racuna)) }}</span></li>
                        <hr class="invisible">
                        <li style="width: 47%"><strong>Brojač računa:</strong> <span style="width: 130%; text-align: right">{{ $racun->brojac_racuna }}</span></li>
                        <hr class="invisible">
                    </ul> -->
                </div>
        
            <br>

            <div class="bottom-w-100">
                <ul>
                  <li><strong>Artikli:</strong></li>
                  <hr>
                </ul>

                <table>
                    
                        <tr>
                            <th><strong>GTIN</strong></th>
                            <th><strong>Ime</strong></th>
                            <th><strong>Cena</strong></th>
                            <th><strong>Količina</strong></th>
                            <th><strong>Ukupna cena</strong></th> 
                        </tr>
                    
                    
                        @foreach($stavke as $stavka)
                            <tr class="border-bottom-td">
                                <td>{{ $stavka->gtin }}</td>
                                <td>{{ $stavka->naziv_stavke }} ({{$stavka->etikete}})</td>
                                <td>{{ number_format($stavka->pcena,2, ',', '') }}</td>
                                <td>{{ number_format($stavka->kolicina,3, ',', '') }}</td>
                                <td>{{ number_format($stavka->uk_cena,2, ',', '') }}</td>
                            </tr>
                        @endforeach
                    
                </table>

             

                <!-- <ul>
                  <li style="width: 40%;"><strong>GTIN</strong></li>
                  <li style="width: 12.5%;"><strong>Ime</strong></li>
                  <li style="width: 12.5%;"><strong>Cena</strong></li>
                  <li style="width: 12.5%;"><strong>Količina</strong></li>
                  <li style="width: 15.5%; padding-left: 2rem; text-align: right;"><strong>Ukupna cena</strong></li>
                  <hr style="margin: 0; padding: 0;"> 
                </ul>
                @foreach($stavke as $stavka)
                <ul class="border-btm-list">
                  <li style="width: 27%; text-align: center; padding-left: 0rem; display: inline-block;">{{ $stavka->gtin }}</li>
                  <li style="width: 25%; text-align: left; padding-left: .2rem">{{ $stavka->naziv_stavke }} ({{$stavka->etikete}})</li>
                  <li style="width: 12.5%; text-align: left;"><strong>{{  number_format($stavka->pcena,2, ',', '') }}</strong></li>
                  <li style="width: 12.5%; text-align: center;"><strong>{{  number_format($stavka->kolicina,3, ',', '') }}</strong></li> 
                  <li style="width: 19.5%; text-align: right;"><strong>{{ number_format($stavka->uk_cena,2, ',', '') }}</strong></li>
                </ul>
                @endforeach

                <ul>
                  <li><strong>Poreske stope:</strong></li>
                  <hr>
                </ul>
                <ul style="padding: 0; margin: 0;">
                  <li style="width: 32%;"><strong>Oznaka</strong></li>
                  <li style="width: 21.6%;"><strong>Ime</strong></li>
                  <li style="width: 21.6%;"><strong>Stopa</strong></li>
                  <li style="width: 21.6%; text-align: right;"><strong>Porez</strong></li>
                </ul>
                <hr>
                @foreach($porezi as $porez)
                <ul style="padding:0; margin: 0;" class="border-btm-list">
                  <li style="width: 32%;">{{ $porez->poreska_oznaka }}</li>
                  <li style="width: 21.6%;">{{ $porez->naziv_kategorije }}</li>
                  <li style="width: 21.6%;">{{ number_format($porez->iznos_poreske_stope,2,',','') }} %</li>
                  <li style="width: 21.6%; text-align: right;">{{ number_format($porez->iznos_poreza,2,',','') }}</li>
                </ul>
                @endforeach
                <hr> -->

                <br><br>

                <table class="stope">
                    
                        <tr>
                          <th>Poreske stope:</th>
                        </tr> 
                   
                        @foreach($porezi as $porez)
                        <tr class="border-bottom-td">
                            <td>{{ $porez->poreska_oznaka }}</td>
                            <td>{{ $porez->naziv_kategorije }}</td>
                            <td>{{ number_format($porez->iznos_poreske_stope,2,',','') }} %</td>
                            <td>{{ number_format($porez->iznos_poreza,2,',','') }}</td>
                        </tr>
                        @endforeach
                    
                </table>

            </div>
        </div>
    </div>




   </body></html>