<section id="main-content">

	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/narudzbina/{{$narudzbina->web_b2c_narudzbina_id}}">
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					{{ AdminLanguage::transAdmin('Nazad') }}
				</div>
			</a>
		</div>
	</div>

	<div class="row"> 
	  	<div class="columns medium-12 small-12"> 
		<form method="POST" action="{{AdminOptions::base_url()}}admin/narudzbina-stavka-city-update">
			<input type="hidden" name="web_b2c_narudzbina_id" value="{{ $narudzbina->web_b2c_narudzbina_id }}">		
			<input type="hidden" name="posta_zahtev_api_id" type="text" value="{{$posta_zahtev_api_id}}">

			<div class="flat-box"> 
				<div class="field-group">
					<div class="row"> 
						<div class="columns medium-3"> 
							<label>{{ AdminLanguage::transAdmin('Pošta') }}:</label>
							<select name="posta_slanje_id">
								@foreach(AdminPartneri::poste() as $posta_slanje)
								@if((!is_null(Input::old('posta_slanje_id')) ? Input::old('posta_slanje_id') : $narudzbina_stavka->posta_slanje_id) == $posta_slanje->posta_slanje_id)						
								<option value="{{$posta_slanje->posta_slanje_id}}" selected >{{$posta_slanje->naziv}}</option>
								@else
								<option value="{{$posta_slanje->posta_slanje_id}}">{{$posta_slanje->naziv}}</option>
								@endif
								@endforeach
							</select> 
						</div> 
					</div>
				</div>
			</div>

			<div class="flat-box"> 
				<div class="field-group">
					<h5 class="title-med"> Pošiljalac </h5>
					<div class="row"> 
					  	<div class="columns medium-2">  
							<label>{{ AdminLanguage::transAdmin('Pošiljalac') }}:</label>
							<select name="posiljalac_id" class="search_select" id="JSPartnerId">
								@foreach(AdminPartneri::partneri() as $partner)
								@if((!is_null(Input::old('posiljalac_id')) ? Input::old('posiljalac_id') : $posiljalac_id) == $partner->partner_id)
								<option value="{{$partner->partner_id}}" selected>{{AdminPartneri::partner_naziv($partner->partner_id)}}</option>
								@else
								<option value="{{$partner->partner_id}}">{{AdminPartneri::partner_naziv($partner->partner_id)}}</option>
								@endif
								@endforeach
							</select> 
						</div>

						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('Poštanski broj i Grad') }}</label>
							<select class="form-control" id="JSPartnerMesto" name="posiljalac_ptt">
								<option value="0">Izaberite adresu pošiljaoca</option>	
								@if(isset($partner_ptt_cityexpress_id) AND !empty($partner_ptt_cityexpress_id) )
								<option value="{{ $partner_ptt_cityexpress_id }}" selected>{{AdminPartneri::cityNaziv($partner_ptt_cityexpress_id)->ptt}}->{{AdminPartneri::cityNaziv($partner_ptt_cityexpress_id)->naziv}}</option>
								@endif
								@foreach(AdminNarudzbine::mesta_cityexpress() as $mesto )
								<option value="{{ $mesto->narudzbina_mesto_id }}">{{ $mesto->ptt }}->{{ $mesto->naziv }}</option>						
								@endforeach
							</select>
							<div class="error red-dot-error">{{ $errors->first('posiljalac_ptt') ? $errors->first('posiljalac_ptt') : "" }}</div>
						</div>

						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('Ulica') }}</label>						
								<input name="posiljalac_ulica" id="JSPartnerAdresa" type="text" value="{{AdminPartneri::partner($posiljalac_id)->adresa}}">					
						</div>
					</div> 
				</div>
			</div>
			<div class="flat-box"> 
				<div class="field-group">
					<h5 class="title-med"> Primalac </h5>
					<div class="row">
						<div class="columns medium-2">  
							<label>{{ AdminLanguage::transAdmin('Primalac') }}:</label>
							<select name="primalac_id">
								@foreach(AdminPartneri::kupac() as $kupac)								
								@if((!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $kupac->web_kupac_id) == $web_kupac->web_kupac_id)
									@if(isset($kupac->ime) AND isset($kupac->ime))
									<option value="{{$kupac->web_kupac_id}}" selected>{{$kupac->ime}} {{$kupac->prezime}}</option>
									@else
									<option value="{{$kupac->web_kupac_id}}" selected>{{$kupac->naziv}}</option>
									@endif
								@endif
								@endforeach
							</select> 
						</div>
						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('Postanski broj i Grad') }}</label>
							<select class="form-control search_select" name="primalac_ptt" id="JSMesto">					
							<option value="0">Izaberite adresu primaoca</option>	
								@if(isset($kupac_ptt_cityexpress_id) AND !empty($kupac_ptt_cityexpress_id) )
								<option value="{{ $kupac_ptt_cityexpress_id }}" selected>{{AdminPartneri::cityNaziv($kupac_ptt_cityexpress_id)->ptt}}->{{AdminPartneri::cityNaziv($kupac_ptt_cityexpress_id)->naziv}}</option>
								@endif
								@foreach(AdminNarudzbine::mesta_cityexpress() as $mesto )
								<option value="{{ $mesto->narudzbina_mesto_id }}">{{ $mesto->ptt }}->{{ $mesto->naziv }}</option>						
								@endforeach
							
							</select>
							<div class="error red-dot-error">{{ $errors->first('primalac_ptt') ? $errors->first('primalac_ptt') : "" }}</div>				
						</div>
						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('Ulica i broj') }}</label>
							<input name="adresa_kupca" type="text" value="{{$web_kupac->adresa}}">				
						</div>
						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('Telefon') }}</label>
							<input name="kupac_telefon" type="text" value="{{$web_kupac->telefon}}">				
						</div>
						<div class="columns medium-2">
							<label>{{ AdminLanguage::transAdmin('E-mail') }}</label>
							<input name="kupac_mail" type="text" value="{{$web_kupac->email}}">					
						</div>
					</div> 
				</div>
			</div>

		<div class="flat-box"> 
			<div class="field-group">
				<h5 class="title-med"> Tehnikalije </h5>
				<div class="row"> 
					<div class="columns medium-2"> 
						<label>{{ AdminLanguage::transAdmin('Vreme dostave') }}</label>
						<select class="form-control" name="vreme_dostave">

							<option value="21" {{(Input::old('vreme_dostave') ? Input::old('vreme_dostave') : $vreme_dostave) == 21 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Do 10:30') }}</option>
							<option value="22" {{(Input::old('vreme_dostave') ? Input::old('vreme_dostave') : $vreme_dostave) == 22 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Do 08:30') }}</option>
							<option value="100" {{(Input::old('vreme_dostave') ? Input::old('vreme_dostave') : $vreme_dostave) == 100 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Celog dana') }}</option>
						</select>
					</div>
					<div class="columns medium-2"> 
						<label>{{ AdminLanguage::transAdmin('Tip obaveštenja') }}</label>
						<select class="form-control" name="tip_obavestenja_id">
							<option value="0" {{(Input::old('tip_obavestenja_id') ? Input::old('tip_obavestenja_id') : $tip_obavestenja_id) == 0 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Bez') }}</option>
							<option value="1" {{(Input::old('tip_obavestenja_id') ? Input::old('tip_obavestenja_id') : $tip_obavestenja_id) == 1 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Email') }}</option>
							<option value="2" {{(Input::old('tip_obavestenja_id') ? Input::old('tip_obavestenja_id') : $tip_obavestenja_id) == 2 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('SMS') }}</option>
							<option value="3" {{(Input::old('tip_obavestenja_id') ? Input::old('tip_obavestenja_id') : $tip_obavestenja_id) == 3 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Oba') }}</option>
						</select>
					</div>

					<div class="columns medium-2"> 
						<label>{{ AdminLanguage::transAdmin('Dostava subotom') }}</label>
						<select class="form-control" name="dostava_subotom">
							<option value="false" {{(Input::old('dostava_subotom') ? Input::old('dostava_subotom') : $dostava_subotom) == false ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('NE') }}</option>
							<option value="true" {{(Input::old('dostava_subotom') ? Input::old('dostava_subotom') : $dostava_subotom) == true ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('DA') }}</option>
						</select>
					</div>
					<div class="columns medium-2"> 
						<label>{{ AdminLanguage::transAdmin('Otkup plaća')}}</label>
						<select class="form-control" name="otkupnina">
							<option value="4" {{(!is_null(Input::old('otkupnina')) ? Input::old('otkupnina') : $posta_zahtev->otkupnina) == 4 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Primalac') }}</option>
							<option value="0" {{(Input::old('otkupnina') ? Input::old('otkupnina') : $posta_zahtev->otkupnina) == 0 ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Pošiljalac') }}</option>
						</select>
					</div>
				</div>

				<div class="row"> 
					<div class="columns medium-1"> 
						<label>{{ AdminLanguage::transAdmin('Masa(kg)') }}</label>
						<input name="masa" type="text" value="{{ htmlentities(Input::old('masa') ? intval(Input::old('masa')) : intval($masa)) }}">
						<div class="error red-dot-error">{{ $errors->first('masa') ? $errors->first('masa') : "" }}</div>
					</div> 	
			 		<div class="columns medium-3"> 
						<label>{{ AdminLanguage::transAdmin('Opis pošiljke') }}</label>
						<input name="opis" type="text" value="{{ (Input::old('opis') != null) ? Input::old('opis') : $opis }}">
						<div class="error red-dot-error">{{ $errors->first('opis') ? $errors->first('opis') : "" }}</div>
					</div>
					<div class="columns medium-3"> 
						<label>{{ AdminLanguage::transAdmin('Napomena') }}</label>
						<input name="napomena" type="text" value="{{ (Input::old('napomena') != null) ? Input::old('napomena') : trim($napomena) }}">
						<div class="error red-dot-error">{{ $errors->first('napomena') ? $errors->first('napomena') : "" }}</div>
					</div>
					<div class="columns medium-2 clearfix"> 
						<label>{{ AdminLanguage::transAdmin('Cena otkupa') }}</label>
						<input style="max-width: 70px;" class="pull-left" name="cena_otkupa" type="text" value="{{ (Input::old('cena_otkupa') != null) ? Input::old('cena_otkupa') : $cena_otkupa }}"><span class="pull-left"> &nbsp; RSD </span>
						<div class="error red-dot-error">{{ $errors->first('cena_otkupa') ? $errors->first('cena_otkupa') : "" }}</div>
					</div>
			 	</div>
			</div>
		</div>
		<div class="flat-box"> 
	    	<div class="row"> 
				@foreach($stavke as $stavka)
					<input name="stavka_id" type="hidden" value="{{$stavka->web_b2c_narudzbina_stavka_id}}">
				@endforeach
				<div class="columns medium-2">  
					<label>{{ AdminLanguage::transAdmin('Stavke') }}:</label>
					<select  class="JSStavka" name="stavka_id">
					@foreach($stavke as $stavka)
						@if(DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id',$stavka->web_b2c_narudzbina_stavka_id)->pluck('stavka_id') != $stavka->web_b2c_narudzbina_stavka_id)
						<option  value="{{$stavka->web_b2c_narudzbina_stavka_id}}">{{AdminNarudzbine::nazivweb($stavka->web_b2c_narudzbina_stavka_id)}}</option>
						@endif
					@endforeach	
					</select> 
				</div>
				<div class="columns medium-2"> 
					<label>{{ AdminLanguage::transAdmin('Kolicina') }}:</label>
					<input name="kolicina" type="text" value="">
					<div class="error red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : "" }}</div>
				</div>
				<table>
					<thead>
					<tr>
						<th>{{ AdminLanguage::transAdmin('Artikal') }}</th>
						<th>{{ AdminLanguage::transAdmin('Kolicina') }}</th>
					</tr>
					</thead>

					<tbody>
					@foreach(AdminNarudzbine::narudzbina_stavka_posta($posta_zahtev_api_id) as $row)	

						<tr>
							<td>{{ AdminNarudzbine::artikal_narudzbina($row->stavka_id) }}</td>								
							<td>{{ number_format($row->kolicina,0,'.','')}}
							<a href="{{AdminOptions::base_url()}}admin/obrisi-stavku/{{$row->stavka_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>	
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>

			<div class="btn-container center">
				<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
			</div>
		</div>
	</form>
	</div>	
	</div>
</section>
