<div class="row"> 
	<br>
	<h4>{{ AdminLanguage::transAdmin('Rekapitulacija') }}</h4>
</div>
		
<div class="row"> 
	<div class="col-7"> 
		<table class="rekapitulacija">
			<thead>
			<tr>
				<td></td>
				<td>{{ AdminLanguage::transAdmin('Osnovica') }}</td>
				<td>{{ AdminLanguage::transAdmin('PDV') }}</td>
			</tr>
			</thead>

					<tbody>
						<tr>
							<td>Oslobođeno poreza</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'bez_poreza')->osnovica)}}</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'bez_poreza')->pdv)}}</td>
						</tr>
						<tr>
							<td>Po posebnoj stopi</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'posebna_stopa')->osnovica)}}</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'posebna_stopa')->pdv)}}</td>
						</tr>
						<tr>
							<td>Po opštoj stopi</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'opsta_stopa')->osnovica)}}</td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_stope_osnovica_i_pdv($web_b2c_narudzbina_id,'opsta_stopa')->pdv)}}</td>

						</tr>
						<tr>
							<td><strong>Ukupno</strong></td>
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
						</tr>
					</tbody>
		</table>
	</div>

	<div class="col-4"> 	
		<table class="summary">
			<tr class="text-right">
				<td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span> {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
			</tr>
			<tr class="text-right">
				<td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
			</tr>
			<?php $popust = Order::popust($web_b2c_narudzbina_id); ?>
		    @if($popust> 0)
		        <tr class="text-right">		                
		         	<td class="summary text-right"><span class="text-center sum-span" style="padding-right: 20px">{{ AdminLanguage::transAdmin('Popust') }}:</span>      
		              	{{AdminCommon::cena($popust)}}
		          	</td>
		        </tr>	
		    @endif
			@if(($cena_dostave = AdminCommon::cena_dostave($web_b2c_narudzbina_id)) > 0)
            <tr class="text-right">		                
                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
                	{{AdminCommon::cena($cena_dostave)}}	                	
             	</td>
            </tr>
            @endif
            <tr class="text-right">
                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
                {{AdminCommon::cena((AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+AdminCommon::cena_dostave($web_b2c_narudzbina_id)-$popust))}}</td>
            </tr>
		</table>
    </div>
</div>