@extends('rma.templates.main')
@section('content')
<div id="main-content" class="kupci-page">

	<div class="flat-box"> 
		@if(Admin_model::check_admin(array('RMA_AZURIRANJE')))
		<div class="row"> 
			<a class="btn btn-create btn-small" href="{{RmaOptions::base_url()}}rma/radni-nalog-prijem/0">Novi radni nalog</a> 
		</div>
		@endif
		@if(RmaOptions::user('admin'))
		<div class="row">  
			<div class="columns medium-12 no-padd">      
				<button id="JSRadniNalogIzvestajSve" class="btn btn-primary btn-small">Detaljni izveštaj za primenjene filtere</button> 
				<button id="JSRadniNalogIzvestaj" class="btn btn-primary btn-small">Izveštaj za primenjene filtere</button> 
				<button id="JSRadniNalogIzvestajServiseri" class="btn btn-primary btn-small">Izveštaj za servisere</button> 
				<!-- <button id="JSRadniNalogIzvestajBrendovi" class="btn btn-primary btn-small">Izveštaj brendovi</button>  -->
				<!-- <button id="JSRadniNalogIzvestajGrupe" class="btn btn-primary btn-small">Izveštaj grupe</button>  -->
<!-- 
				<div class="inline-block vert-al-top">  
					<div class="inline-block"> 	
						<select name="grupa_pr_id">
							{{RMA::selectGroups()}}
						</select>
					</div>
					<button id="JSRadniNalogIzvestajGrupeModeli" class="btn btn-primary btn-small">Izveštaj modeli</button>
				</div>  -->
				
				<button id="JSRadniNalogIzvestajRezervniDelovi" class="btn btn-primary btn-small">Izveštaj utroseni materijal</button> 
				<button id="JSRadniNalogIzvestajOperacije" class="btn btn-primary btn-small">Izveštaj operacije</button> 
			</div>
		</div>

		<br>
		
		@endif

		<div class="row">
			<div class="columns medium-3 no-padd">  
				<div class="m-input-and-button">
					<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="Pretraga..." class="m-input-and-button__input">
					<button id="JSRadniNalogSearch" class="btn btn-primary m-input-and-button__btn lineh">Pretraga</button>
					<a class="btn btn-danger btn-small" href="{{RmaOptions::base_url()}}rma">Poništi</a>
				</div>  
			</div>

			<div class="columns medium-9 no-padd"> 
				<div class="row"> 
					<div class="columns medium-2">
						<input name="datum_od" type="text" value="{{ $datum_od }}" autocomplete="off" placeholder="Datum od">
					</div>
					<div class="columns medium-2">
						<input name="datum_do" type="text" value="{{ $datum_do }}" autocomplete="off" placeholder="Datum do">
					</div>
					<div class="columns medium-2">
						<select name="radni_nalog_status_id">
							<option value="0">Svi satusi</option>
							{{ RMA::statusSelect($radni_nalog_status_id) }}
						</select>
					</div>
					@if(RmaOptions::user('admin'))
					<div class="columns medium-2">
						<select name="otpis">
							<option value="null">Svi uređaji</option>
							<option value="1" {{ !is_null($otpis) && $otpis == 1 ? 'selected' : '' }}>Otpis: Da</option>
							<option value="0" {{ !is_null($otpis) && $otpis == 0 ? 'selected' : '' }}>Otpis: Ne</option>
						</select>
					</div>
					<div class="columns medium-2">
						<select name="ostecen">
							<option value="null">Svi uređaji</option>
							<option value="1" {{ !is_null($ostecen) && $ostecen == 1 ? 'selected' : '' }}>Oštećen: Da</option>
							<option value="0" {{ !is_null($ostecen) && $ostecen == 0 ? 'selected' : '' }}>Oštećen: Ne</option>
						</select>
					</div>
					@endif
				</div> 
			</div>
		</div> 
		<br>
		@if(RmaOptions::user('admin'))
		<div class="row"> 
			<div class="columns medium-3 no-padd">
				<div class="relative"> 
					<select name="partner_id" multiple>
						{{ RMA::servisMultiSelect($partner_ids) }}
					</select>
					<button id="JSRadniNalogPartnerSearch" class="btn btn-primary m-input-and-button__btn btn-abs"><i class="fa fa-check" area-hidden="true"></i></button>
				</div>
			</div>
			<div class="columns medium-3">
				<div class="relative"> 
					<select name="serviser_id" multiple>
						{{ RMA::serviseriMultiSelect((count($partner_ids) > 0 ? $partner_ids : array()),$serviser_ids) }}
					</select>
					<button id="JSRadniNalogServiserSearch" class="btn btn-primary m-input-and-button__btn btn-abs"><i class="fa fa-check" area-hidden="true"></i></button>
				</div>
			</div> 
		</div>
		<br>
		@endif
		<div class="row"> 
			<div class="columns medium-12 no-padd">
				<div class="table-scroll"> 
					<table>
						<tr>
							<td>Ukupno: {{ $count }}</td>
							<th class="JSSort" data-sort_column="broj_naloga" data-sort_direction="{{ $sort_column == 'broj_naloga' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Broj</th>
							<th class="JSSort" data-sort_column="kupac_id" data-sort_direction="{{ $sort_column == 'kupac_id' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Klijent</th>
							<th class="JSSort" data-sort_column="partner_id" data-sort_direction="{{ $sort_column == 'partner_id' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Servis</th>
							<th class="JSSort" data-sort_column="datum_prijema" data-sort_direction="{{ $sort_column == 'datum_prijema' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Datum prijema</th>
							<th class="JSSort" data-sort_column="datum_zavrsetka" data-sort_direction="{{ $sort_column == 'datum_zavrsetka' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Datum realizacije</th>
							<th class="JSSort" data-sort_column="uredjaj" data-sort_direction="{{ $sort_column == 'uredjaj' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Uređaj</th>
							<th class="JSSort" data-sort_column="opis_kvara" data-sort_direction="{{ $sort_column == 'opis_kvara' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Opis kvara</th>
							<td class="JSSort" data-sort_column="iznos" data-sort_direction="{{ $sort_column == 'iznos' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Cena</td>
							<td class="JSSort" data-sort_column="status_id" data-sort_direction="{{ $sort_column == 'status_id' ? ($sort_direction == 'ASC' ? 'DESC' : 'ASC') : 'ASC' }}">Status</td>
							<td></td>
						</tr>
						@foreach($radni_nalozi as $row)
						<tr>
							<td class="table-col-icons">
								<a class="edit icon" href="{{RmaOptions::base_url()}}rma/radni-nalog-prijem/{{ $row->radni_nalog_id }}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Izmeni</a>
							</td>
							<td>{{ $row->broj_naloga }}</td>
							@if(!is_null($row->kupac_id))
							<td>{{ RMA::getKupac($row->kupac_id,'ime').' '.RMA::getKupac($row->kupac_id,'prezime') }}</td>
							@else
							<td>{{ !is_null($partner=RMA::partner($row->b2b_partner_id)) ? $partner->naziv : '' }}</td>
							@endif
							<td>{{ RMA::partner($row->partner_id)->naziv }}</td>
							<td>{{ $row->datum_prijema }}</td>
							<td>{{ $row->datum_zavrsetka }}</td>
							<td>{{ $row->uredjaj }}</td>
							<td>{{ substr($row->opis_kvara,0,50) }}</td>
							<td>{{ (RMA::radni_nalog_cene($row->radni_nalog_id)->cena_rada * 1.2) }}</td>
							<td>{{ RMA::getRadniNalogStatus($row->status_id)->naziv }}</td>
							<td class="table-col-icons">
								<a class="remove icon JSbtn-delete" data-link="{{RmaOptions::base_url()}}rma/radni-nalog/{{ $row->radni_nalog_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Obriši</a>
							</td>
						</tr>
						@endforeach

					</table>
				</div>
				{{ is_array($radni_nalozi) ? Paginator::make($radni_nalozi, $count, $limit)->links() : '' }}
			</div>
		</div>
	</div>
</div>
@endsection