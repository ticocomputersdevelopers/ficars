<!-- MENU_TOP.blade -->
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal" rel="nofollow"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave" rel="nofollow"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin" rel="nofollow"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div class="preheader">

    <div class="social-icons">  
        {{Options::social_icon()}} 
    </div>

    <div class="container"> 
        <div class="row top-menu">

            <div class="col-md-11 col-sm-11 col-xs-10">     
                
                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block text-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 

                <ul class="hidden-small JStoggle-content">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a class="center-block" href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

            </div>

            <div class="col-md-1 col-sm-1 col-xs-2 text-center"> 
                @if(Options::checkB2B())
                <a href="{{Options::domain()}}b2b/login" class="center-block" rel="nofollow">B2B</a> 
                @endif 
            </div>   
 
        </div> 
    </div>
</div>
<!-- MENU_TOP.blade END -->



