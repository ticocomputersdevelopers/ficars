<div class="col-md-1 col-sm-1 col-xs-2 sm-no-padd">
	
	<div class="header-cart-container relative">  
		
		<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
			
			<i class="fas fa-shopping-cart"></i>		
			
			<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		
			
			<!-- <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	 -->
		</a>

		<div class="JSheader-cart-content hidden-sm hidden-xs">
			@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
		</div>
	</div>
</div> 