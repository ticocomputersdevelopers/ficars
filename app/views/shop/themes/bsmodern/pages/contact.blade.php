@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTACT.blade -->

<div class="row contact-page"> 
	<br><br>
	<h2 class="text-center">
		<span class="text-uppercase">
			{{ Language::trans('Pošaljite upit') }}
		</span>
	</h2>
	<p class="text-center">
		<i> {{ Language::trans('popunite upit kako bi vam dostavili ponudu za traženi proizvod') }} </i>
	</p>
	<br><br>

	<div class="col-md-8 col-sm-12 col-xs-12 sm-no-padd"> 
		<br>
		<!-- <h2><span class="section-title">{{ Language::trans('Pošaljite poruku') }}</span></h2> -->

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<!-- <label id="label_name">{{ Language::trans('Vaše ime') }} *</label> -->
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}" placeholder="{{ Language::trans('VAŠE IME') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<!-- <label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label> -->
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" placeholder="{{ Language::trans('VAŠ EMAIL') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>
				<input class="contact-model-vozila" name="contact-model-vozila" id="" type="text" value="{{ Input::old('contact-model-vozila') }}" placeholder="{{ Language::trans('MODEL VOZILA') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-model-vozila') ? $errors->first('contact-model-vozila') : "" }}</div>
			</div>		

			<div>
				<input class="contact-godiste-vozila" name="contact-godiste-vozila" id="" type="text" value="{{ Input::old('contact-godiste-vozila') }}" placeholder="{{ Language::trans('GODIŠTE VOZILA') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-godiste-vozila') ? $errors->first('contact-godiste-vozila') : "" }}</div>
			</div>	

			<div>
				<input class="contact-kubikaza-vozila" name="contact-kubikaza-vozila" id="" type="text" value="{{ Input::old('contact-kubikaza-vozila') }}" placeholder="{{ Language::trans('KUBIKAŽA VOZILA') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-kubikaza-vozila') ? $errors->first('contact-kubikaza-vozila') : "" }}</div>
			</div>	

			<div>
				<input class="contact-snaga-vozila" name="contact-snaga-vozila" id="" type="text" value="{{ Input::old('contact-snaga-vozila') }}" placeholder="{{ Language::trans('KW VOZILA') }} *">
				<div class="error red-dot-error">{{ $errors->first('contact-snaga-vozila') ? $errors->first('contact-snaga-vozila') : "" }}</div>
			</div>	

			<div>
				<input class="contact-broj-sasije" name="contact-broj-sasije" id="" type="text" value="{{ Input::old('contact-broj-sasije') }}" placeholder="{{ Language::trans('BROJ ŠASIJE VOZILA - OPCIONO') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-broj-sasije') ? $errors->first('contact-broj-sasije') : "" }}</div>
			</div>	

			<div>	
				<!-- <label id="label_message">{{ Language::trans('Vaša poruka') }} </label> -->
				<textarea class="contact-message" name="contact-message" rows="5" id="message" placeholder="{{ Language::trans('VAŠA PORUKA') }}*">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>


	<div class="col-md-4 col-sm-12 col-xs-12 sm-no-padd">
		<br>
		<div class="row">
			<div class="col-xs-2 no-padding">
				<div class="v-align inline-block contact-icons"></div>
			</div>

			<div class="col-xs-10 color-gray">
				<p class="text-uppercase"> {{ Language::trans('Prodaja') }} </p>

				@if(Options::company_name() != '') 
					<div> {{ Options::company_name() }} </div>
				@endif

				@if(Options::company_adress() != '')
				<div>
					{{ Options::company_adress() }}	
				</div>
				@endif

				@if(Options::company_city() != '')
				<div>	
					{{ Options::company_city() }}
				</div>
				@endif
			</div>
		</div>
		<br>
		<div class="row">

			<div class="col-xs-2 no-padding">
				<div class="v-align inline-block contact-icons contact-icon-2"></div>
			</div>

			<div class="col-xs-10 color-gray">
				<p class="text-uppercase"> {{ Language::trans('Kontaktirajte nas') }}  </p>
				@if( Options::company_email() != '')
					<div class="flex">
						<div>{{ Language::trans('E-mail') }}:</div>
						<div>
							<a class="mailto" href="mailto:{{ Options::company_email() }}">
							{{ Options::company_email() }}
							</a>
						</div> 
					</div>
				@endif 

				@if(Options::company_phone() != '')
					<div class="flex">
						<div>{{ Language::trans('Telefon') }}:</div>
						<div> 
							<a href="te:{{ Options::company_phone() }}">
								{{ Options::company_phone() }}
							</a>
						</div>
					</div>
				@endif
			</div>

		</div>
		<!-- <h2><span class="section-title">{{ Language::trans('Kontakt informacije') }}</span></h2> -->

		<!-- @if(Options::company_name() != '') 
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Firma') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_name() }}</div>
		</div> 
		@endif

		@if(Options::company_adress() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_adress() }}</div>
		</div>
		@endif
		
		@if(Options::company_city() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_city() }}</div>
		</div>
		@endif
		
		@if(Options::company_phone() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_phone() }}</div>
		</div>
		@endif
		
		@if(Options::company_fax() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Fax') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_fax() }}</div>
		</div>
		@endif
		
		@if(Options::company_pib() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('PIB') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_pib() }}</div>
		</div>
		@endif
		
		@if(Options::company_maticni() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Matični broj') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_maticni() }}</div>
		</div>
		@endif
		
		@if( Options::company_email() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('E-mail') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</div> 
		</div>
		@endif -->
		<br><br>
	</div> 
</div>

<!-- MAP IS IN TEMPLATE -->
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

	});
</script>
@endif

<!-- CONTACT.blade END -->

@endsection     